package com.film.bot;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.utility.DockerImageName;

public class MongoExtension implements BeforeAllCallback {
    private static MongoDBContainer mongoDBContainer;

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {
        if (mongoDBContainer == null) {
            mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:4.0.10"));
            mongoDBContainer.withCommand("--noauth");
            mongoDBContainer.waitingFor(Wait.forLogMessage(".*for connection.*", 1))
                    .withReuse(true);
        }
        mongoDBContainer.start();
        System.setProperty("spring.data.mongodb.port", mongoDBContainer.getFirstMappedPort().toString());
        System.setProperty("spring.data.mongodb.host", mongoDBContainer.getHost());
    }

    @Override
    protected void finalize() throws Throwable {
        mongoDBContainer.stop();
        mongoDBContainer.close();
        super.finalize();
    }
}
