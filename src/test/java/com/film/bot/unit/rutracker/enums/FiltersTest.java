package com.film.bot.unit.rutracker.enums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

class FiltersTest {
    private final List<String> strsList = List.of("Seed", "Rating", "Best", "FIRST", "SECOND", "THIRD", "FOURTH");
    private final List<Filters> filters = Arrays.stream(Filters.values()).toList();
    private final HashMap<String, Filters> strToFiler = new HashMap<>();
    private final HashMap<Filters, String> filterToStr = new HashMap<>();
    private final int size = strsList.size();

    @BeforeEach
    void init() {
        for (int i = 0; i < size; i++) {
            strToFiler.put(strsList.get(i), filters.get(i));
            filterToStr.put(filters.get(i), strsList.get(i));
        }
    }

    @Test
    void fromValue() {
        for (String filter : strsList) {
            Assertions.assertEquals(Filters.fromValue(filter), strToFiler.get(filter));
        }
    }

    @Test
    void getAlias() {
        for (Filters filter : filters) {
            Assertions.assertEquals(filter.getAlias(), filterToStr.get(filter));
        }
    }

    @Test
    void values() {
        Assertions.assertEquals(filters, Arrays.stream(Filters.values()).toList());
    }

    @Test
    void valueOf() {
        for (Filters filter : filters) {
            Assertions.assertEquals(Filters.valueOf(filter.toString()), filter);
        }
    }
}