package com.film.bot.unit.rutracker;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

class TorrentStatusTest {
    private final List<String> strsList = List.of("проверено", "не проверено", "недооформлено", "сомнительно", "поглощено", "временная", "закрыто");
    private final List<TorrentStatus> statuses = List.of(TorrentStatus.APPROVED, TorrentStatus.NOT_APPROVED, TorrentStatus.NEED_EDIT, TorrentStatus.DUBIOUSLY, TorrentStatus.CONSUMED, TorrentStatus.TEMPORARY, TorrentStatus.CLOSED);
    private final HashMap<String, TorrentStatus> strToStatus = new HashMap<>();
    private final HashMap<TorrentStatus, String> statusToStr = new HashMap<>();
    private final int size = strsList.size();

    @BeforeEach
    void init() {
        for (int i = 0; i < size; i++) {
            strToStatus.put(strsList.get(i), statuses.get(i));
            statusToStr.put(statuses.get(i), strsList.get(i));
        }
    }


    @Test
    void getStatus() {
        for (TorrentStatus obj : statuses) {
            Assertions.assertEquals(obj.getStatus(), statusToStr.get(obj));
        }
    }

    @Test
    void valueOfLabel() {
        for (String str : strsList) {
            Assertions.assertEquals(TorrentStatus.valueOfLabel(str), strToStatus.get(str));
        }
    }

    @Test
    void valueOfLabelIncorrect() {
        Assertions.assertNull(TorrentStatus.valueOfLabel("sdf"));
    }

    @Test
    void valueOf() {
        for (TorrentStatus obj : statuses) {
            Assertions.assertEquals(TorrentStatus.valueOf(obj.toString()), obj);
        }
    }
}