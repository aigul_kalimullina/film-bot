package com.film.bot.unit.rutracker;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Date;

class TorrentTest {

    private final TorrentStatus status = TorrentStatus.APPROVED;
    private final String forum = "3D Мультфильмы";
    private final String search = "Тайна Коко";
    private final String name = "Тайна Коко 3Д / Coco 3D (Ли Анкрич / Lee Unkrich, Эдриан Молина / Adrian Molina) [2017, США, мультфильм, фэнтези, комедия, детектив, приключения, WEB-DL] [Локализованный видеоряд] Dub (Невафильм) + Kaz + Original (Eng) Half SideBySide / Горизонтальная анаморфная стереопара";
    private final String url = "https://rutracker.org/forum/viewtopic.php?t=6326386";
    private final String author = "SOFCJ";
    private final double size = 4.0; // size in GB
    private final int seeders = 4;
    private final int leechers = 0;
    private final int timesDownloaded = 688;
    private final Date date = new Date(1677096373000l);
    private String torrentMagnetURL = "magnet:?xt=urn:btih:E3F5FC6129B7CBDECF5DC1C8B2FE74CA8FE4F5BB&tr=http%3A%2F%2Fbt2.t-ru.org%2Fann%3Fmagnet&dn=Тайна%20Коко%203Д%20%2F%20Coco%203D%20(Ли%20Анкрич%20%2F%20Lee%20Unkrich%2C%20Эдриан%20Молина%20%2F%20Adrian%20Molina)%20%5B2017%2C%20США%2C%20мультфильм%2C%20фэнтези%2C%20комедия%2C%20детектив%2C%20приключения%2C%20WEB-";
    private Torrent torrent = new Torrent(status, forum, name, url, author, size, seeders, leechers, timesDownloaded, date, torrentMagnetURL);
    private Torrent torrent_elem = new Torrent(getElem());

    @Test
    void constructorTest() {
        Element element = getElem();
        Assertions.assertEquals(torrent.getName(), new Torrent(element).getName());
    }

    @Test
    void getStatus() {
        Assertions.assertEquals(status, torrent.getStatus());
    }

    @Test
    void getForum() {
        Assertions.assertEquals(forum, torrent.getForum());
    }

    @Test
    void getName() {
        Assertions.assertEquals(name, torrent.getName());
    }

    @Test
    void getUrl() {
        Assertions.assertEquals(url, torrent.getUrl());
    }

    @Test
    void getAuthor() {
        Assertions.assertEquals(author, torrent.getAuthor());
    }

    @Test
    void getSize() {
        Assertions.assertEquals(size, torrent.getSize());
    }

    @Test
    void getSeeders() {
        Assertions.assertEquals(seeders, torrent.getSeeders());
    }

    @Test
    void getLeechers() {
        Assertions.assertEquals(leechers, torrent.getLeechers());
    }

    @Test
    void getTimesDownloaded() {
        Assertions.assertEquals(timesDownloaded, torrent.getTimesDownloaded());
    }

    @Test
    void getDate() {
        Assertions.assertEquals(date, torrent.getDate());
    }

    @Test
    void getTorrentMagnetURL() {
        Assertions.assertEquals(torrentMagnetURL, torrent.getTorrentMagnetURL());
    }

    @Test
    void round() {
        Assertions.assertEquals(torrent.round(3.5559), 3.56);
    }

    private Element getElem() {
        try {
            if (Settings.USE_PROXY) {
                HttpProxy.setProxy();
            }
            if (!Authenticator.isAuthenticated()) {
                Authenticator.login();
            }
            Document doc;
            if (Settings.USE_PROXY) {
                doc = Jsoup.connect(Settings.SEARCH_URL + search)
                        .cookies(Authenticator.getCookies())
                        .proxy(HttpProxy.getProxyObject())
                        .get();
            } else {
                doc = Jsoup.connect(Settings.SEARCH_URL + search)
                        .cookies(Authenticator.getCookies())
                        .get();
            }

            Elements results = doc.getElementsByClass("tCenter hl-tr");
            return results.get(0);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testEquals() {
        Assertions.assertTrue(torrent_elem.equals(new Torrent(getElem())));
    }

    @Test
    void canEqual() {
        Assertions.assertTrue(torrent_elem.canEqual(new Torrent(getElem())));
    }

    @Test
    void testHashCode() {
        Assertions.assertEquals(torrent_elem.hashCode(), new Torrent(getElem()).hashCode());
    }

    @Test
    void testToString() {
        Assertions.assertEquals(torrent_elem.toString(), new Torrent(getElem()).toString());
    }

    @Test
    void setTorrentMagnetURL() {
        torrent_elem.setTorrentMagnetURL("123");
        Torrent tst = new Torrent(getElem());
        tst.setTorrentMagnetURL("123");
        Assertions.assertEquals(torrent_elem, tst);

    }
}