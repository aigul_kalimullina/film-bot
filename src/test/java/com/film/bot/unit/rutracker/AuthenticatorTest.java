package com.film.bot.unit.rutracker;

import java.io.IOException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AuthenticatorTest {

    @Test
    void testLogin() {
        try {
            Settings.USE_PROXY = false; //with vpn works
            Authenticator.login();
            Assertions.assertTrue(Authenticator.authenticated);
            Assertions.assertFalse(Authenticator.getCookies().isEmpty());
        } catch (IOException e) {
            Assertions.fail("Error occurred during login");
        }
    }
}