package com.film.bot.unit.rutracker;

import com.film.bot.unit.movie.enums.Size;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

class RuTrackerSearchTest {
    final TorrentStatus status = TorrentStatus.CLOSED;
    final String forum = "3D Мультфильмы";
    final String name = "Тайна Коко 3Д / Coco 3D (Ли Анкрич / Lee Unkrich, Эдриан Молина / Adrian Molina) [2017, США, мультфильм, фэнтези, комедия, детектив, приключения, WEB-DL] [Локализованный видеоряд] Dub (Невафильм) + Kaz + Original (Eng) Half SideBySide / Горизонтальная анаморфная стереопара";
    final String author = "SOFCJ";
    final String url = "https://rutracker.org/forum/viewtopic.php?t=6326386";
    final double size = 4.0; // size in GB
    final int seeders = 0;
    final int leechers = 0;
    final int timesDownloaded = 688;
    final Date date = new Date(1677096373000l);
    String torrentMagnetURL = "magnet:?xt=urn:btih:E3F5FC6129B7CBDECF5DC1C8B2FE74CA8FE4F5BB&tr=http%3A%2F%2Fbt2.t-ru.org%2Fann%3Fmagnet&dn=Тайна%20Коко%203Д%20%2F%20Coco%203D%20(Ли%20Анкрич%20%2F%20Lee%20Unkrich%2C%20Эдриан%20Молина%20%2F%20Adrian%20Molina)%20%5B2017%2C%20США%2C%20мультфильм%2C%20фэнтези%2C%20комедия%2C%20детектив%2C%20приключения%2C%20WEB-";
    Torrent torrent = new Torrent(status, forum, name, url, author, size, seeders, leechers, timesDownloaded, date, torrentMagnetURL);
    private final String search = "Тайна Коко";
    private final String searchLink = "https://rutracker.org/forum/tracker.php?nm=%D0%A2%D0%B0%D0%B9%D0%BD%D0%B0+%D0%9A%D0%BE%D0%BA%D0%BE";
    private RuTrackerSearch ruTrackerSearch = new RuTrackerSearch();
    private List<Torrent> torrents = getTorrents();


    @Test
    void createSimpleSearchLink() {
        Assertions.assertEquals(searchLink, ruTrackerSearch.createSimpleSearchLink(search));
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void Constructor(boolean proxy) {
        Settings.USE_PROXY = proxy;
        Assertions.assertDoesNotThrow(() -> construct());
    }

    private RuTrackerSearch construct() {
        return new RuTrackerSearch();
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void search(boolean proxy) throws IOException {
        Settings.USE_PROXY = proxy;
        Assertions.assertEquals(getTorrent(), ruTrackerSearch.search(search).get(0));
    }

    @Test
    void seedersFilter() {
        List<Torrent> torrents_tst = new ArrayList<>(torrents);
        torrents_tst = torrents_tst.stream().
                filter(row -> row.getSeeders() > 0).
                filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                sorted(Comparator.comparingInt(Torrent::getSeeders).reversed()).toList();
        Assertions.assertEquals(torrents_tst.get(0), ruTrackerSearch.seedersFilter(torrents));
    }

    @Test
    void ratingFilter() {
        List<Torrent> torrents_tst = new ArrayList<>(torrents);
        torrents_tst = torrents_tst.stream().
                filter(row -> row.getSeeders() > 0).
                filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                sorted(Comparator.comparingInt(Torrent::getTimesDownloaded).reversed()).toList();
        Assertions.assertEquals(torrents_tst.get(0), ruTrackerSearch.ratingFilter(torrents));
    }

    @Test
    void ratingFilterNull() {
        List<Torrent> torrents_tst = new ArrayList<>();
        Assertions.assertNull(ruTrackerSearch.ratingFilter(torrents_tst));
    }

    @Test
    void seedersFilterNull() {
        List<Torrent> torrents_tst = new ArrayList<>();
        Assertions.assertNull(ruTrackerSearch.seedersFilter(torrents_tst));
    }

    @Test
    void sizeFilter() {
        Size sizeReq = Size.SECOND;
        List<Torrent> torrents_tst = new ArrayList<>(torrents);
        torrents_tst = torrents_tst.stream().
                filter(row -> row.getSeeders() > 0).
                filter(row -> row.getSize() >= sizeReq.getStart()).
                filter(row -> sizeReq.getEnd() == null || row.getSize() <= sizeReq.getEnd()).
                filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                sorted(Comparator.comparingInt(Torrent::getSeeders).reversed()).toList();
        Torrent currentResult = torrents_tst.get(0);
        int topSeedsAmount = currentResult.getSeeders();
        for (int i = 1; i < torrents_tst.size(); i++) {
            if (torrents_tst.get(i).getDate().after(currentResult.getDate()) &&
                    (torrents_tst.get(0).getSeeders() - torrents_tst.get(i).getSeeders()) < percentSeeders(topSeedsAmount) &&
                    torrents_tst.get(i).getSeeders() > 0) {
                currentResult = torrents_tst.get(i);
            }
        }
        Assertions.assertEquals(currentResult, ruTrackerSearch.sizeFilter(torrents, sizeReq));
    }

    @Test
    void offerBestOne() {
        List<Torrent> torrents_tst = new ArrayList<>(torrents);
        torrents_tst = torrents_tst.stream().
                filter(row -> row.getSeeders() > 0).
                filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                sorted(Comparator.comparingInt(Torrent::getSeeders).reversed()).toList();
        Torrent currentResult = torrents_tst.get(0);
        int topSeedsAmount = currentResult.getSeeders();
        for (int i = 1; i < torrents_tst.size(); i++) {
            if (torrents_tst.get(i).getDate().after(currentResult.getDate()) &&
                    (torrents_tst.get(0).getSeeders() - torrents_tst.get(i).getSeeders()) < percentSeeders(topSeedsAmount) &&
                    torrents_tst.get(i).getSeeders() > 0) {
                currentResult = torrents_tst.get(i);
            }
        }
        Assertions.assertEquals(torrents_tst.get(0), ruTrackerSearch.offerBestOne(torrents));
    }

    @Test
    void offerBestOneNull() {
        List<Torrent> torrents_tst = new ArrayList<>();
        Assertions.assertNull(ruTrackerSearch.offerBestOne(torrents_tst));
    }

    @Test
    void sizeFilterNull() {
        List<Torrent> torrents_tst = new ArrayList<>();
        Assertions.assertNull(ruTrackerSearch.sizeFilter(torrents_tst, Size.SECOND));
    }

    @Test
    void sizeFilterOnly0() {
        List<Torrent> torrents_tst = new ArrayList<>();
        torrents_tst.add(torrent);
        Assertions.assertEquals(torrent, ruTrackerSearch.sizeFilter(torrents_tst, Size.SECOND));
    }

    @Test
    void offerBestOneOnly0() {
        List<Torrent> torrents_tst = new ArrayList<>();
        torrents_tst.add(torrent);
        Assertions.assertEquals(torrent, ruTrackerSearch.offerBestOne(torrents_tst));
    }

    private int percentSeeders(int seedersAmount) {
        double result = ((double) seedersAmount / 100) * 15;
        return (int) result;
    }


    private Torrent getTorrent() {
        try {
            if (!Authenticator.isAuthenticated()) {
                Authenticator.login();
            }
            Document doc = Jsoup.connect(Settings.SEARCH_URL + search)
                    .cookies(Authenticator.getCookies())
                    .get();
            Elements results = doc.getElementsByClass("tCenter hl-tr");
            return new Torrent(results.get(0));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<Torrent> getTorrents() {
        try {
            if (!Authenticator.isAuthenticated()) {
                Authenticator.login();
            }
            Document doc = Jsoup.connect(Settings.SEARCH_URL + search)
                    .cookies(Authenticator.getCookies())
                    .get();
            Elements results = doc.getElementsByClass("tCenter hl-tr");
            List<Torrent> searchResults = new ArrayList<>();
            for (Element result : results) {
                searchResults.add(new Torrent(result));
            }
            return searchResults;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}