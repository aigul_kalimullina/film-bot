package com.film.bot.unit.user.manager.mapper;

import com.film.bot.unit.user.dal.entity.UserHistoryEntity;
import org.junit.jupiter.api.Test;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.api.objects.inlinequery.ChosenInlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.payments.PreCheckoutQuery;
import org.telegram.telegrambots.meta.api.objects.payments.ShippingQuery;
import org.telegram.telegrambots.meta.api.objects.polls.Poll;
import org.telegram.telegrambots.meta.api.objects.polls.PollAnswer;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserHistoryEntityMapperTest {

    Integer updateId = 12345;
    Message message = new Message();
    InlineQuery inlineQuery = new InlineQuery();
    ChosenInlineQuery chosenInlineQuery = new ChosenInlineQuery();
    CallbackQuery callbackQuery = new CallbackQuery();
    Message editedMessage = new Message();
    Message channelPost = new Message();
    Message editedChannelPost = new Message();
    ShippingQuery shippingQuery = new ShippingQuery();
    PreCheckoutQuery preCheckoutQuery = new PreCheckoutQuery();
    Poll poll = new Poll();
    PollAnswer pollAnswer = new PollAnswer();
    ChatMemberUpdated myChatMember = new ChatMemberUpdated();
    ChatMemberUpdated chatMember = new ChatMemberUpdated();
    ChatJoinRequest chatJoinRequest = new ChatJoinRequest();
    Update update = new Update(updateId, message, inlineQuery, chosenInlineQuery, callbackQuery, editedMessage,
            channelPost, editedChannelPost, shippingQuery, preCheckoutQuery, poll, pollAnswer, myChatMember,
            chatMember, chatJoinRequest);
    String id = "123";
    Long userId = 123l;
    Long createAt = 123543l;
    private UserHistoryMapper userHistoryMapper = new UserHistoryMapper();
    private UserHistoryEntity userHistoryEntity = new UserHistoryEntity(id, userId, createAt, update);

    @Test
    void testFromDal() {
        Update update = userHistoryEntity.getUpdate();
        Update result = userHistoryMapper.fromDal(userHistoryEntity);
        assertEquals(update, result);
    }

    @Test
    void testToDalWithMsg() {
        User usr = new User();
        usr.setId(userId);
        Message message = new Message();
        message.setFrom(usr);
        update.setMessage(message);
        UserHistoryEntity userHistoryEntity = userHistoryMapper.toDal(update);
        assertEquals(update, userHistoryEntity.getUpdate());
    }

    @Test
    void testToDal() {
        User usr = new User();
        usr.setId(userId);
        update.setMessage(null);
        CallbackQuery callbackQuery = new CallbackQuery();
        callbackQuery.setFrom(usr);
        update.setCallbackQuery(callbackQuery);
        UserHistoryEntity userHistoryEntity = userHistoryMapper.toDal(update);
        assertEquals(update, userHistoryEntity.getUpdate());
    }
}