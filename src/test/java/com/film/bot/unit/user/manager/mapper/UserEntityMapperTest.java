package com.film.bot.unit.user.manager.mapper;

import com.film.bot.unit.user.dal.entity.UserEntity;
import com.film.bot.unit.user.manager.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserEntityMapperTest {
    private Long id = 1l;
    private String name = "myName";
    private UserEntity userEntity = new UserEntity(id, name);
    private User user = new User(id, name);
    private UserEntityMapper entityMapper = new UserEntityMapper();

    @Test
    void fromDal() {
        User test = entityMapper.fromDal(userEntity);
        Assertions.assertEquals(user.getId(), test.getId());
        Assertions.assertEquals(user.getUsername(), test.getUsername());
    }

    @Test
    void toDal() {
        UserEntity entityTest = entityMapper.toDal(user);
        Assertions.assertEquals(entityTest.getId(), userEntity.getId());
        Assertions.assertEquals(entityTest.getUsername(), userEntity.getUsername());
    }
}