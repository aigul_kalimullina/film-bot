package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductionCompanyTest {

    @Test
    public void testGetterAndSetter() {
        ProductionCompany company = new ProductionCompany();
        company.setName("Company A");
        assertEquals("Company A", company.getName());
    }

    @Test
    public void testAllArgsConstructor() {
        ProductionCompany company = new ProductionCompany("Company B");
        assertEquals("Company B", company.getName());
    }

    @Test
    public void testNoArgsConstructor() {
        ProductionCompany company = new ProductionCompany();
        assertNull(company.getName());
    }
}