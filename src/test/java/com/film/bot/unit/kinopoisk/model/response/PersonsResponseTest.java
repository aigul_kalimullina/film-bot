package com.film.bot.unit.kinopoisk.model.response;

import com.film.bot.unit.kinopoisk.model.Person;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonsResponseTest {
    Long id = 1L;
    String name = "Имя";
    String enName = "Name";
    Integer age = 23;
    Long id2 = 2L;
    String name2 = "Имя2";
    String enName2 = "Name2";
    Integer age2 = 24;
    Long id3 = 3L;
    String name3 = "Имя3";
    String enName3 = "Name3";
    Integer age3 = 25;
    Integer limit = 10;
    Integer limit2 = 5;
    List<Person> persons = new ArrayList<>();
    List<Person> newPersons = new ArrayList<>();
    Person p1 = new Person(id, name, enName, age);
    Person p2 = new Person(id2, name2, enName2, age2);
    Person p3 = new Person(id3, name3, enName3, age3);
    @Test
    public void testConstructorAndGettersSetters() {
        persons.add(p1);
        persons.add(p2);
        // Создаем объект PersonsResponse с использованием конструктора
        PersonsResponse response = new PersonsResponse();
        response.setPersons(persons);
        response.setLimit(limit);

        assertEquals(persons, response.getPersons());
        assertEquals(limit, response.getLimit());

        newPersons.add(p3);
        newPersons.add(p1);
        response.setPersons(newPersons);
        response.setLimit(limit2);

        // Проверяем, что сеттеры установили новые значения
        assertEquals(newPersons, response.getPersons());
        assertEquals(limit2, response.getLimit());
    }

    @Test
    public void testNoArgsConstructor() {
        // Создаем объект PersonsResponse с использованием пустого конструктора
        PersonsResponse response = new PersonsResponse();

        // Проверяем, что поля инициализированы значениями по умолчанию (null, 0)
        assertEquals(null, response.getPersons());
        assertEquals(null, response.getLimit());
    }

    @Test
    public void testAllArgsConstructor() {
        persons.add(p1);
        persons.add(p2);
        // Создаем объект PersonsResponse с использованием конструктора
        PersonsResponse response = new PersonsResponse(persons, limit);

        assertEquals(persons, response.getPersons());
        assertEquals(limit, response.getLimit());
    }
}
