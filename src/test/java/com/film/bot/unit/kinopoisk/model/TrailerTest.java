package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TrailerTest {

    @Test
    public void testGetUrl() {
        Trailer trailer = new Trailer("https://example.com");
        assertEquals("https://example.com", trailer.getUrl());
    }

    @Test
    public void testSetUrl() {
        Trailer trailer = new Trailer();
        trailer.setUrl("https://example.com");
        assertEquals("https://example.com", trailer.getUrl());
    }

    @Test
    public void testAllArgsConstructor() {
        Trailer trailer = new Trailer("https://example.com");
        assertEquals("https://example.com", trailer.getUrl());
    }

    @Test
    public void testNoArgsConstructor() {
        Trailer trailer = new Trailer();
        assertEquals(null, trailer.getUrl());
    }
}