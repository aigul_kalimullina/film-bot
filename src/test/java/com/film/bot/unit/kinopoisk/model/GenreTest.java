package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GenreTest {

    @Test
    public void testGetterAndSetter() {
        Genre genre = new Genre();

        genre.setName("Action");
        assertEquals("Action", genre.getName());
    }

    @Test
    public void testAllArgsConstructor() {
        Genre genre = new Genre("Comedy");
        assertEquals("Comedy", genre.getName());
    }

    @Test
    public void testNoArgsConstructor() {
        Genre genre = new Genre();
        assertNull(genre.getName());
    }
}