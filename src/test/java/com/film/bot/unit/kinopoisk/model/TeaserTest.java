package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TeaserTest {

    @Test
    public void testGetUrl() {
        Teaser teaser = new Teaser("https://example.com");
        assertEquals("https://example.com", teaser.getUrl());
    }

    @Test
    public void testSetUrl() {
        Teaser teaser = new Teaser();
        teaser.setUrl("https://example.com");
        assertEquals("https://example.com", teaser.getUrl());
    }

    @Test
    public void testAllArgsConstructor() {
        Teaser teaser = new Teaser("https://example.com");
        assertEquals("https://example.com", teaser.getUrl());
    }

    @Test
    public void testNoArgsConstructor() {
        Teaser teaser = new Teaser();
        assertEquals(null, teaser.getUrl());
    }
}