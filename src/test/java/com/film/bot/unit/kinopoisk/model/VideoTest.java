package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class VideoTest {

    @Test
    public void testGetterAndSetter() {
        Video video = new Video();

        List<Trailer> trailers = new ArrayList<>();
        trailers.add(new Trailer("Trailer 1"));
        video.setTrailers(trailers);
        assertEquals(trailers, video.getTrailers());

        List<Teaser> teasers = new ArrayList<>();
        teasers.add(new Teaser("Teaser 1"));
        video.setTeasers(teasers);
        assertEquals(teasers, video.getTeasers());
    }

    @Test
    public void testAllArgsConstructor() {
        List<Trailer> trailers = new ArrayList<>();
        trailers.add(new Trailer("Trailer 1"));

        List<Teaser> teasers = new ArrayList<>();
        teasers.add(new Teaser("Teaser 1"));

        Video video = new Video(trailers, teasers);

        assertEquals(trailers, video.getTrailers());
        assertEquals(teasers, video.getTeasers());
    }

    @Test
    public void testNoArgsConstructor() {
        Video video = new Video();
        assertNull(video.getTrailers());
        assertNull(video.getTeasers());
    }
}