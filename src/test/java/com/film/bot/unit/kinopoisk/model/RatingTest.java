package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RatingTest {
    @Test
    public void testGetterAndSetter() {
        Rating rating = new Rating();
        rating.setKp(7.5);
        assertEquals(7.5, rating.getKp());
    }

    @Test
    public void testAllArgsConstructor() {
        Rating rating = new Rating(8.0);
        assertEquals(8.0, rating.getKp());
    }

    @Test
    public void testNoArgsConstructor() {
        Rating rating = new Rating();
        assertEquals(0.0, rating.getKp());
    }
}