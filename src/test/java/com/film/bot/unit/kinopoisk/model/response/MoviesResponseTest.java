package com.film.bot.unit.kinopoisk.model.response;

import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.kinopoisk.model.response.MoviesResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MoviesResponseTest {
    Integer limit = 10;
    Integer pages = 5;
    List<Film> films = new ArrayList<>();
    Film film1 = mock(Film.class);
    Film film2 = mock(Film.class);

    @Test
    public void testGettersAndSetters() {
        films.add(film1);
        films.add(film2);

        MoviesResponse moviesResponse = new MoviesResponse();
        moviesResponse.setFilms(films);
        moviesResponse.setLimit(limit);
        moviesResponse.setPages(pages);

        assertEquals(films, moviesResponse.getFilms());
        assertEquals(limit, moviesResponse.getLimit());
        assertEquals(pages, moviesResponse.getPages());
    }

    @Test
    public void testNoArgsConstructor() {
        MoviesResponse moviesResponse = new MoviesResponse();
        assertEquals(0, moviesResponse.getLimit());
        assertEquals(0, moviesResponse.getPages());
        assertEquals(null, moviesResponse.getFilms());
    }

    @Test
    public void testAllArgsConstructor() {
        films.add(film1);
        films.add(film2);
        MoviesResponse moviesResponse = new MoviesResponse(films, limit, pages);

        assertEquals(films, moviesResponse.getFilms());
        assertEquals(limit, moviesResponse.getLimit());
        assertEquals(pages, moviesResponse.getPages());
    }

    @Test
    public void testBuilder() {
        films.add(film1);
        films.add(film2);

        MoviesResponse moviesResponse = MoviesResponse.builder()
                .films(films)
                .limit(limit)
                .pages(pages)
                .build();

        assertEquals(films, moviesResponse.getFilms());
        assertEquals(limit, moviesResponse.getLimit());
        assertEquals(pages, moviesResponse.getPages());
    }
}
