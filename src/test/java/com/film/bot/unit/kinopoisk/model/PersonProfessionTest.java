package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonProfessionTest {
    @Test
    public void testGetterAndSetter() {
        PersonProfession personProfession = new PersonProfession();
        personProfession.setId(1L);
        personProfession.setProfession("Engineer");
        assertEquals(1L, personProfession.getId());
        assertEquals("Engineer", personProfession.getProfession());
    }

    @Test
    public void testAllArgsConstructor() {
        PersonProfession personProfession = new PersonProfession(2L, "Doctor");
        assertEquals(2L, personProfession.getId());
        assertEquals("Doctor", personProfession.getProfession());
    }

    @Test
    public void testNoArgsConstructor() {
        PersonProfession personProfession = new PersonProfession();
        assertNull(personProfession.getId());
        assertNull(personProfession.getProfession());
    }
}