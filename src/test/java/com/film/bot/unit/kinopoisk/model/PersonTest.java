package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PersonTest {

    @Test
    public void testGetterAndSetter() {
        Person person = new Person();
        person.setId(1L);
        person.setName("Alice");
        person.setEnName("Anna");
        person.setAge(25);
        assertEquals(1L, person.getId());
        assertEquals("Alice", person.getName());
        assertEquals("Anna", person.getEnName());
        assertEquals(25, person.getAge());
    }

    @Test
    public void testAllArgsConstructor() {
        Person person = new Person(2L, "Bob", "Boris", 30);
        assertEquals(2L, person.getId());
        assertEquals("Bob", person.getName());
        assertEquals("Boris", person.getEnName());
        assertEquals(30, person.getAge());
    }

    @Test
    public void testNoArgsConstructor() {
        Person person = new Person();
        assertNull(person.getId());
        assertNull(person.getName());
        assertNull(person.getEnName());
        assertNull(person.getAge());
    }

    @Test
    public void testToString() {
        Person person = new Person();
        person.setName("Alice");
        person.setAge(25);
        assertEquals("Alice 25 лет", person.toString());

        person.setName("");
        person.setEnName("Anna");
        assertEquals("Anna 25 лет", person.toString());

        person.setAge(21);
        assertEquals("Anna 21 год", person.toString());

        person.setAge(32);
        assertEquals("Anna 32 года", person.toString());

        person.setAge(100);
        assertEquals("Anna 100 лет", person.toString());
    }
}