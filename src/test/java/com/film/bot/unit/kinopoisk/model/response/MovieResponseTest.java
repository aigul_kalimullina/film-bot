package com.film.bot.unit.kinopoisk.model.response;

import com.film.bot.unit.kinopoisk.model.*;

import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.kinopoisk.model.utils.MovieTextGenerator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovieResponseTest {
    Long id = 1L;
    String movieName = "Movie Name";
    String alternativeName = "Alt Name";
    String enName = "EnName";
    String description = "Description";
    Rating rating = new Rating(7.4);
    Genre genre = new Genre("anime");
    Country country = new Country("Russia");
    Trailer trailers = new Trailer("url");
    Teaser teasers = new Teaser("url");
    Video video = new Video(List.of(trailers), List.of(teasers));
    Video videoWithoutTeasers = new Video(List.of(trailers), null);
    Video nullVideo = new Video();
    Video videoWithoutTrailers = new Video(null, List.of(teasers));
    ProductionCompany pc = new ProductionCompany("name1");
    ProductionCompany pc2 = new ProductionCompany("name2");
    List<ProductionCompany> productionCompanies = new ArrayList<>();

    Integer year = 2023;
    Integer ageRating = 18;
    Integer movieLength = 120;

    @Test
    public void testGettersAndSetters() {
        productionCompanies.add(pc);
        productionCompanies.add(pc2);
        MovieResponse movieResponse = new MovieResponse();
        movieResponse.setId(id);
        movieResponse.setName(movieName);
        movieResponse.setAlternativeName(alternativeName);
        movieResponse.setEnName(enName);
        movieResponse.setDescription(description);
        movieResponse.setRating(rating);
        movieResponse.setYear(year);
        movieResponse.setAgeRating(ageRating);
        movieResponse.setMovieLength(movieLength);
        movieResponse.setGenres(List.of(genre));
        movieResponse.setCountries(List.of(country));
        movieResponse.setVideos(video);
        movieResponse.setProductionCompanies(productionCompanies);

        assertEquals(id, movieResponse.getId());
        assertEquals(movieName, movieResponse.getName());
        assertEquals(alternativeName, movieResponse.getAlternativeName());
        assertEquals(enName, movieResponse.getEnName());
        assertEquals(description, movieResponse.getDescription());
        assertEquals(rating, movieResponse.getRating());
        assertEquals(year, movieResponse.getYear());
        assertEquals(ageRating, movieResponse.getAgeRating());
        assertEquals(movieLength, movieResponse.getMovieLength());
        assertEquals(List.of(genre), movieResponse.getGenres());
        assertEquals(List.of(country), movieResponse.getCountries());
        assertEquals(video, movieResponse.getVideos());
        assertEquals(productionCompanies, movieResponse.getProductionCompanies());
    }

    @Test
    public void testAllArgsConstructor() {
        MovieResponse movieResponse = new MovieResponse(id, movieName, alternativeName, enName, description,
                rating, year, ageRating, movieLength,
                List.of(genre), List.of(country),
                video,
                productionCompanies);
        assertEquals(id, movieResponse.getId());
        assertEquals(movieName, movieResponse.getName());
        assertEquals(alternativeName, movieResponse.getAlternativeName());
        assertEquals(enName, movieResponse.getEnName());
        assertEquals(description, movieResponse.getDescription());
        assertEquals(rating, movieResponse.getRating());
        assertEquals(year, movieResponse.getYear());
        assertEquals(ageRating, movieResponse.getAgeRating());
        assertEquals(movieLength, movieResponse.getMovieLength());
        assertEquals(List.of(genre), movieResponse.getGenres());
        assertEquals(List.of(country), movieResponse.getCountries());
        assertEquals(video, movieResponse.getVideos());
        assertEquals(productionCompanies, movieResponse.getProductionCompanies());
    }

    @Test
    public void testNoArgsConstructor() {
        MovieResponse movieResponse = new MovieResponse();
        // Проверить, что все поля инициализированы соответствующими значениями по умолчанию (например, 0, null)
        assertEquals(0, movieResponse.getId());
        assertEquals(null, movieResponse.getName());
        assertEquals(null, movieResponse.getAlternativeName());
        assertEquals(null, movieResponse.getEnName());
        assertEquals(null, movieResponse.getDescription());
        assertEquals(null, movieResponse.getRating());
        assertEquals(0, movieResponse.getYear());
        assertEquals(0, movieResponse.getAgeRating());
        assertEquals(0, movieResponse.getMovieLength());
        assertEquals(null, movieResponse.getGenres());
        assertEquals(null, movieResponse.getCountries());
        assertEquals(null, movieResponse.getVideos());
        assertEquals(null, movieResponse.getProductionCompanies());
    }

    @Test
    public void testToString() {
        productionCompanies.add(pc);
        productionCompanies.add(pc2);
        MovieResponse movieResponse = new MovieResponse(id, movieName, alternativeName, enName, description,
                rating, year, ageRating, movieLength,
                List.of(genre), List.of(country),
                video,
                productionCompanies);
        Set<String> names = productionCompanies
                .stream()
                .filter(Objects::nonNull)
                .map(ProductionCompany::getName)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        final StringBuilder sb = MovieTextGenerator.generateMovieBase(
                movieName,
                alternativeName,
                enName,
                year,
                description,
                movieLength,
                rating,
                List.of(genre),
                List.of(country),
                ageRating
        );
        String trailer = video.getTrailers().get(0).getUrl();
        sb.append("Трейлер: ").append(trailer).append('\n');
        String teaser = video.getTeasers().get(0).getUrl();
        sb.append("Тизер: ").append(teaser).append('\n');
        sb.append("Компании производства: ").append(String.join(", ", names)).append('\n');
        String expected = sb.toString();
        assertEquals(expected, movieResponse.toString());

    }

    @Test
    public void testToStringVideoWithoutTrailers() {
        MovieResponse movieResponse = new MovieResponse(id, movieName, alternativeName, enName, description,
                rating, year, ageRating, movieLength,
                List.of(genre), List.of(country),
                videoWithoutTrailers,
                List.of(pc));

        final StringBuilder sb = MovieTextGenerator.generateMovieBase(
                movieName,
                alternativeName,
                enName,
                year,
                description,
                movieLength,
                rating,
                List.of(genre),
                List.of(country),
                ageRating
        );
        String teaser = videoWithoutTrailers.getTeasers().get(0).getUrl();
        sb.append("Тизер: ").append(teaser).append('\n');
        sb.append("Компании производства: ").append(pc.getName()).append('\n');
        String expected = sb.toString();
        assertEquals(expected, movieResponse.toString());

    }

    @Test
    public void testToStringVideoWithoutTeasers() {
        MovieResponse movieResponse = new MovieResponse(id, movieName, alternativeName, enName, description,
                rating, year, ageRating, movieLength,
                List.of(genre), List.of(country),
                videoWithoutTeasers,
                List.of(pc));

        final StringBuilder sb = MovieTextGenerator.generateMovieBase(
                movieName,
                alternativeName,
                enName,
                year,
                description,
                movieLength,
                rating,
                List.of(genre),
                List.of(country),
                ageRating
        );
        String trailer = videoWithoutTeasers.getTrailers().get(0).getUrl();
        sb.append("Трейлер: ").append(trailer).append('\n');
        sb.append("Компании производства: ").append(pc.getName()).append('\n');
        String expected = sb.toString();
        assertEquals(expected, movieResponse.toString());

    }

    @Test
    public void testToStringProductionCompaniesVideoNull() {
        MovieResponse movieResponse = new MovieResponse(id, movieName, alternativeName, enName, description,
                rating, year, ageRating, movieLength,
                List.of(genre), List.of(country),
                nullVideo,
                null);
        final StringBuilder sb = MovieTextGenerator.generateMovieBase(
                movieName,
                alternativeName,
                enName,
                year,
                description,
                movieLength,
                rating,
                List.of(genre),
                List.of(country),
                ageRating
        );
        String expected = sb.toString();
        assertEquals(expected, movieResponse.toString());

    }
}
