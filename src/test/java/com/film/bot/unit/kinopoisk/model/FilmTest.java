package com.film.bot.unit.kinopoisk.model;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FilmTest {

    @Test
    public void testGetterAndSetter() {
        Film film = new Film();
        film.setId(1);
        film.setName("FilmName");
        film.setAlternativeName("AltName");
        film.setEnName("EnglishName");
        film.setYear(2022);
        film.setDescription("Description");
        film.setMovieLength(120);
        List<PersonProfession> persons = new ArrayList<>();
        film.setPersons(persons);
        Rating rating = new Rating(4.5);
        film.setRating(rating);
        List<Genre> genres = new ArrayList<>();
        film.setGenres(genres);
        List<Country> countries = new ArrayList<>();
        film.setCountries(countries);
        film.setAgeRating(16);

        assertEquals(1, film.getId());
        assertEquals("FilmName", film.getName());
        assertEquals("AltName", film.getAlternativeName());
        assertEquals("EnglishName", film.getEnName());
        assertEquals(2022, film.getYear());
        assertEquals("Description", film.getDescription());
        assertEquals(120, film.getMovieLength());
        assertEquals(persons, film.getPersons());
        assertEquals(rating, film.getRating());
        assertEquals(genres, film.getGenres());
        assertEquals(countries, film.getCountries());
        assertEquals(16, film.getAgeRating());
    }

    @Test
    public void testAllArgsConstructor() {
        Rating rating = new Rating(3.8);
        List<Genre> genres = new ArrayList<>();
        List<Country> countries = new ArrayList<>();
        Film film = new Film(2, "Film2", "AltName2", "EnglishName2", 2023, "Description2", 110,
                new ArrayList<>(), rating, genres, countries, 12);

        assertEquals(2, film.getId());
        assertEquals("Film2", film.getName());
        assertEquals("AltName2", film.getAlternativeName());
        assertEquals("EnglishName2", film.getEnName());
        assertEquals(2023, film.getYear());
        assertEquals("Description2", film.getDescription());
        assertEquals(110, film.getMovieLength());
        assertEquals(new ArrayList<>(), film.getPersons());
        assertEquals(rating, film.getRating());
        assertEquals(genres, film.getGenres());
        assertEquals(countries, film.getCountries());
        assertEquals(12, film.getAgeRating());
    }

    @Test
    public void testNoArgsConstructor() {
        Film film = new Film();

        assertNull(film.getName());
        assertNull(film.getAlternativeName());
        assertNull(film.getEnName());
        assertEquals(0, film.getYear());
        assertNull(film.getDescription());
        assertEquals(0, film.getMovieLength());
        assertNull(film.getPersons());
        assertNull(film.getRating());
        assertNull(film.getGenres());
        assertNull(film.getCountries());
        assertEquals(0, film.getAgeRating());
    }

    @Test
    public void testToString() {
        Film film = new Film();
        film.setName("Film1");
        film.setAlternativeName("AltName1");
        film.setEnName("English1");
        film.setYear(2021);
        film.setDescription("Description1");
        film.setMovieLength(100);
        Rating rating = new Rating(4.0);
        List<Genre> genres = new ArrayList<>();
        List<Country> countries = new ArrayList<>();

        String expectedString = "Русское название: Film1\n" +
                "Английское название: English1\n" +
                "Альтернативное название: AltName1\n" +
                "Описание: Description1\n" +
                "Год производства: 2021\n" +
                "Продолжительность: 100 мин.\n";

        assertEquals(expectedString, film.toString());
    }
}