package com.film.bot.unit.kinopoisk.model.utils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.film.bot.unit.kinopoisk.model.Country;
import com.film.bot.unit.kinopoisk.model.Genre;
import com.film.bot.unit.kinopoisk.model.Rating;
import com.film.bot.unit.movie.manager.model.Movie;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class MovieTextGeneratorTest {
    Long id =1L;
    String name = "Test Movie";
    String alternativeName = "Alternative Test Movie";
    String enName = "Test Movie (English)";
    int year = 2022;
    String description = "This is a test movie description.";
    int movieLength = 120;
    Rating rating = new Rating(8.5);
    List<Genre> genres = List.of(new Genre("Action"), new Genre("Adventure"));
    List<Country> countries = List.of(new Country("USA"), new Country("UK"));
    List<String> movieGenres = List.of("Action", "Adventure");
    int ageRating = 16;
    @Test
    void testGenerateMovieBase() {
        StringBuilder result = MovieTextGenerator.generateMovieBase(
                name,
                alternativeName,
                enName,
                year,
                description,
                movieLength,
                rating,
                genres,
                countries,
                ageRating
        );
        List<String> names = countries
                .stream()
                .filter(Objects::nonNull)
                .map(Country::getName)
                .filter(Objects::nonNull)
                .limit(4)
                .distinct()
                .collect(Collectors.toList());
        String expected = "Русское название: " + name + "\n" +
                "Английское название: " + enName + "\n" +
                "Альтернативное название: " + alternativeName + "\n" +
                "Описание: " + description + "\n" +
                "Рейтинг: " + rating.getKp() + "\n" +
                "Год производства: " + year + "\n" +
                "Возрастное ограничение: " + ageRating + "+ \n" +
                "Продолжительность: " + movieLength + " мин.\n" +
                "Жанры: " + String.join(", ", genres
                .stream()
                .filter(Objects::nonNull)
                .map(Genre::getName)
                .filter(Objects::nonNull)
                .toList()) + "\n" +
                "Страны: " + String.join(", ", names) + "\n";
        assertEquals(expected, result.toString());
    }

    @Test
    void testGenerateMovieBaseEmpty() {
        StringBuilder result = MovieTextGenerator.generateMovieBase(
                null,
                null,
                null,
                0,
                null,
               0,
                null,
                null,
                null,
                0
        );

        String expected = "";
        assertEquals(expected, result.toString());
    }

    @Test
    void testGenerateFavouriteBase() {
        Movie movie = new Movie(id, name,enName,movieGenres, rating.getKp() );

        StringBuilder result = MovieTextGenerator.generateFavouriteBase(movie);

        String expected = "\uD83C\uDDF7\uD83C\uDDFA Название: " + movie.ruName() + "\n" +
                "\uD83C\uDDEC\uD83C\uDDE7 Название: " + movie.enName() + "\n" +
                "\uD83E\uDDE9 Жанры: " + String.join(", ", movie.genres()) + "\n" +
                "⬆️ Рейтинг: " + movie.rating() + "\n";
        assertEquals(expected, result.toString());
    }

    @Test
    void testGenerateFavouriteBaseEmptyMovie() {
        Movie movie = new Movie(null,null,null,null, 0 );

        StringBuilder result = MovieTextGenerator.generateFavouriteBase(movie);

        String expected = "";
        assertEquals(expected, result.toString());
    }
}
