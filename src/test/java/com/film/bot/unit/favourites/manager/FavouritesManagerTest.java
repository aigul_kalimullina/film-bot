package com.film.bot.unit.favourites.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.film.bot.unit.favourites.dal.FavouritesDao;
import com.film.bot.unit.favourites.dal.entity.FavouritesEntity;
import com.film.bot.unit.favourites.manager.model.Favourites;
import com.film.bot.unit.movie.manager.MovieManager;
import com.film.bot.unit.movie.manager.model.Movie;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class FavouritesManagerTest {

    @Mock
    private FavouritesDao favouritesDao;
    @Mock
    private MovieManager movieManager;
    @InjectMocks
    private FavouritesManager favouritesManager;

    @Test
    void testFindFavouriteMoviesByUserId() {
        long userId = 1L;
        List<Long> movieIds = List.of(1L, 2L);

        Mockito.when(favouritesDao.findById(userId)).thenReturn(Optional.of(new FavouritesEntity(userId, movieIds)));
        Mockito.when(movieManager.findMoviesByIds(movieIds))
                .thenReturn(List.of(
                                createMovie(100L, "Movie 1"),
                                createMovie(200L, "Movie 2")
                        )
                );

        List<Movie> favouriteMovies = favouritesManager.findFavouriteMoviesByUserId(userId);

        Assertions.assertEquals(2, favouriteMovies.size());
        Assertions.assertEquals("Movie 1", favouriteMovies.get(0).ruName());
        Assertions.assertEquals("Movie 2", favouriteMovies.get(1).ruName());
    }

    @Test
    void testAddToFavourites() {
        long userId = 1L;
        long movieId = 3L;
        Favourites favourites = new Favourites(userId, movieId);

        Mockito.when(favouritesDao.findById(userId))
                .thenReturn(Optional.of(new FavouritesEntity(userId, new ArrayList<>())));

        favouritesManager.addToFavourites(favourites);

        FavouritesEntity addedFavourites = new FavouritesEntity(userId, List.of(movieId));
        Mockito.verify(favouritesDao).save(addedFavourites);
    }

    @Test
    void testAddToFavouritesNotPresent() {
        long userId = 1L;
        long movieId = 3L;
        Favourites favourites = new Favourites(userId, movieId);

        Mockito.when(favouritesDao.findById(userId))
                .thenReturn(Optional.empty());

        favouritesManager.addToFavourites(favourites);

        FavouritesEntity addedFavourites = new FavouritesEntity(userId, List.of(movieId));
        Mockito.verify(favouritesDao).insert(addedFavourites);
    }

    @Test
    void testFindFavouriteMovieInfo() {
        long movieId = 100L;

        Mockito.when(movieManager.findByMovieId(movieId)).thenReturn(createMovie(movieId, "Movie 1"));

        Movie favouriteMovieInfo = favouritesManager.findFavouriteMovieInfo(movieId);

        Assertions.assertEquals("Movie 1", favouriteMovieInfo.ruName());
    }

    private Movie createMovie(long id, String name) {
        return new Movie(
                id,
                name,
                null,
                List.of(),
                0.0
        );
    }
}
