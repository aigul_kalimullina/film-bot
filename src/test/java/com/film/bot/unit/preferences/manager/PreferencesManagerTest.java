package com.film.bot.unit.preferences.manager;

import java.util.stream.Stream;

import com.film.bot.unit.preferences.dal.PreferencesDao;
import com.film.bot.unit.preferences.dal.entity.PreferencesEntity;
import com.film.bot.unit.preferences.manager.mapper.PreferencesEntityMapper;
import com.film.bot.unit.preferences.manager.model.Preferences;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PreferencesManagerTest {

    @Test
    public void testSaveGenre() {
        PreferencesDao preferencesDao = mock(PreferencesDao.class);
        PreferencesEntityMapper preferencesEntityMapper = mock(PreferencesEntityMapper.class);
        PreferencesManager preferencesManager = new PreferencesManager(preferencesDao, preferencesEntityMapper);

        long userId = 1L;
        String genre = "Action";

        preferencesManager.saveGenre(userId, genre);

        verify(preferencesDao).save(any(PreferencesEntity.class));
    }

    @Test
    public void testSaveRating() {
        PreferencesDao preferencesDao = mock(PreferencesDao.class);
        PreferencesEntityMapper preferencesEntityMapper = mock(PreferencesEntityMapper.class);
        PreferencesManager preferencesManager = new PreferencesManager(preferencesDao, preferencesEntityMapper);

        long userId = 1L;
        String rating = "5";

        when(preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)).thenReturn(Stream.of(new PreferencesEntity()));

        preferencesManager.saveRating(userId, rating);

        verify(preferencesDao).save(any(PreferencesEntity.class));
    }

    @Test
    public void testSavePeriod() {
        PreferencesDao preferencesDao = mock(PreferencesDao.class);
        PreferencesEntityMapper preferencesEntityMapper = mock(PreferencesEntityMapper.class);
        PreferencesManager preferencesManager = new PreferencesManager(preferencesDao, preferencesEntityMapper);

        long userId = 1L;
        String period = "2022";

        when(preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)).thenReturn(Stream.of(new PreferencesEntity()));

        preferencesManager.savePeriod(userId, period);

        verify(preferencesDao).save(any(PreferencesEntity.class));
    }

    @Test
    public void testSavePreferencesWithDirector() {
        PreferencesDao preferencesDao = mock(PreferencesDao.class);
        PreferencesEntityMapper preferencesEntityMapper = mock(PreferencesEntityMapper.class);
        PreferencesManager preferencesManager = new PreferencesManager(preferencesDao, preferencesEntityMapper);

        long userId = 1L;
        long directorId = 10L;

        when(preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)).thenReturn(Stream.of(new PreferencesEntity()));

        preferencesManager.savePreferencesWithDirector(userId, directorId);

        verify(preferencesDao).save(any(PreferencesEntity.class));
    }

    @Test
    public void testSavePreferencesWithActor() {
        PreferencesDao preferencesDao = mock(PreferencesDao.class);
        PreferencesEntityMapper preferencesEntityMapper = mock(PreferencesEntityMapper.class);
        PreferencesManager preferencesManager = new PreferencesManager(preferencesDao, preferencesEntityMapper);

        long userId = 1L;
        long actorId = 20L;

        when(preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)).thenReturn(Stream.of(new PreferencesEntity()));

        preferencesManager.savePreferencesWithActor(userId, actorId);

        verify(preferencesDao).save(any(PreferencesEntity.class));
    }

    @Test
    public void testGetAllSavedPreferences() {
        PreferencesDao preferencesDao = mock(PreferencesDao.class);
        PreferencesEntityMapper preferencesEntityMapper = mock(PreferencesEntityMapper.class);
        PreferencesManager preferencesManager = new PreferencesManager(preferencesDao, preferencesEntityMapper);

        long userId = 1L;

        when(preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)).thenReturn(Stream.of(new PreferencesEntity()));
        when(preferencesEntityMapper.fromDal(any(PreferencesEntity.class))).thenReturn(new Preferences("", "", "", "", 1L, 1L, 1L, 1L));

        Preferences preferences = preferencesManager.getAllSavedPreferences(userId);

        assertNotNull(preferences);
    }
}
