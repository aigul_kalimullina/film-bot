package com.film.bot.unit.preferences.manager.mapper;

import com.film.bot.unit.preferences.dal.entity.PreferencesEntity;
import com.film.bot.unit.preferences.manager.model.Preferences;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PreferencesEntityMapperTest {
    private PreferencesEntityMapper preferencesEntityMapper = new PreferencesEntityMapper();

    @Test
    public void testFromDal() {
        PreferencesEntity preferencesEntity = new PreferencesEntity("1", "Action", "PG-13", "2021", 123L, 456L, 789L, 1634025600L);
        Preferences preferences = preferencesEntityMapper.fromDal(preferencesEntity);

        assertEquals("1", preferences.id());
        assertEquals("Action", preferences.genre());
        assertEquals("PG-13", preferences.rating());
        assertEquals("2021", preferences.period());
        assertEquals(Long.valueOf(123L), preferences.director());
        assertEquals(Long.valueOf(456L), preferences.actor());
        assertEquals(Long.valueOf(789L), preferences.userId());
        assertEquals(Long.valueOf(1634025600L), preferences.createdAt());
    }
}