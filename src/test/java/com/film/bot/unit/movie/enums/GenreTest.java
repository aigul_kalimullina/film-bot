package com.film.bot.unit.movie.enums;

import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GenreTest {

    @Test
    void testGetAlias() {
        assertEquals("аниме", Genre.ANIME.getAlias());
        assertEquals("биография", Genre.BIOGRAPHY.getAlias());
        assertEquals("боевик", Genre.ACTION.getAlias());
        assertEquals("вестерн", Genre.WESTERN.getAlias());
        assertEquals("военный", Genre.MILITARY.getAlias());
        assertEquals("детектив", Genre.DETECTIVE.getAlias());
        assertEquals("детский", Genre.KIDS.getAlias());
        assertEquals("для взрослых", Genre.FOR_ADULTS.getAlias());
        assertEquals("документальный", Genre.DOCUMENTARY.getAlias());
        assertEquals("история", Genre.HISTORY.getAlias());
        assertEquals("игра", Genre.GAME.getAlias());
        assertEquals("комедия", Genre.COMEDY.getAlias());
        assertEquals("концерт", Genre.CONCERT.getAlias());
        assertEquals("короткометражка", Genre.SHORT.getAlias());
        assertEquals("криминал", Genre.CRIMINAL.getAlias());
        assertEquals("мелодрама", Genre.SOAP_DRAMA.getAlias());
        assertEquals("мультфильм", Genre.CARTOON.getAlias());
        assertEquals("мюзикл", Genre.MUSICAL.getAlias());
        assertEquals("новости", Genre.NEWS.getAlias());
        assertEquals("приключения", Genre.ADVENTURE.getAlias());
        assertEquals("реальное ТВ", Genre.REAL_TV.getAlias());
        assertEquals("семейный", Genre.FAMILY.getAlias());
        assertEquals("спорт", Genre.SPORT.getAlias());
        assertEquals("ток-шоу", Genre.TALK_SHOW.getAlias());
        assertEquals("триллер", Genre.THRILLER.getAlias());
        assertEquals("ужасы", Genre.HORROR.getAlias());
        assertEquals("фантастика", Genre.FANTASTIC.getAlias());
        assertEquals("фильм-нуар", Genre.FILM_NOIR.getAlias());
        assertEquals("фэнтези", Genre.FANTASY.getAlias());
    }

    @Test
    void testAllGenresList() {
        List<String> allGenres = Genre.ALL_GENRES;
        assertEquals(30, allGenres.size()); // Проверяем, что список содержит 30 элементов
        assertTrue(allGenres.contains("аниме"));
        assertTrue(allGenres.contains("биография"));
        assertTrue(allGenres.contains("боевик"));
        assertTrue(allGenres.contains("вестерн"));
        assertTrue(allGenres.contains("военный"));
        assertTrue(allGenres.contains("детектив"));
        assertTrue(allGenres.contains("детский"));
        assertTrue(allGenres.contains("для взрослых"));
        assertTrue(allGenres.contains("документальный"));
        assertTrue(allGenres.contains("драма"));
        assertTrue(allGenres.contains("игра"));
        assertTrue(allGenres.contains("история"));
        assertTrue(allGenres.contains("комедия"));
        assertTrue(allGenres.contains("концерт"));
        assertTrue(allGenres.contains("короткометражка"));
        assertTrue(allGenres.contains("криминал"));
        assertTrue(allGenres.contains("мелодрама"));
        assertTrue(allGenres.contains("мультфильм"));
        assertTrue(allGenres.contains("мюзикл"));
        assertTrue(allGenres.contains("новости"));
        assertTrue(allGenres.contains("приключения"));
        assertTrue(allGenres.contains("реальное ТВ"));
        assertTrue(allGenres.contains("семейный"));
        assertTrue(allGenres.contains("спорт"));
        assertTrue(allGenres.contains("ток-шоу"));
        assertTrue(allGenres.contains("триллер"));
        assertTrue(allGenres.contains("ужасы"));
        assertTrue(allGenres.contains("фантастика"));
        assertTrue(allGenres.contains("фильм-нуар"));
        assertTrue(allGenres.contains("фэнтези"));
    }

    @Test
    void testEnumValues() {
        Genre[] genres = Genre.values();
        assertEquals(30, genres.length);
        for (Genre genre : genres) {
            assertTrue(Genre.ALL_GENRES.contains(genre.getAlias()));
        }
    }
}
