package com.film.bot.unit.movie.enums;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RatingTest {

    @Test
    void testGetValue() {
        assertEquals("< 5", Rating.LESS_FIVE.getValue());
        assertEquals("> 5", Rating.MORE_FIVE.getValue());
        assertEquals("> 6", Rating.MORE_SIX.getValue());
        assertEquals("> 7", Rating.MORE_SEVEN.getValue());
        assertEquals("> 8", Rating.MORE_EIGHT.getValue());
        assertEquals("> 9", Rating.MORE_NINE.getValue());
    }

    @Test
    void testGetKpValue() {
        assertEquals("1-3", Rating.LESS_FIVE.getKpValue());
        assertEquals("5-10", Rating.MORE_FIVE.getKpValue());
        assertEquals("6-10", Rating.MORE_SIX.getKpValue());
        assertEquals("7-10", Rating.MORE_SEVEN.getKpValue());
        assertEquals("8-10", Rating.MORE_EIGHT.getKpValue());
        assertEquals("9-10", Rating.MORE_NINE.getKpValue());
    }
}
