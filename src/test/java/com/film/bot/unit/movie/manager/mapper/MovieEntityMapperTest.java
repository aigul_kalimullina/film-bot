package com.film.bot.unit.movie.manager.mapper;

import com.film.bot.unit.movie.dal.entity.MovieEntity;
import com.film.bot.unit.movie.manager.model.Movie;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MovieEntityMapperTest {

    private MovieEntityMapper movieEntityMapper = new MovieEntityMapper();

    @Test
    public void testFromDal() {
        MovieEntity movieEntity = new MovieEntity(1L, "Фильм", "Movie", List.of("Action", "Adventure"), 8.5);
        Movie movie = movieEntityMapper.fromDal(movieEntity);

        assertEquals(Long.valueOf(1L), movie.id());
        assertEquals("Фильм", movie.ruName());
        assertEquals("Movie", movie.enName());
        assertEquals(List.of("Action", "Adventure"), movie.genres());
        assertEquals(8.5, movie.rating(), 0.001);
    }

    @Test
    public void testToDal() {
        Movie movie = new Movie(1L, "Фильм", "Movie", List.of("Action", "Adventure"), 8.5);
        MovieEntity movieEntity = movieEntityMapper.toDal(movie);

        assertEquals(Long.valueOf(1L), movieEntity.getId());
        assertEquals("Фильм", movieEntity.getRuName());
        assertEquals("Movie", movieEntity.getEnName());
        assertEquals(List.of("Action", "Adventure"), movieEntity.getGenres());
        assertEquals(8.5, movieEntity.getRating(), 0.001);
    }

    // Add more test cases as needed

}