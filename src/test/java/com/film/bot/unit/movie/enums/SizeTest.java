package com.film.bot.unit.movie.enums;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SizeTest {

    @Test
    void testGetStart() {
        assertEquals(0.5, Size.FIRST.getStart());
        assertEquals(1.0, Size.SECOND.getStart());
        assertEquals(4.0, Size.THIRD.getStart());
        assertEquals(16.0, Size.FOURTH.getStart());
    }

    @Test
    void testGetEnd() {
        assertEquals(1.0, Size.FIRST.getEnd());
        assertEquals(4.0, Size.SECOND.getEnd());
        assertEquals(16.0, Size.THIRD.getEnd());
        assertNull(Size.FOURTH.getEnd());
    }
}
