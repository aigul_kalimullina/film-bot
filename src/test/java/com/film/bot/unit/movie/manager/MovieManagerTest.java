package com.film.bot.unit.movie.manager;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.film.bot.unit.movie.dal.MovieDao;
import com.film.bot.unit.movie.dal.entity.MovieEntity;
import com.film.bot.unit.movie.manager.mapper.MovieEntityMapper;
import com.film.bot.unit.movie.manager.model.Movie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MovieManagerTest {

    @Mock
    private MovieDao movieDao;

    @Mock
    private MovieEntityMapper movieEntityMapper;

    @InjectMocks
    private MovieManager movieManager;

    @Test
    void testAddMovie() {
        // given
        Movie movie = new Movie(1L, "Test Movie", null, List.of(), 0.0);

        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setId(1L);
        movieEntity.setRuName("Test Movie");

        Mockito.when(movieEntityMapper.toDal(movie)).thenReturn(movieEntity);

        // when
        movieManager.addMovie(movie);

        // then
        Mockito.verify(movieDao).save(movieEntity);
    }

    @Test
    void testFindMoviesByIds() {
        // given
        List<Long> movieIds = Arrays.asList(1L, 2L, 3L);

        MovieEntity movieEntity1 = new MovieEntity();
        movieEntity1.setId(1L);
        movieEntity1.setRuName("Movie 1");

        MovieEntity movieEntity2 = new MovieEntity();
        movieEntity2.setId(2L);
        movieEntity2.setRuName("Movie 2");

        MovieEntity movieEntity3 = new MovieEntity();
        movieEntity3.setId(3L);
        movieEntity3.setRuName("Movie 3");

        List<MovieEntity> movieEntities = Arrays.asList(movieEntity1, movieEntity2, movieEntity3);

        Mockito.when(movieDao.findByIdIn(movieIds)).thenReturn(movieEntities);
        Mockito.when(movieEntityMapper.fromDal(movieEntity1))
                .thenReturn(new Movie(1L, "Movie 1", null, List.of(), 0.0));
        Mockito.when(movieEntityMapper.fromDal(movieEntity2))
                .thenReturn(new Movie(2L, "Movie 2", null, List.of(), 0.0));
        Mockito.when(movieEntityMapper.fromDal(movieEntity3))
                .thenReturn(new Movie(3L, "Movie 3", null, List.of(), 0.0));

        // when
        List<Movie> movies = movieManager.findMoviesByIds(movieIds);

        // then
        assertEquals(3, movies.size());
        assertEquals("Movie 1", movies.get(0).ruName());
        assertEquals("Movie 2", movies.get(1).ruName());
        assertEquals("Movie 3", movies.get(2).ruName());
    }

    @Test
    void testFindByMovieId() {
        // given
        long movieId = 1L;

        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setId(1L);
        movieEntity.setRuName("Test Movie");

        Mockito.when(movieDao.findById(movieId)).thenReturn(Optional.of(movieEntity));
        Mockito.when(movieEntityMapper.fromDal(movieEntity))
                .thenReturn(new Movie(1L, "Test Movie", null, List.of(), 0.0));

        // when
        Movie movie = movieManager.findByMovieId(movieId);

        // then
        assertEquals(1L, movie.id());
        assertEquals("Test Movie", movie.ruName());
    }
}