package com.film.bot.unit.movie.dal.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MovieEntityTest {
    private final Long id = 123l;
    private final String ruName = "ruName";
    private final String enName = "enName";
    private List<String> genres = new ArrayList<>();
    private final double rating = 3.5;
    private final Long new_id = 1234l;
    private final String new_ruName = "ru";
    private final String new_enName = "en";
    private List<String> new_genres = new ArrayList<>();
    private final double new_rating = 5.1;
    private MovieEntity movieEntity = new MovieEntity(id, ruName, enName, genres, rating);

    @Test
    void getId() {
        Assertions.assertEquals(id, movieEntity.getId());
    }

    @Test
    void getRuName() {
        Assertions.assertEquals(ruName, movieEntity.getRuName());
    }

    @Test
    void getEnName() {
        Assertions.assertEquals(enName, movieEntity.getEnName());
    }

    @Test
    void getGenres() {
        Assertions.assertEquals(genres, movieEntity.getGenres());
    }

    @Test
    void getRating() {
        Assertions.assertEquals(rating, movieEntity.getRating());
    }

    @Test
    void setId() {
        movieEntity.setId(new_id);
        Assertions.assertEquals(new_id, movieEntity.getId());
    }

    @Test
    void setRuName() {
        movieEntity.setRuName(new_ruName);
        Assertions.assertEquals(new_ruName, movieEntity.getRuName());
    }

    @Test
    void setEnName() {
        movieEntity.setEnName(new_enName);
        Assertions.assertEquals(new_enName, movieEntity.getEnName());
    }

    @Test
    void setGenres() {
        new_genres.add("123");
        movieEntity.setGenres(new_genres);
        Assertions.assertEquals(new_genres, movieEntity.getGenres());
    }

    @Test
    void setRating() {
        movieEntity.setRating(new_rating);
        Assertions.assertEquals(new_rating, movieEntity.getRating());
    }

    @Test
    void testEquals() {
        MovieEntity testMovieEntity = new MovieEntity(id, ruName, enName, genres, rating);
        assertEquals(movieEntity, testMovieEntity);
    }

    @Test
    void canEqual() {
        MovieEntity testMovieEntity = new MovieEntity(id, ruName, enName, genres, rating);
        assertTrue(testMovieEntity.canEqual(movieEntity));
    }

    @Test
    void testHashCode() {
        assertDoesNotThrow(()->movieEntity.hashCode());
    }

    @Test
    void testToString() {
        assertDoesNotThrow(()->movieEntity.toString());
    }

    @Test
    void builder() {
        assertDoesNotThrow(MovieEntity::builder);
    }
    @Test
    void noArgsContructor() {
        assertDoesNotThrow(()-> new MovieEntity());
    }
}