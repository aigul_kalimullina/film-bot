package com.film.bot.service;

import java.util.HashSet;
import java.util.List;

import com.film.bot.service.enums.PreferencesFunction;
import com.film.bot.unit.kinopoisk.model.Person;
import com.film.bot.unit.kinopoisk.model.response.PersonsResponse;
import com.film.bot.unit.preferences.manager.PreferencesManager;
import com.film.bot.unit.preferences.manager.model.Preferences;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PreferencesServiceTest {

    @Mock
    private PreferencesManager preferencesManager;
    @Mock
    private KinopoiskService kinopoiskService;
    @Mock
    private BotMessageGenerator botMessageGenerator;

    private PreferencesService underTest;

    @BeforeEach
    public void init() {
        underTest = new PreferencesService(
                List.of("1890-1940", "1940-1980", "1980-1990", "1990-2000", "2000-2010", "2010-2020", "2020-2023"),
                preferencesManager,
                kinopoiskService,
                botMessageGenerator
        );
    }

    @Test
    public void preferencesMethodsTest() {
        List<Preferences> preferences = List.of(
                getPreferences("1"),
                getPreferences("1"),
                getPreferences("1"),
                getPreferences("2")
        );

        assertThat(new HashSet<>(preferences)).hasSize(2);
        assertThat(getPreferences("1")).isNotEqualTo("2");
    }

    @Test
    public void testStartSearchByGenres() {
        SendMessage message = new SendMessage();
        underTest.startSearchByGenres(message);
        verify(botMessageGenerator).generateGenres(message, "Preferences_" + PreferencesFunction.GENRE + "_");
    }

    @Test
    public void testWriteChosenGenre() {
        underTest.writeChosenGenre(1L, "Action");
        verify(preferencesManager).saveGenre(1L, "Action");
    }

    @Test
    public void testSearchByRating() {
        SendMessage message = new SendMessage();
        underTest.searchByRating(message);

        assertThat(message.getText()).isEqualTo("Выбери минимальный рейтинг фильма:");
    }

    @Test
    public void testWriteChosenRating() {
        underTest.writeChosenRating(1L, "7");
        verify(preferencesManager).saveRating(1L, "7");
    }

    @Test
    public void testSearchByPeriods() {
        SendMessage message = new SendMessage();
        underTest.searchByPeriods(message);
        assertThat(message.getText()).isEqualTo("Выбери период выпуска:");
    }

    @Test
    public void testWriteChosenPeriod() {
        underTest.writeChosenPeriod(1L, "2000-2010");
        verify(preferencesManager).savePeriod(1L, "2000-2010");
    }

    @Test
    public void testSearchByDirector() {
        SendMessage message = new SendMessage();
        underTest.searchByDirector(message);
        assertThat(message.getText()).isEqualTo("Введи данные режиссера с клавиатуры:");
    }

    @Test
    public void testWriteChosenDirector() {
        underTest.writeChosenDirector(1L, 100L);
        verify(preferencesManager).savePreferencesWithDirector(1L, 100L);
    }

    @Test
    public void testSearchByActor() {
        SendMessage message = new SendMessage();
        underTest.searchByActor(message);
        assertThat(message.getText()).isEqualTo("Введи данные актера с клавиатуры:");
    }

    @Test
    public void testWriteChosenActor() {
        underTest.writeChosenActor(1L, 200L);
        verify(preferencesManager).savePreferencesWithActor(1L, 200L);
    }

    @Test
    public void testFullSearch() {
        Preferences preferences = getPreferences("1");

        when(preferencesManager.getAllSavedPreferences(anyLong())).thenReturn(preferences);

        SendMessage message = new SendMessage();

        underTest.fullSearch(123456, message);

        verify(botMessageGenerator).generateMovieInfoMessage(any(), eq("По твоим предпочтениям ничего не нашлось"), eq("Фильмы по твоим предпочтениям:"), eq(message));
    }

    @Test
    public void testFindPerson() {
        PersonsResponse personsResponse = new PersonsResponse();
        personsResponse.setPersons(List.of(new Person(30L, "John Doe", null, 32)));

        when(kinopoiskService.findPersonByName(anyString())).thenReturn(personsResponse);

        SendMessage message = Mockito.mock(SendMessage.class);
        PreferencesFunction function = PreferencesFunction.ACTOR;
        String currentText = "John Doe";

        underTest.findPerson(message, currentText, function);

        verify(message).setText("Выбери персону: ");
        verify(kinopoiskService).findPersonByName(eq(currentText));
        verify(message, times(1)).setReplyMarkup(any(InlineKeyboardMarkup.class));
    }


    private static Preferences getPreferences(String id) {
        return new Preferences(
                id,
                "Action",
                "8.0",
                "2020",
                456L,
                -1L,
                123L,
                123L
        );
    }
}