package com.film.bot.service;

import java.util.stream.Stream;

import com.film.bot.unit.user.manager.UserManager;
import com.film.bot.unit.user.manager.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.objects.Update;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserManager userManager;

    @InjectMocks
    private UserService userService;

    @Test
    public void testSaveUserInteraction() {
        Update update = new Update();
        userService.saveUserInteraction(update);
        Mockito.verify(userManager, Mockito.times(1)).saveInteraction(update);
    }

    @Test
    public void testFindUserHistory() {
        long userId = 123;
        Stream<Update> expectedStream = Stream.of(new Update());
        Mockito.when(userManager.findUserInteractions(userId)).thenReturn(expectedStream);

        Stream<Update> resultStream = userService.findUserHistory(userId);

        Assertions.assertEquals(expectedStream, resultStream);
    }

    @Test
    public void testInsert() {
        User user = new User(134L, "login");
        Mockito.doNothing().when(userManager).insert(user);

        userService.insert(user);

        Mockito.verify(userManager, Mockito.times(1)).insert(user);
    }

    @Test
    public void testExistsByUsername() {
        String username = "test_user";
        Mockito.when(userManager.exists(username)).thenReturn(true);

        boolean result = userService.existsByUsername(username);

        Assertions.assertTrue(result);
    }
}
