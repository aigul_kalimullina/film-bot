package com.film.bot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.kinopoisk.model.PersonProfession;
import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.kinopoisk.model.response.MoviesResponse;
import com.film.bot.unit.kinopoisk.model.response.PersonsResponse;
import com.film.bot.unit.preferences.manager.model.Preferences;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KinopoiskServiceTest {

    @Mock
    ApiRequestPerformer apiRequestPerformer;

    @InjectMocks
    KinopoiskService underTest;

    @Captor
    ArgumentCaptor<String> urlCaptor;

    @Captor
    ArgumentCaptor<Class<?>> classArgumentCaptor;


    @Test
    void findPersonByName() {
        String queryName = "valid";
        assertThatCode(() -> underTest.findPersonByName(queryName))
                .doesNotThrowAnyException();

        Mockito.verify(apiRequestPerformer)
                .generateRequest(urlCaptor.capture(), classArgumentCaptor.capture());

        assertThat(urlCaptor.getValue()).isEqualTo("/v1.4/person/search?page=1&limit=200&query=" + queryName);
        assertThat(classArgumentCaptor.getValue()).isEqualTo(PersonsResponse.class);
    }

    @Test
    void searchMovieByName() {
        String queryName = "valid";
        assertThatCode(() -> underTest.searchMovieByName(queryName))
                .doesNotThrowAnyException();

        Mockito.verify(apiRequestPerformer)
                .generateRequest(urlCaptor.capture(), classArgumentCaptor.capture());

        assertThat(urlCaptor.getValue()).isEqualTo("/v1.4/movie/search?page=1&limit=5&query=" + queryName);
        assertThat(classArgumentCaptor.getValue()).isEqualTo(MoviesResponse.class);
    }

    @MethodSource
    @ParameterizedTest
    void findMovieByFilters(String genre) {

        assertThatCode(() -> underTest.findMovieByFilters(genre))
                .doesNotThrowAnyException();

        Mockito.verify(apiRequestPerformer)
                .generateRequest(urlCaptor.capture(), classArgumentCaptor.capture());

        assertThat(urlCaptor.getValue()).startsWith("/v1.4/movie?page=1&limit=10");
        assertThat(classArgumentCaptor.getValue()).isEqualTo(MoviesResponse.class);
    }

    public static Stream<Arguments> findMovieByFilters() {
        return Stream.of(
                Arguments.of("validGenre"),
                Arguments.of(""),
                Arguments.of((String) null)
        );
    }

    @ParameterizedTest
    @CsvSource({"1", "2000", "50000"})
    void findMovieById(Long id) {
        assertThatCode(() -> underTest.findMovieById(id))
                .doesNotThrowAnyException();

        Mockito.verify(apiRequestPerformer)
                .generateRequest(urlCaptor.capture(), classArgumentCaptor.capture());
        assertThat(urlCaptor.getValue()).startsWith("/v1.4/movie/" + id);
        assertThat(classArgumentCaptor.getValue()).isEqualTo(MovieResponse.class);

    }

    @Test
    void findTop10Movies() {
        assertThatCode(() -> underTest.findTop10Movies())
                .doesNotThrowAnyException();

        Mockito.verify(apiRequestPerformer)
                .generateRequest(urlCaptor.capture(), classArgumentCaptor.capture());

        assertThat(urlCaptor.getValue()).startsWith("/v1.4/movie?page=1&limit=10");
        assertThat(classArgumentCaptor.getValue()).isEqualTo(MoviesResponse.class);
    }

    @Test
    void findRandomMovie() {
        assertThatCode(() -> underTest.findRandomMovie())
                .doesNotThrowAnyException();

        Mockito.verify(apiRequestPerformer)
                .generateRequest(urlCaptor.capture(), classArgumentCaptor.capture());

        assertThat(urlCaptor.getValue()).startsWith("/v1.4/movie/random");
        assertThat(classArgumentCaptor.getValue()).isEqualTo(MovieResponse.class);
    }

    @MethodSource
    @ParameterizedTest
    public void testFindByPreferences(Preferences preferences, List<Integer> expectedIds, String expectedUrl) {
        List<Film> films = new ArrayList<>();
        Film film1 = new Film();
        film1.setId(1);
        film1.setPersons(List.of(
                new PersonProfession(123L, "актеры"),
                new PersonProfession(456L, "режиссеры"))
        );
        films.add(film1);

        Film film2 = new Film();
        film2.setPersons(List.of());
        film2.setId(2);
        film2.setPersons(List.of(
                new PersonProfession(123L, "актеры"),
                new PersonProfession(456L, "режиссеры"))
        );
        films.add(film2);

        MoviesResponse response = new MoviesResponse();
        response.setFilms(films);
        response.setFilms(films);

        when(apiRequestPerformer.generateRequest(urlCaptor.capture(), eq(MoviesResponse.class))).thenReturn(response);

        List<Film> result = underTest.findByPreferences(preferences);

        for (int i = 0; i < expectedIds.size(); i++) {
            assertEquals(expectedIds.get(i), result.get(i).getId());
        }
        Mockito.verify(apiRequestPerformer)
                .generateRequest(urlCaptor.capture(), classArgumentCaptor.capture());

        assertThat(urlCaptor.getValue()).isEqualTo(expectedUrl);
        assertThat(classArgumentCaptor.getValue()).isEqualTo(MoviesResponse.class);
    }

    public static Stream<Arguments> testFindByPreferences() {
        return Stream.of(
                Arguments.of(
                        new Preferences(
                                "123",
                                "Action",
                                "8.0",
                                "2021",
                                456L,
                                123L,
                                123L,
                                123L
                        ),
                        List.of(1, 2),
                        "/v1.4/movie?page=1&limit=20&genres.name=Action&persons.id=123&persons.profession=актер&persons.id=456&persons.profession=режиссер&year=2021&rating.kp=8.0&selectFields=persons&selectFields=id&selectFields=name&selectFields=alternativeName&selectFields=enName&selectFields=type&selectFields=year&selectFields=description&selectFields=shortDescription&selectFields=movieLength&selectFields=isSeries&selectFields=ticketsOnSale&selectFields=totalSeriesLength&selectFields=seriesLength&selectFields=ratingMpaa&selectFields=ageRating&selectFields=top10&selectFields=top250&selectFields=typeNumber&selectFields=status&selectFields=names&selectFields=logo&selectFields=poster&selectFields=backdrop&selectFields=rating&selectFields=votes&selectFields=genres&selectFields=countries&selectFields=releaseYears"
                ),
                Arguments.of(
                        new Preferences(
                                "123",
                                "Action",
                                "8.0",
                                "2021",
                                456L,
                                -1L,
                                123L,
                                123L
                        ),
                        List.of(1, 2),
                        "/v1.4/movie?page=1&limit=20&genres.name=Action&persons.id=456&persons.profession=режиссер&year=2021&rating.kp=8.0&selectFields=persons&selectFields=id&selectFields=name&selectFields=alternativeName&selectFields=enName&selectFields=type&selectFields=year&selectFields=description&selectFields=shortDescription&selectFields=movieLength&selectFields=isSeries&selectFields=ticketsOnSale&selectFields=totalSeriesLength&selectFields=seriesLength&selectFields=ratingMpaa&selectFields=ageRating&selectFields=top10&selectFields=top250&selectFields=typeNumber&selectFields=status&selectFields=names&selectFields=logo&selectFields=poster&selectFields=backdrop&selectFields=rating&selectFields=votes&selectFields=genres&selectFields=countries&selectFields=releaseYears"
                ),
                Arguments.of(
                        new Preferences(
                                "123",
                                "Action",
                                "8.0",
                                "2021",
                                -1L,
                                123L,
                                123L,
                                123L
                        ),
                        List.of(1, 2),
                        "/v1.4/movie?page=1&limit=20&genres.name=Action&persons.id=123&persons.profession=актер&year=2021&rating.kp=8.0&selectFields=persons&selectFields=id&selectFields=name&selectFields=alternativeName&selectFields=enName&selectFields=type&selectFields=year&selectFields=description&selectFields=shortDescription&selectFields=movieLength&selectFields=isSeries&selectFields=ticketsOnSale&selectFields=totalSeriesLength&selectFields=seriesLength&selectFields=ratingMpaa&selectFields=ageRating&selectFields=top10&selectFields=top250&selectFields=typeNumber&selectFields=status&selectFields=names&selectFields=logo&selectFields=poster&selectFields=backdrop&selectFields=rating&selectFields=votes&selectFields=genres&selectFields=countries&selectFields=releaseYears"
                ),
                Arguments.of(
                        new Preferences(
                                "123",
                                "Action",
                                "8.0",
                                "2021",
                                -1L,
                                -1L,
                                123L,
                                123L
                        ),
                        List.of(1, 2),
                        "/v1.4/movie?page=1&limit=20&genres.name=Action&year=2021&rating.kp=8.0&selectFields=persons&selectFields=id&selectFields=name&selectFields=alternativeName&selectFields=enName&selectFields=type&selectFields=year&selectFields=description&selectFields=shortDescription&selectFields=movieLength&selectFields=isSeries&selectFields=ticketsOnSale&selectFields=totalSeriesLength&selectFields=seriesLength&selectFields=ratingMpaa&selectFields=ageRating&selectFields=top10&selectFields=top250&selectFields=typeNumber&selectFields=status&selectFields=names&selectFields=logo&selectFields=poster&selectFields=backdrop&selectFields=rating&selectFields=votes&selectFields=genres&selectFields=countries&selectFields=releaseYears"
                ),
                Arguments.of(
                        new Preferences(
                                "123",
                                null,
                                null,
                                null,
                                -1L,
                                -1L,
                                123L,
                                123L
                        ),
                        List.of(1, 2),
                        "/v1.4/movie?page=1&limit=20&selectFields=persons&selectFields=id&selectFields=name&selectFields=alternativeName&selectFields=enName&selectFields=type&selectFields=year&selectFields=description&selectFields=shortDescription&selectFields=movieLength&selectFields=isSeries&selectFields=ticketsOnSale&selectFields=totalSeriesLength&selectFields=seriesLength&selectFields=ratingMpaa&selectFields=ageRating&selectFields=top10&selectFields=top250&selectFields=typeNumber&selectFields=status&selectFields=names&selectFields=logo&selectFields=poster&selectFields=backdrop&selectFields=rating&selectFields=votes&selectFields=genres&selectFields=countries&selectFields=releaseYears"
                )
        );
    }

}