package com.film.bot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.film.bot.unit.favourites.manager.FavouritesManager;
import com.film.bot.unit.favourites.manager.model.Favourites;
import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.movie.manager.MovieManager;
import com.film.bot.unit.user.manager.UserManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import com.film.bot.unit.movie.manager.model.Movie;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FavouritesServiceTest {

    @Mock
    private KinopoiskService kinopoiskService;

    @Mock
    private MovieManager movieManager;

    @Mock
    private MovieService movieService;

    @Mock
    private FavouritesManager favouritesManager;

    @Mock
    private UserManager userManager;

    @InjectMocks
    private FavouritesService favouritesService;

    @Test
    public void testAddMovieToFavourites() {
        long userId = 1L;
        SendMessage message = new SendMessage();
        Update update = new Update();
        CallbackQuery callbackQuery = new CallbackQuery();
        callbackQuery.setData("Info_123");
        update.setCallbackQuery(callbackQuery);

        when(userManager.findUserInteractions(userId)).thenReturn(Stream.of(update));
        when(kinopoiskService.findMovieById(123L)).thenReturn(new MovieResponse());

        favouritesService.addMovieToFavourites(userId, message);

        verify(favouritesManager, times(1)).addToFavourites(new Favourites(userId, 123L));
    }

    @Test
    public void testGetUserFavouriteMovies() {
        long userId = 1L;
        SendMessage message = new SendMessage();

        when(favouritesManager.findFavouriteMoviesByUserId(userId)).thenReturn(new ArrayList<>());

        favouritesService.getUserFavouriteMovies(userId, message);

        assertThat(message.getText()).isEqualTo("Ни один фильм еще не был добавлен в Избранное");
    }

    @Test
    public void testGetFavouriteMovieInfo() {
        long movieId = 1L;
        SendMessage message = new SendMessage();
        Movie movie = new Movie(1L, "Movie", "Movie", List.of(), 5.0);

        when(favouritesManager.findFavouriteMovieInfo(movieId)).thenReturn(movie);

        favouritesService.getFavouriteMovieInfo(movieId, message);

        verify(movieService, times(1)).getLinksByMovieId(message, movieId);
    }
}
