package com.film.bot.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.film.bot.unit.kinopoisk.model.Country;
import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.kinopoisk.model.Genre;
import com.film.bot.unit.kinopoisk.model.PersonProfession;
import com.film.bot.unit.kinopoisk.model.Rating;
import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.kinopoisk.model.response.MoviesResponse;
import com.film.bot.unit.rutracker.RuTrackerSearch;
import com.film.bot.unit.rutracker.Torrent;
import com.film.bot.unit.rutracker.enums.Filters;
import com.film.bot.unit.user.manager.UserManager;
import org.jsoup.HttpStatusException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class MovieServiceTest {

    @Mock
    private KinopoiskService kinopoiskService;
    @Mock
    private BotMessageGenerator botMessageGenerator;
    @Mock
    private RuTrackerSearch ruTrackerSearch;
    @Mock
    private UserManager userManager;

    @InjectMocks
    private MovieService underTestService;

    @Captor
    ArgumentCaptor<String> stringCaptor;

    @Test
    void getMovieGenres() {
        SendMessage message = new SendMessage();
        underTestService.getMovieGenres(message);
        assertThat(message.getText()).isEqualTo("Выбери жанр:");
    }

    @MethodSource
    @ParameterizedTest
    void getRandomMovie(MovieResponse movie, String messageText, String buttonText) {
        Mockito.when(kinopoiskService.findRandomMovie()).thenReturn(movie);
        SendMessage message = new SendMessage();
        underTestService.getRandomMovie(message);
        assertThat(message.getText()).isEqualTo(messageText);
        InlineKeyboardMarkup replyMarkup = (InlineKeyboardMarkup) message.getReplyMarkup();
        if (replyMarkup != null) {
            assertThat(replyMarkup.getKeyboard().get(0).get(0).getText()).isEqualTo(buttonText);
        }
    }

    public static Stream<Arguments> getRandomMovie() {
        return Stream.of(
                Arguments.of(
                        MovieResponse.builder()
                                .id(1)
                                .name("name")
                                .build(),
                        "Полученный фильм:",
                        "name"
                ),
                Arguments.of(
                        MovieResponse.builder()
                                .id(1)
                                .enName("enname")
                                .build(),
                        "Полученный фильм:",
                        "enname"
                ),
                Arguments.of(
                        MovieResponse.builder()
                                .id(1)
                                .alternativeName("alternativename")
                                .build(),
                        "Полученный фильм:",
                        "alternativename"
                ),
                Arguments.of(
                        MovieResponse.builder()
                                .id(1)
                                .build(),
                        "Полученный фильм:",
                        null
                ),
                Arguments.of(
                        null,
                        "Фильм не был найден",
                        null
                )
        );
    }

    @Test
    void getPopularMovies() {
        Mockito.when(kinopoiskService.findTop10Movies()).thenReturn(MoviesResponse.builder().films(List.of()).build());
        underTestService.getPopularMovies(new SendMessage());
        Mockito.verify(kinopoiskService).findTop10Movies();
        Mockito.verify(botMessageGenerator).generateMovieInfoMessage(any(), any(), any(), any());
    }

    @MethodSource
    @ParameterizedTest
    void getRichMovieInfo(MovieResponse movie, String expectedText) {
        SendMessage sendMessage = new SendMessage();
        Mockito.when(kinopoiskService.findMovieById(anyLong())).thenReturn(movie);

        underTestService.getRichMovieInfo(1L, sendMessage);
        assertThat(sendMessage.getText()).isEqualTo(expectedText);
    }

    public static Stream<Arguments> getRichMovieInfo() {
        return Stream.of(
                Arguments.of(
                        MovieResponse.builder().build(),
                        null
                ),
                Arguments.of(
                        null,
                        "Фильм не был найден"
                )
        );
    }

    @Test
    void getMovieByGenre() {
        Mockito.when(kinopoiskService.findMovieByFilters(any())).thenReturn(MoviesResponse.builder().films(List.of()).build());
        underTestService.getMovieByGenre("genre", new SendMessage());
        Mockito.verify(kinopoiskService).findMovieByFilters(any());
        Mockito.verify(botMessageGenerator).generateMovieInfoMessage(any(), any(), any(), any());
    }


    @MethodSource
    @ParameterizedTest
    void getMovieInfoBySearch(String text, String filmName, List<Film> films) {
        Mockito.when(kinopoiskService.searchMovieByName(any())).thenReturn(MoviesResponse.builder().films(films).build());

        SendMessage sendMessage = new SendMessage();

        underTestService.getMovieInfoBySearch(sendMessage, "searchString");

        if (filmName != null) {
            Mockito.verify(botMessageGenerator).initMovieMessage(stringCaptor.capture(), any());
            assertThat(stringCaptor.getValue()).contains(filmName);
        }
        assertThat(sendMessage.getText()).isEqualTo(text);
    }

    public static Stream<Arguments> getMovieInfoBySearch() {
        return Stream.of(
                Arguments.of(
                        "Фильм по запросу не был найден!",
                        null,
                        List.of()
                ),
                Arguments.of(
                        null,
                        "name",
                        List.of(
                                new Film(
                                        1,
                                        "name",
                                        "altname",
                                        "enname",
                                        1,
                                        "name",
                                        1,
                                        List.of(new PersonProfession(10L, "artist")),
                                        new Rating(7.4),
                                        List.of(new Genre("anime")),
                                        List.of(new Country("Russia")),
                                        18
                                ))
                ),
                Arguments.of(
                        null,
                        "altname",
                        List.of(
                                new Film(
                                        1,
                                        null,
                                        "altname",
                                        "enname",
                                        1,
                                        "name",
                                        1,
                                        List.of(new PersonProfession(10L, "artist")),
                                        new Rating(7.4),
                                        List.of(new Genre("anime")),
                                        List.of(new Country("Russia")),
                                        18
                                ))
                ),
                Arguments.of(
                        null,
                        "enname",
                        List.of(
                                new Film(
                                        1,
                                        null,
                                        null,
                                        "enname",
                                        1,
                                        "name",
                                        1,
                                        List.of(new PersonProfession(10L, "artist")),
                                        new Rating(7.4),
                                        List.of(new Genre("anime")),
                                        List.of(new Country("Russia")),
                                        18
                                ))
                ),
                Arguments.of(
                        null,
                        "name",
                        List.of(
                                new Film(
                                        1,
                                        "name",
                                        null,
                                        null,
                                        1,
                                        "name",
                                        1,
                                        List.of(new PersonProfession(10L, "artist")),
                                        new Rating(7.4),
                                        List.of(new Genre("anime")),
                                        List.of(new Country("Russia")),
                                        18
                                ))
                ),
                Arguments.of(
                        null,
                        "name",
                        List.of(
                                new Film(
                                        1,
                                        "name",
                                        "altName",
                                        null,
                                        1,
                                        "name",
                                        1,
                                        List.of(new PersonProfession(10L, "artist")),
                                        new Rating(7.4),
                                        List.of(new Genre("anime")),
                                        List.of(new Country("Russia")),
                                        18
                                ))
                )
        );
    }

    @Test
    void getLinks() {
        SendMessage message = new SendMessage();
        Mockito.when(userManager.findLastMovie(anyLong())).thenReturn(1L);

        assertThatCode(() -> underTestService.getLinks(1L, message)).doesNotThrowAnyException();
    }

    @Test
    void getLinksByMovieId() {
        assertThatCode(() -> underTestService.getLinksByMovieId(new SendMessage(), 1L)).doesNotThrowAnyException();
    }

    @MethodSource
    @ParameterizedTest
    void getDownloadLinkWithFilters(List<Torrent> torrents, String expectedText) throws Exception {
        Mockito.when(kinopoiskService.findMovieById(anyLong()))
                .thenReturn(
                        MovieResponse
                                .builder()
                                .name("name")
                                .build()
                );

        Mockito.when(ruTrackerSearch.search(any())).thenReturn(torrents);
        lenient().when(ruTrackerSearch.seedersFilter(any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);
        lenient().when(ruTrackerSearch.ratingFilter(any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);
        lenient().when(ruTrackerSearch.offerBestOne(any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);
        lenient().when(ruTrackerSearch.sizeFilter(any(), any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);

        SendMessage sendMessage = new SendMessage();
        underTestService.getDownloadLinkWithFilters(1L, sendMessage, Filters.FIRST);

        assertThat(sendMessage.getText()).isEqualTo(expectedText);
    }

    public static Stream<Arguments> getDownloadLinkWithFilters() {
        return Stream.of(
                Arguments.of(
                        List.of(),
                        "По данным параметрам не найден ни один торрент"
                ),
                Arguments.of(
                        List.of(Mockito.mock(Torrent.class)),
                        "Ссылка на скачивание: null\n" +
                                "Ссылка на простой поиск торрента: null"
                )
        );
    }

    @MethodSource
    @ParameterizedTest
    void getDownloadLinkWithException(Exception e, String expectedText) throws Exception {
        Mockito.when(kinopoiskService.findMovieById(anyLong()))
                .thenReturn(
                        MovieResponse
                                .builder()
                                .name("name")
                                .build()
                );

        Mockito.when(ruTrackerSearch.search(any())).thenThrow(e);

        SendMessage sendMessage = new SendMessage();
        underTestService.getDownloadLinkWithFilters(1L, sendMessage, Filters.FIRST);

        assertThat(sendMessage.getText()).isEqualTo(expectedText);
    }

    public static Stream<Arguments> getDownloadLinkWithException() {
        return Stream.of(
                Arguments.of(
                        new HttpStatusException("message", 400, "url"),
                        "Проблемы с сервисом rutracher\n" +
                                "Ссылка на простой поиск торрента: null"
                ),
                Arguments.of(
                        new RuntimeException("message"),
                        "По данным параметрам не найдена ни одна ссылка на скачивание\n" +
                                "Ссылка на простой поиск торрента: null"
                )
        );
    }

    @Test
    void getDownloadLinkWithExceptionInDownload() throws IOException {
        Mockito.when(kinopoiskService.findMovieById(anyLong()))
                .thenReturn(
                        MovieResponse
                                .builder()
                                .alternativeName("name")
                                .year(100)
                                .build()
                );

        Mockito.when(ruTrackerSearch.search(any())).thenThrow(new RuntimeException()).thenReturn(List.of());

        SendMessage sendMessage = new SendMessage();
        underTestService.getDownloadLinkWithFilters(1L, sendMessage, Filters.FIRST);

        assertThat(sendMessage.getText()).isEqualTo("По данным параметрам не найден ни один торрент");
    }

    @MethodSource
    @ParameterizedTest
    void getDownloadLinkWithDifferentFilters(List<Torrent> torrents, String expectedText, Filters filters) throws Exception {
        Mockito.when(kinopoiskService.findMovieById(anyLong()))
                .thenReturn(
                        MovieResponse
                                .builder()
                                .name("name")
                                .build()
                );

        Mockito.when(ruTrackerSearch.search(any())).thenReturn(torrents);
        lenient().when(ruTrackerSearch.seedersFilter(any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);
        lenient().when(ruTrackerSearch.ratingFilter(any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);
        lenient().when(ruTrackerSearch.offerBestOne(any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);
        lenient().when(ruTrackerSearch.sizeFilter(any(), any())).thenReturn(torrents.size() != 0 ? torrents.get(0) : null);

        SendMessage sendMessage = new SendMessage();
        underTestService.getDownloadLinkWithFilters(1L, sendMessage, filters);

        assertThat(sendMessage.getText()).isEqualTo(expectedText);
    }

    public static Stream<Arguments> getDownloadLinkWithDifferentFilters() {
        return Arrays.stream(Filters.values()).map(filter ->
                Arguments.of(
                        List.of(Mockito.mock(Torrent.class)),
                        "Ссылка на скачивание: null\n" +
                                "Ссылка на простой поиск торрента: null",
                        filter
                )
        );
    }
}