package com.film.bot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.film.bot.unit.kinopoisk.model.Country;
import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.kinopoisk.model.Genre;
import com.film.bot.unit.kinopoisk.model.PersonProfession;
import com.film.bot.unit.kinopoisk.model.Rating;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

@ExtendWith(MockitoExtension.class)
public class BotMessageGeneratorTest {

    @InjectMocks
    private BotMessageGenerator botMessageGenerator;

    @Mock
    private SendMessage message;

    @Test
    public void testGenerateGenres() {
        String callbackText = "test_callback_";
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        button1.setText("Action");
        button1.setCallbackData(callbackText + "Action");
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        button2.setText("Adventure");
        button2.setCallbackData(callbackText + "Adventure");

        List<List<InlineKeyboardButton>> expectedRowList = new ArrayList<>();
        expectedRowList.add(Arrays.asList(button1, button2));

        InlineKeyboardButton anyGenreButton = new InlineKeyboardButton();
        anyGenreButton.setText("любой жанр");
        anyGenreButton.setCallbackData(callbackText);
        expectedRowList.add(List.of(anyGenreButton));

        InlineKeyboardMarkup expectedInlineKeyboardMarkup = new InlineKeyboardMarkup();
        expectedInlineKeyboardMarkup.setKeyboard(expectedRowList);

        botMessageGenerator.generateGenres(message, callbackText);

        Mockito.verify(message).setReplyMarkup(ArgumentMatchers.any());
    }

    @Test
    public void testGeneratePreferencesButtons() {
        String callbackText = "test_callback_";
        List<List<InlineKeyboardButton>> existingButtons = new ArrayList<>();
        InlineKeyboardButton dontCareButton = new InlineKeyboardButton();
        dontCareButton.setText("Не важно");
        dontCareButton.setCallbackData(callbackText);
        existingButtons.add(List.of(dontCareButton));

        InlineKeyboardMarkup expectedInlineKeyboardMarkup = new InlineKeyboardMarkup();
        expectedInlineKeyboardMarkup.setKeyboard(existingButtons);

        botMessageGenerator.generatePreferencesButtons(message, callbackText, existingButtons);

        Mockito.verify(message).setReplyMarkup(expectedInlineKeyboardMarkup);
    }

    @Test
    public void testGeneratePreferencesSize() {
        BotMessageGenerator botMessageGenerator = new BotMessageGenerator();
        SendMessage message = new SendMessage();
        String callbackText = "Preference_";
        List<List<InlineKeyboardButton>> buttonsKeyboard = new ArrayList<>();

        botMessageGenerator.generatePreferencesButtons(message, callbackText, buttonsKeyboard);

        InlineKeyboardMarkup inlineKeyboardMarkup = (InlineKeyboardMarkup) message.getReplyMarkup();
        List<List<InlineKeyboardButton>> keyboard = inlineKeyboardMarkup.getKeyboard();

        Assertions.assertEquals(1, keyboard.size());
    }

    @Test
    public void testGenerateMovieInfoMessage() {
        BotMessageGenerator botMessageGenerator = new BotMessageGenerator();
        List<Film> films = new ArrayList<>();
        films.add(createFilm(1, "name"));
        films.add(createFilm(2, "name2"));
        String emptyMessage = "No films found.";
        String baseMessage = "Choose a movie:";
        SendMessage message = new SendMessage();

        botMessageGenerator.generateMovieInfoMessage(films, emptyMessage, baseMessage, message);

        Assertions.assertEquals(baseMessage, message.getText());
        InlineKeyboardMarkup inlineKeyboardMarkup = (InlineKeyboardMarkup) message.getReplyMarkup();
        List<List<InlineKeyboardButton>> keyboard = inlineKeyboardMarkup.getKeyboard();

        Assertions.assertEquals(2, keyboard.size());
    }

    @Test
    public void testGenerateMovieInfoMessage_EmptyFilms() {
        BotMessageGenerator botMessageGenerator = new BotMessageGenerator();
        List<Film> films = new ArrayList<>();
        String emptyMessage = "No films found.";
        String baseMessage = "Choose a movie:";
        SendMessage message = new SendMessage();

        botMessageGenerator.generateMovieInfoMessage(films, emptyMessage, baseMessage, message);

        Assertions.assertEquals(emptyMessage, message.getText());
        InlineKeyboardMarkup inlineKeyboardMarkup = (InlineKeyboardMarkup) message.getReplyMarkup();
        Assertions.assertNull(inlineKeyboardMarkup); // No reply markup for empty films
    }

    @Test
    public void testGenerateMovieInfoMessage_NonEmptyFilms() {
        BotMessageGenerator botMessageGenerator = new BotMessageGenerator();
        List<Film> films = new ArrayList<>();
        films.add(createFilm(1, "name"));
        films.add(createFilm(2, null)); // Film with null name
        String emptyMessage = "No films found.";
        String baseMessage = "Choose a movie:";
        SendMessage message = new SendMessage();

        botMessageGenerator.generateMovieInfoMessage(films, emptyMessage, baseMessage, message);

        Assertions.assertEquals(baseMessage, message.getText());
        InlineKeyboardMarkup inlineKeyboardMarkup = (InlineKeyboardMarkup) message.getReplyMarkup();
        List<List<InlineKeyboardButton>> keyboard = inlineKeyboardMarkup.getKeyboard();

        Assertions.assertEquals(1, keyboard.size()); // Only one film with non-null name
        Assertions.assertEquals("name", keyboard.get(0).get(0).getText());
        Assertions.assertEquals("Info_1", keyboard.get(0).get(0).getCallbackData());
    }

    @Test
    public void testInitMovieMessage() {
        String text = "Test message";
        SendMessage message = new SendMessage();

        botMessageGenerator.initMovieMessage(text, message);

        Assertions.assertEquals(text, message.getText());
        ReplyKeyboardMarkup replyKeyboardMarkup = (ReplyKeyboardMarkup) message.getReplyMarkup();
        Assertions.assertNotNull(replyKeyboardMarkup);
        Assertions.assertTrue(replyKeyboardMarkup.getResizeKeyboard());
        Assertions.assertTrue(replyKeyboardMarkup.getOneTimeKeyboard());
        Assertions.assertEquals(1, replyKeyboardMarkup.getKeyboard().size());
        Assertions.assertEquals(3, replyKeyboardMarkup.getKeyboard().get(0).size());
    }

    private static Film createFilm(int id, String name) {
        return new Film(
                id,
                name,
                "name",
                "name",
                1,
                "name",
                1,
                List.of(new PersonProfession(10L, "artist")),
                new Rating(7.4),
                List.of(new Genre("anime")),
                List.of(new Country("Russia")),
                18
        );
    }
}
