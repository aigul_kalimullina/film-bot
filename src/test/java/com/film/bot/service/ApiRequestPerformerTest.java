package com.film.bot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ApiRequestPerformerTest {

    @Mock
    private CloseableHttpClient httpclient;

    @Mock
    private ObjectMapper objectMapper;

    private ApiRequestPerformer apiRequestPerformer;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        apiRequestPerformer = new ApiRequestPerformer("test_token", "test_api", objectMapper);
        //apiRequestPerformer.setHttpclient(httpclient);
    }

    @Test
    public void testGenerateRequestNegative() throws IOException {
        String jsonResponse = "{\"key\":\"value\"}";
        TestClass testObject = new TestClass("value");
        CloseableHttpResponse response = null;
        when(httpclient.execute(any())).thenReturn(response);
        when(objectMapper.readValue(jsonResponse, TestClass.class)).thenReturn(testObject);

        TestClass result = apiRequestPerformer.generateRequest("/test", TestClass.class);

        assertNull(result);
    }

    static class TestClass {
        private String key;

        public TestClass(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
