package com.film.bot.app;

import java.util.Arrays;
import java.util.stream.Stream;

import com.film.bot.service.FavouritesService;
import com.film.bot.service.MovieService;
import com.film.bot.service.PreferencesService;
import com.film.bot.service.enums.PreferencesFunction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class BotMessageHandlerTest {

    @Mock
    private FavouritesService favouritesService;
    @Mock
    private MovieService movieService;
    @Mock
    private PreferencesService preferencesService;

    private BotMessageHandler underTests;

    @BeforeEach
    public void setUp() {
        favouritesService = mock(FavouritesService.class);
        movieService = mock(MovieService.class);
        preferencesService = mock(PreferencesService.class);
        underTests = new BotMessageHandler(favouritesService, movieService, preferencesService);
    }

    @Test
    public void testStart() {
        SendMessage message = Mockito.mock(SendMessage.class);
        underTests.start(message);
        verify(message, times(1)).setText(anyString());
        verify(message, times(1)).setReplyMarkup(any());
    }

    @MethodSource
    @ParameterizedTest
    public void testHandleFunctions(BotFunctions functions) {
        long id = 1;
        SendMessage message = new SendMessage();

        underTests.handleFunctions(functions, id, message);
    }

    @Test
    public void testHandleMessageFavourite() {
        long userId = 1;
        String txt = "Favourite_123";
        SendMessage message = new SendMessage();

        underTests.handleMessage(userId, txt, message);

        verify(favouritesService).getFavouriteMovieInfo(123L, message);
    }

    @Test
    public void testHandleMessageInfo() {
        long userId = 1;
        String txt = "Info_123";
        SendMessage message = new SendMessage();

        underTests.handleMessage(userId, txt, message);

        verify(movieService).getRichMovieInfo(123L, message);
    }

    @Test
    public void testHandleMessageGenre() {
        long userId = 1;
        String txt = "Genre_Scifi";
        SendMessage message = new SendMessage();

        underTests.handleMessage(userId, txt, message);

        verify(movieService).getMovieByGenre("Scifi", message);
    }

    @Test
    public void testHandleMessageFilter() {
        long userId = 1;
        String txt = "Filter_A_123";
        SendMessage message = new SendMessage();

        underTests.handleMessage(userId, txt, message);

        verify(movieService).getDownloadLinkWithFilters(123L, message, null);
    }

    public static Stream<Arguments> testHandleFunctions() {
        return Arrays.stream(BotFunctions.values()).map(Arguments::of);
    }

    @MethodSource
    @ParameterizedTest
    public void testHandleMessagePreferences(PreferencesFunction functions) {
        long userId = 1;
        String txt = "Preferences_" + functions.name();
        SendMessage message = new SendMessage();

        assertThatCode(() -> underTests.handleMessage(userId, txt, message)).doesNotThrowAnyException();
    }

    public static Stream<Arguments> testHandleMessagePreferences() {
        return Arrays.stream(PreferencesFunction.values()).map(Arguments::of);
    }

    @MethodSource
    @ParameterizedTest
    public void testHandleMessagePreferencesWithParam(PreferencesFunction functions) {
        long userId = 1;
        String txt = "Preferences_" + functions.name() + "_1";
        SendMessage message = new SendMessage();

        assertThatCode(() -> underTests.handleMessage(userId, txt, message)).doesNotThrowAnyException();
    }

    public static Stream<Arguments> testHandleMessagePreferencesWithParam() {
        return Arrays.stream(PreferencesFunction.values()).map(Arguments::of);
    }
}