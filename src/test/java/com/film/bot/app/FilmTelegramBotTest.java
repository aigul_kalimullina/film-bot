package com.film.bot.app;

import com.film.bot.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class FilmTelegramBotTest {

    @Mock
    private UserService userService;

    @Mock
    private BotMessageHandler botMessageHandler;

    @InjectMocks
    private FilmTelegramBot filmTelegramBot;

    @Test
    public void testOnUpdateReceivedWithMessageText() {
        Update update = new Update();
        Message message = new Message();
        message.setText("/start");
        message.setChat(new Chat(1L, "name"));
        User user = new User();
        user.setUserName("testUser");
        message.setFrom(user);
        update.setMessage(message);

        filmTelegramBot.onUpdateReceived(update);

        // Verify that botMessageHandler.start() was called
        verify(botMessageHandler).start(any(SendMessage.class));
    }

    @Test
    public void testOnUpdateReceivedWithCallbackQuery() {
        Update update = new Update();
        CallbackQuery callbackQuery = new CallbackQuery();
        Message message = new Message();
        message.setChat(new Chat(1L, "name"));
        callbackQuery.setMessage(message);
        callbackQuery.setData("SomeData");
        User from = new User();
        from.setId(123L);
        callbackQuery.setFrom(from);
        update.setCallbackQuery(callbackQuery);

        filmTelegramBot.onUpdateReceived(update);

        // Verify that botMessageHandler.handleMessage() was called
        verify(botMessageHandler).handleMessage(eq(123L), eq("SomeData"), any(SendMessage.class));
    }

}