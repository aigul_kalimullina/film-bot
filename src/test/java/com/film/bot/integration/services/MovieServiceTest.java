package com.film.bot.integration.services;

import java.util.List;
import java.util.stream.Stream;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.service.BotMessageGenerator;
import com.film.bot.service.MovieService;
import com.film.bot.unit.kinopoisk.model.Country;
import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.kinopoisk.model.Genre;
import com.film.bot.unit.kinopoisk.model.ProductionCompany;
import com.film.bot.unit.kinopoisk.model.Rating;
import com.film.bot.unit.kinopoisk.model.Trailer;
import com.film.bot.unit.kinopoisk.model.Video;
import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.rutracker.enums.Filters;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class MovieServiceTest extends IntegrationTestBase {

    @Autowired
    private MovieService movieService;

    @Autowired
    private BotMessageGenerator messageGenerator;

    public static Stream<Arguments> testDownloadLinkWithFilters() {
        return Stream.of(
                Arguments.of(
                        Filters.RATING,
                        "https://rutracker.org/forum/viewtopic.php?t=3186952",
                        "https://rutracker.org/forum/tracker.php?nm=%D0%9F%D0%BE%D0%B1%D0%B5%D0%B3+%D0%B8%D0%B7+%D0%A8%D0%BE%D1%83%D1%88%D0%B5%D0%BD%D0%BA%D0%B0+1994"
                ),
                Arguments.of(
                        Filters.SEED,
                        "https://rutracker.org/forum/viewtopic.php?t=5670238",
                        "https://rutracker.org/forum/tracker.php?nm=%D0%9F%D0%BE%D0%B1%D0%B5%D0%B3+%D0%B8%D0%B7+%D0%A8%D0%BE%D1%83%D1%88%D0%B5%D0%BD%D0%BA%D0%B0+1994"
                ),
                Arguments.of(
                        Filters.BEST,
                        "https://rutracker.org/forum/viewtopic.php?t=5670238",
                        "https://rutracker.org/forum/tracker.php?nm=%D0%9F%D0%BE%D0%B1%D0%B5%D0%B3+%D0%B8%D0%B7+%D0%A8%D0%BE%D1%83%D1%88%D0%B5%D0%BD%D0%BA%D0%B0+1994"
                ),
                Arguments.of(
                        Filters.FIRST,
                        "https://rutracker.org/forum/viewtopic.php?t=3144018",
                        "https://rutracker.org/forum/tracker.php?nm=%D0%9F%D0%BE%D0%B1%D0%B5%D0%B3+%D0%B8%D0%B7+%D0%A8%D0%BE%D1%83%D1%88%D0%B5%D0%BD%D0%BA%D0%B0+1994"
                ),
                Arguments.of(
                        Filters.SECOND,
                        "https://rutracker.org/forum/viewtopic.php?t=5670238",
                        "https://rutracker.org/forum/tracker.php?nm=%D0%9F%D0%BE%D0%B1%D0%B5%D0%B3+%D0%B8%D0%B7+%D0%A8%D0%BE%D1%83%D1%88%D0%B5%D0%BD%D0%BA%D0%B0+1994"
                )
        );
    }

    @Test
    public void testRichMovieInfo() {
        SendMessage actualMessage = new SendMessage();
        movieService.getRichMovieInfo(435L, actualMessage);

        MovieResponse expectedMovie = new MovieResponse(
                435,
                "Зеленая миля",
                "The Green Mile",
                null,
                "Пол Эджкомб — начальник блока смертников в тюрьме «Холодная гора», каждый из узников которого однажды проходит «зеленую милю» по пути к месту казни. Пол повидал много заключённых и надзирателей за время работы. Однако гигант Джон Коффи, обвинённый в страшном преступлении, стал одним из самых необычных обитателей блока.\n",
                new Rating(9.076),
                1999,
                18,
                189,
                List.of(
                        new Genre("драма"),
                        new Genre("фэнтези"),
                        new Genre("криминал")
                ),
                List.of(new Country("США")),
                new Video(
                        List.of(
                                new Trailer("https://www.youtube.com/embed/TODt_q-_4C4"),
                                new Trailer("https://www.youtube.com/embed/TODt_q-_4C4"),
                                new Trailer("https://www.youtube.com/embed/Bg7epsq0OIQ")
                        ),
                        null
                ),
                List.of(
                        new ProductionCompany("Castle Rock Entertainment"),
                        new ProductionCompany("Darkwoods Productions"),
                        new ProductionCompany("Warner Bros. Pictures"),
                        new ProductionCompany("Universal Pictures")
                )
        );
        SendMessage expectedMessage = new SendMessage();
        messageGenerator.initMovieMessage(expectedMovie.toString(), expectedMessage);
        Assertions.assertEquals(expectedMessage.getText(), actualMessage.getText());
    }

    @Test
    public void testRandomMovie() {
        SendMessage message = new SendMessage();
        movieService.getRandomMovie(message);

        Assertions.assertTrue(message.getText().contains("Полученный фильм:"));
    }

    @Test
    public void testLinksByMovieId() {
        SendMessage actualMessage = new SendMessage();
        movieService.getLinksByMovieId(actualMessage, 251733);

        SendMessage expectedMessage = new SendMessage();
        expectedMessage.setText("""
                Ссылка на кинопоиск: https://www.kinopoisk.ru/film/251733/
                Ссылка на просмотр: http://sspoisk.ru/film/251733/
                Для получения ссылки на скачивание выбери необходимые фильтры через клавиатуру:\s""");

        Assertions.assertEquals(expectedMessage.getText(), actualMessage.getText());
    }

    @Test
    public void testMovieByGenre() {
        SendMessage actualMessage = new SendMessage();
        movieService.getMovieByGenre("спорт", actualMessage);

        List<Film> expectedFilms = List.of(
                new Film(
                        835086,
                        "Ford против Ferrari",
                        "Ford v Ferrari",
                        null,
                        2019,
                        "В начале 1960-х Генри Форд II принимает решение улучшить имидж компании и сменить курс на производство более модных автомобилей. После неудавшейся попытки купить практически банкрота Ferrari американцы решают бросить вызов итальянским конкурентам на трассе и выиграть престижную гонку 24 часа Ле-Мана. Чтобы создать подходящую машину, компания нанимает автоконструктора Кэррола Шэлби, а тот отказывается работать без выдающегося, но, как считается, трудного в общении гонщика Кена Майлза. Вместе они принимаются за разработку впоследствии знаменитого спорткара Ford GT40.",
                        152,
                        null,
                        new Rating(8.233),
                        List.of(
                                new Genre("биография"),
                                new Genre("спорт"),
                                new Genre("драма"),
                                new Genre("боевик")
                        ),
                        List.of(new Country("США")),
                        18
                ),
                new Film(
                        601564,
                        "Легенда №17",
                        null,
                        null,
                        2012,
                        "История жизни хоккеиста Валерия Харламова.",
                        134,
                        null,
                        new Rating(7.978),
                        List.of(new Genre("биография"), new Genre("спорт"), new Genre("драма")),
                        List.of(new Country("Россия")),
                        6
                ),
                new Film(
                        61249,
                        "Тачки",
                        "Cars",
                        null,
                        2006,
                        "Неукротимый в своем желании всегда и во всем побеждать гоночный автомобиль «Молния» Маккуин вдруг обнаруживает, что сбился с пути и застрял в маленьком захолустном городке Радиатор-Спрингс, что находится где-то на трассе 66 в Калифорнии.\n" +
                                "\n" +
                                "Участвуя в гонках на Кубок Поршня, где ему противостояли два очень опытных соперника, Маккуин совершенно не ожидал, что отныне ему придется общаться с персонажами совсем иного рода. Это, например, Салли — шикарный Порше 2002-го года выпуска, Док Хадсон — легковушка модели «Хадсон Хорнет», 1951-го года выпуска или Метр — ржавый грузовичок-эвакуатор. И все они помогают Маккуину понять, что в мире существуют некоторые более важные вещи, чем слава, призы и спонсоры…",
                        112,
                        null,
                        new Rating(7.514),
                        List.of(
                                new Genre("мультфильм"),
                                new Genre("комедия"),
                                new Genre("приключения"),
                                new Genre("семейный"),
                                new Genre("спорт")
                        ),
                        List.of(new Country("США")),
                        0
                ),
                new Film(
                        840817,
                        "Движение вверх",
                        null,
                        null,
                        2017,
                        "Есть победы, которые меняют ход истории. Победы духа, победы страны, победы всего мира. Таким триумфом стали легендарные «три секунды» - выигрыш сборной СССР по баскетболу на роковой мюнхенской Олимпиаде 1972 г. Впервые за 36 лет была повержена «непобедимая» команда США. Никто даже помыслить не мог о том, что это возможно – обыграть великолепных непогрешимых американцев на Олимпийских играх! Никто, кроме советских баскетболистов (русских и грузин, украинцев и казахов, белорусов и литовцев).\n" +
                                "\n" +
                                "Когда проигрыш означал поражение страны, когда нужно было выходить и бороться в раскаленной обстановке из-за произошедшего теракта, великий тренер сборной СССР был готов на все, лишь бы помочь своим подопечным разбить американский миф о непотопляемой команде мечты. Ведь он знал, что создал самую сильную сборную на планете, и в начале заставил поверить в это своих игроков, а затем весь мир.",
                        133,
                        null,
                        new Rating(7.487),
                        List.of(new Genre("спорт"), new Genre("драма")),
                        List.of(new Country("Россия")),
                        6
                ),
                new Film(
                        1311936,
                        "Трудные подростки",
                        "Трудные подростки",
                        null,
                        2019,
                        "Известный футболист Антон Ковалёв, отсидевший два года в тюрьме за пьяный дебош в кафе, выходит на свободу. Этим проступком бывший капитан сборной России поставил крест на своей успешной карьере. Репутация «сбитого летчика» не позволяет ему продолжить любимое дело и получить престижную работу. Некогда воспитывавший Антона глава центра трудных подростков Герман предлагает ему вернуться в альма-матер, чтобы подготовить непростых ребят к местной Олимпиаде. От безысходности Ковалев соглашается на предложение, но даже не представляет, насколько сложно будет найти общий язык с ребятами и какое важное место они могут занять в его жизни.",
                        0,
                        null,
                        new Rating(8.088),
                        List.of(new Genre("драма"), new Genre("комедия"), new Genre("спорт")),
                        List.of(new Country("Россия")),
                        18
                ),
                new Film(
                        450213,
                        "Невидимая сторона",
                        "The Blind Side",
                        null,
                        2009,
                        "Благополучная белая семья берет к себе толстого, неграмотного бездомного темнокожего подростка и помогает стать ему спортивной звездой, и поступить в университет.",
                        129,
                        null,
                        new Rating(8.185),
                        List.of(new Genre("драма"), new Genre("спорт"), new Genre("биография")),
                        List.of(new Country("США")),
                        18
                ),
                new Film(
                        81297,
                        "Малышка на миллион",
                        "Million Dollar Baby",
                        null,
                        2004,
                        "Тренеру по боксу Фрэнку Данну так и не удалось воспитать чемпиона. Он владеет спортивным залом в Лос-Анджелесе, где всё ещё проводит тренировки. Дочь не отвечает на его письма, а его лучший боец подписал контракт с другим менеджером. Неожиданно в жизни Фрэнка появляется Мэгги Фицжеральд, 31-летняя официантка, мечтающая стать боксером. Фрэнк не желает тренировать женщину, но упорство Мэгги заставляет его передумать. Впереди - их главное сражение, требующее собрать в кулак всю волю и мужество.",
                        132,
                        null,
                        new Rating(8.114),
                        List.of(new Genre("драма"), new Genre("спорт")),
                        List.of(new Country("США")),
                        18
                ),
                new Film(
                        1234853,
                        "Мистер Нокаут",
                        null,
                        null,
                        2022,
                        "История жизни и приключений легендарного советского боксера Валерия Попенченко, чемпиона СССР, Европы и победителя Олимпийских игр 1964 года в Токио — о его детстве в суворовском училище в Ташкенте, о службе курсантом-пограничником, о его первых успехах и неудачах, и о его дружбе с тренером спортивного общества «Динамо» Григорием Кусикьянцем. История о том, что в любом, даже самом престижном поединке, для спортсмена главное — преодолеть себя, свои страхи и слабости, и только тогда можно будет одержать настоящую победу.",
                        117,
                        null,
                        new Rating(7.601),
                        List.of(new Genre("спорт"), new Genre("драма")),
                        List.of(new Country("Россия")),
                        12
                ),
                new Film(
                        596125,
                        "Гонка",
                        "Rush",
                        null,
                        2013,
                        "1970-е. Два непримиримых соперника в гонках «Формула-1» — обаятельный плейбой-англичанин Джеймс Хант и дисциплинированный перфекционист-австриец Ники Лауда — доводят себя до предела физической и психологической выносливости ради победы. Каждый из них стремится быть первым, а ошибка на трассе может стоить гонщикам жизни.",
                        123,
                        null,
                        new Rating(8.094),
                        List.of(new Genre("спорт"), new Genre("драма"), new Genre("биография")),
                        List.of(new Country("Великобритания"), new Country("США")),
                        18
                ),
                new Film(
                        1253633,
                        "Ход королевы",
                        "The Queen's Gambit",
                        "The Queens Gambit",
                        2020,
                        "Штат Кентукки, 1957 год. После смерти матери 9-летняя Элизабет Хармон остаётся сиротой и отправляется в католический приют. В этом заведении детям регулярно дают «витамины», и по совету другой подопечной Бет оставляет зелёные капсулы на ночь. Там же девочка знакомится с пожилым уборщиком Шайбелем, которого просит научить её играть в шахматы. Днём, сбежав со скучных занятий под предлогом помыть тряпку, Бет играет с мистером Шайбелем в его подвальной подсобке, а ночью, приняв зелёную капсулу, прокручивает шахматные партии на потолке общей спальни. Через несколько лет девочку удочерят, у неё начнётся совсем другая жизнь, но главную роль в ней будет играть любовь к шахматам.",
                        0,
                        null,
                        new Rating(8.283),
                        List.of(new Genre("драма"), new Genre("спорт")),
                        List.of(new Country("США")),
                        18
                )
        );

        SendMessage expectedMessage = new SendMessage();
        messageGenerator.generateMovieInfoMessage(
                expectedFilms,
                "",
                "Фильмы, найденные в жанре спорт : ",
                expectedMessage
        );
        Assertions.assertEquals(expectedMessage.getText(), actualMessage.getText());
    }

    @Test
    public void testMovieInfoBySearch() {
        SendMessage actualMessage = new SendMessage();
        movieService.getMovieInfoBySearch(actualMessage, "Аватар");

        Film expected = new Film(
                251733,
                "Аватар",
                "Avatar",
                "",
                2009,
                "Бывший морпех Джейк Салли прикован к инвалидному креслу. Несмотря на немощное тело, Джейк в душе по-прежнему остается воином. Он получает задание совершить путешествие в несколько световых лет к базе землян на планете Пандора, где корпорации добывают редкий минерал, имеющий огромное значение для выхода Земли из энергетического кризиса.",
                162,
                null,
                new Rating(7.975),
                List.of(
                        new Genre("фантастика"),
                        new Genre("боевик"),
                        new Genre("драма"),
                        new Genre("приключения")
                ),
                List.of(new Country("США")),
                12
        );
        SendMessage expectedMessage = new SendMessage();
        messageGenerator.initMovieMessage("Найденные фильмы по запросу: \n \n" + expected + "\n", expectedMessage);
        Assertions.assertEquals(expectedMessage.getText(), actualMessage.getText());
    }

    @ParameterizedTest
    @MethodSource
    public void testDownloadLinkWithFilters(Filters filters, String torrentUrl, String simpleUrl) {
        SendMessage actualMessage = new SendMessage();
        movieService.getDownloadLinkWithFilters(326L, actualMessage, filters);

        SendMessage expectedMessage = new SendMessage();
        expectedMessage.setText(
                "Ссылка на скачивание: " + torrentUrl + "\nСсылка на простой поиск торрента: " + simpleUrl
        );
    }

    @Test
    public void testPopularMovies() {
        SendMessage actualMessage = new SendMessage();
        movieService.getPopularMovies(actualMessage);

        SendMessage expectedMessage = new SendMessage();
        List<Film> expectedFilms = List.of(
                new Film(
                        326,
                        "Побег из Шоушенка",
                        "The Shawshank Redemption",
                        null,
                        1994,
                        "Бухгалтер Энди Дюфрейн обвинён в убийстве собственной жены и её любовника. Оказавшись в тюрьме под названием Шоушенк, он сталкивается с жестокостью и беззаконием, царящими по обе стороны решётки. Каждый, кто попадает в эти стены, становится их рабом до конца жизни. Но Энди, обладающий живым умом и доброй душой, находит подход как к заключённым, так и к охранникам, добиваясь их особого к себе расположения.",
                        142,
                        null,
                        new Rating(9.109),
                        List.of(new Genre("драма")),
                        List.of(new Country("США")),
                        18
                ),
                new Film(
                        435,
                        "Зеленая миля",
                        "The Green Mile",
                        null,
                        1999,
                        "Пол Эджкомб — начальник блока смертников в тюрьме «Холодная гора», каждый из узников которого однажды проходит «зеленую милю» по пути к месту казни. Пол повидал много заключённых и надзирателей за время работы. Однако гигант Джон Коффи, обвинённый в страшном преступлении, стал одним из самых необычных обитателей блока.",
                        189,
                        null,
                        new Rating(9.075),
                        List.of(
                                new Genre("драма"),
                                new Genre("фэнтези"),
                                new Genre("криминал")
                        ),
                        List.of(new Country("США")),
                        18
                ),
                new Film(
                        448,
                        "Форрест Гамп",
                        "Forrest Gump",
                        null,
                        1994,
                        "Сидя на автобусной остановке, Форрест Гамп — не очень умный, но добрый и открытый парень — рассказывает случайным встречным историю своей необыкновенной жизни.\n" +
                                "\n" +
                                "С самого малолетства парень страдал от заболевания ног, соседские мальчишки дразнили его, но в один прекрасный день Форрест открыл в себе невероятные способности к бегу. Подруга детства Дженни всегда его поддерживала и защищала, но вскоре дороги их разошлись.",
                        142,
                        null,
                        new Rating(8.921),
                        List.of(
                                new Genre("драма"),
                                new Genre("комедия"),
                                new Genre("мелодрама"),
                                new Genre("история"),
                                new Genre("военный")
                        ),
                        List.of(new Country("США")),
                        18
                ),
                new Film(
                        329,
                        "Список Шиндлера",
                        "Schindler's List",
                        null,
                        1993,
                        "Фильм рассказывает реальную историю загадочного Оскара Шиндлера, члена нацистской партии, преуспевающего фабриканта, спасшего во время Второй мировой войны почти 1200 евреев.",
                        195,
                        null,
                        new Rating(8.845),
                        List.of(
                                new Genre("драма"),
                                new Genre("биография"),
                                new Genre("история"),
                                new Genre("военный")
                        ),
                        List.of(new Country("США")),
                        18
                ),
                new Film(
                        535341,
                        "1+1",
                        "Intouchables",
                        null,
                        2011,
                        "Пострадав в результате несчастного случая, богатый аристократ Филипп нанимает в помощники человека, который менее всего подходит для этой работы, – молодого жителя предместья Дрисса, только что освободившегося из тюрьмы. Несмотря на то, что Филипп прикован к инвалидному креслу, Дриссу удается привнести в размеренную жизнь аристократа дух приключений.",
                        112,
                        null,
                        new Rating(8.821),
                        List.of(
                                new Genre("драма"),
                                new Genre("комедия"),
                                new Genre("биография")
                        ),
                        List.of(new Country("Франция")),
                        18
                ),
                new Film(
                        42664,
                        "Иван Васильевич меняет профессию",
                        null,
                        null,
                        1973,
                        "Инженер-изобретатель Тимофеев сконструировал машину времени, которая соединила его квартиру с далёким шестнадцатым веком — точнее, с палатами государя Ивана Грозного. Туда-то и попадают тёзка царя пенсионер-общественник Иван Васильевич Бунша и квартирный вор Жорж Милославский, а сам великий государь оказывается в квартире Тимофеева.",
                        88,
                        null,
                        new Rating(8.787),
                        List.of(
                                new Genre("комедия"),
                                new Genre("фантастика"),
                                new Genre("приключения")
                        ),
                        List.of(new Country("СССР")),
                        6
                ),
                new Film(
                        2360,
                        "Король Лев",
                        "The Lion King",
                        null,
                        1994,
                        "У величественного Короля-Льва Муфасы рождается наследник по имени Симба. Уже в детстве любознательный малыш становится жертвой интриг своего завистливого дяди Шрама, мечтающего о власти.\n" +
                                "\n" +
                                "Симба познаёт горе утраты, предательство и изгнание, но в конце концов обретает верных друзей и находит любимую. Закалённый испытаниями, он в нелёгкой борьбе отвоёвывает своё законное место в «Круге жизни», осознав, что значит быть настоящим Королём. ",
                        88,
                        null,
                        new Rating(8.775),
                        List.of(
                                new Genre("мультфильм"),
                                new Genre("мюзикл"),
                                new Genre("драма"),
                                new Genre("приключения"),
                                new Genre("семейный")
                        ),
                        List.of(new Country("США")),
                        0
                ),
                new Film(
                        679486,
                        "Тайна Коко",
                        "Coco",
                        null,
                        2017,
                        "12-летний Мигель живёт в мексиканской деревушке в семье сапожников и тайно мечтает стать музыкантом. Тайно, потому что в его семье музыка считается проклятием. Когда-то его прапрадед оставил жену, прапрабабку Мигеля, ради мечты, которая теперь не даёт спокойно жить и его праправнуку. С тех пор музыкальная тема в семье стала табу. Мигель обнаруживает, что между ним и его любимым певцом Эрнесто де ла Крусом, ныне покойным, существует некая связь. Паренёк отправляется к своему кумиру в Страну Мёртвых, где встречает души предков. Мигель знакомится там с духом-скелетом по имени Гектор, который становится его проводником. Вдвоём они отправляются на поиски де ла Круса.",
                        105,
                        null,
                        new Rating(8.723),
                        List.of(
                                new Genre("мультфильм"),
                                new Genre("фэнтези"),
                                new Genre("комедия"),
                                new Genre("приключения"),
                                new Genre("семейный"),
                                new Genre("музыка")
                        ),
                        List.of(
                                new Country("США"),
                                new Country("Мексика"),
                                new Country("Япония")
                        ),
                        12
                ),
                new Film(
                        4540126,
                        "Дюна: Часть вторая",
                        "Dune: Part Two",
                        null,
                        2024,
                        "Герцог Пол Атрейдес присоединяется к фременам, чтобы стать Муад Дибом, одновременно пытаясь остановить наступление войны.",
                        166,
                        null,
                        new Rating(8.707),
                        List.of(
                                new Genre("фантастика"),
                                new Genre("боевик"),
                                new Genre("драма"),
                                new Genre("приключения")
                        ),
                        List.of(new Country("США"), new Country("Канада")),
                        0
                ),
                new Film(
                        325,
                        "Крестный отец",
                        "The Godfather",
                        null,
                        1972,
                        "Криминальная сага, повествующая о нью-йоркской сицилийской мафиозной семье Корлеоне. Фильм охватывает период 1945-1955 годов.\n" +
                                "\n" +
                                "Глава семьи, Дон Вито Корлеоне, выдаёт замуж свою дочь. В это время со Второй мировой войны возвращается его любимый сын Майкл. Майкл, герой войны, гордость семьи, не выражает желания заняться жестоким семейным бизнесом. Дон Корлеоне ведёт дела по старым правилам, но наступают иные времена, и появляются люди, желающие изменить сложившиеся порядки. На Дона Корлеоне совершается покушение.",
                        175,
                        null,
                        new Rating(8.705),
                        List.of(new Genre("драма"), new Genre("криминал")),
                        List.of(new Country("США")),
                        18
                )
        );
        messageGenerator.generateMovieInfoMessage(
                expectedFilms,
                "Топ фильмов не найден!",
                "Топ 10 популярных фильмов: ",
                expectedMessage
        );

        Assertions.assertEquals(expectedMessage.toString(), actualMessage.toString());
    }
}
