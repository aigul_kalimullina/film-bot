package com.film.bot.integration.services;

import java.util.List;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.service.FavouritesService;
import com.film.bot.unit.favourites.manager.FavouritesManager;
import com.film.bot.unit.movie.manager.model.Movie;
import com.film.bot.unit.user.manager.UserManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

public class FavouritesServiceTest extends IntegrationTestBase {

    @Autowired
    FavouritesService favouritesService;

    @Autowired
    FavouritesManager favouritesManager;

    @Autowired
    UserManager userManager;

    private final long userId = 6172566058l;
    private final long movieId = 679486l;
    private final String favorite = "Favourite_";

    @Test
    public void addMovieToFavouritesTest() {
        SendMessage result = new SendMessage();
        String resultStr = "Фильм добавлен в избранное";
        Update update = new Update();
        CallbackQuery callbackQuery = new CallbackQuery();
        User user = new User();
        user.setId(userId);
        callbackQuery.setData(favorite + movieId);
        callbackQuery.setFrom(user);
        update.setCallbackQuery(callbackQuery);
        userManager.saveInteraction(update);

        favouritesService.addMovieToFavourites(userId, result);
        List<Long> movies = favouritesManager.findFavouriteMoviesByUserId(userId)
                .stream()
                .map(Movie::id)
                .toList();

        Assertions.assertTrue(movies.contains(movieId));
        Assertions.assertEquals(result.getText(), resultStr);
    }

    @Test
    public void getUserFavouriteMoviesTest() {
        SendMessage result = new SendMessage();
        String resultStr = "Твои фильмы в избранном: ";
        String resultMovie = "Тайна Коко";
        Update update = new Update();
        CallbackQuery callbackQuery = new CallbackQuery();
        User user = new User();
        user.setId(userId);
        callbackQuery.setData(favorite + movieId);
        callbackQuery.setFrom(user);
        update.setCallbackQuery(callbackQuery);
        userManager.saveInteraction(update);
        favouritesService.addMovieToFavourites(userId, result);
        favouritesService.getUserFavouriteMovies(userId, result);

        Assertions.assertEquals(resultStr, result.getText());
        Assertions.assertTrue(result.getReplyMarkup().toString().contains(resultMovie));
    }

    @Test
    public void getFavouriteMovieInfo() {
        SendMessage result = new SendMessage();
        String resultStr = "\uD83C\uDDF7\uD83C\uDDFA Название: Тайна Коко\n" +
                "\uD83C\uDDEC\uD83C\uDDE7 Название: Coco\n" +
                "\uD83E\uDDE9 Жанры: мультфильм, фэнтези, комедия, приключения, семейный, музыка\n" +
                "⬆️ Рейтинг: 8.723\n" +
                "\n" +
                "Ссылка на кинопоиск: https://www.kinopoisk.ru/film/679486/\n" +
                "Ссылка на просмотр: http://sspoisk.ru/film/679486/\n" +
                "Для получения ссылки на скачивание выбери необходимые фильтры через клавиатуру: ";
        Update update = new Update();
        CallbackQuery callbackQuery = new CallbackQuery();
        User user = new User();
        user.setId(userId);
        callbackQuery.setData(favorite + movieId);
        callbackQuery.setFrom(user);
        update.setCallbackQuery(callbackQuery);
        userManager.saveInteraction(update);
        favouritesService.addMovieToFavourites(userId, result);
        favouritesService.getFavouriteMovieInfo(movieId, result);
        Assertions.assertEquals(resultStr, result.getText());
    }
}