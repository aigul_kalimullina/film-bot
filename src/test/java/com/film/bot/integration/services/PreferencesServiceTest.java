package com.film.bot.integration.services;

import java.util.Arrays;
import java.util.List;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.service.PreferencesService;
import com.film.bot.service.enums.PreferencesFunction;
import com.film.bot.unit.movie.enums.Genre;
import com.film.bot.unit.movie.enums.Rating;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class PreferencesServiceTest extends IntegrationTestBase {

    @Autowired
    PreferencesService preferencesService;

    private final long userId = 6172566058l;
    private final String preferences = "Preferences_";
    private final String genre = Genre.ADVENTURE.getAlias();
    private final String rating = Rating.MORE_FIVE.getKpValue();
    @Value("#{'${movie.periods.for.preferences}'.split(',')}")
    private List<String> periods;
    @Test
    public void startSearchByGenresTest(){
        SendMessage result = new SendMessage();
        String resultStr = "Выбери любимый жанр:";
        preferencesService.startSearchByGenres(result);
        Assertions.assertEquals(resultStr, result.getText());
        List<String> lst = Genre.ALL_GENRES.stream().filter((genre)->!result.getReplyMarkup().toString().contains(genre)).toList();
        Assertions.assertTrue(lst.isEmpty());
    }

    @Test
    public void writeAndFullSearchTest(){
        SendMessage result = new SendMessage();
        String resultStr = "Фильмы по твоим предпочтениям:";
        String resultMovie = "Король Лев";
        String resultMovie2 = "Мумия";

        preferencesService.writeChosenGenre(userId, genre);
        preferencesService.writeChosenRating(userId, rating);
        preferencesService.writeChosenPeriod(userId, periods.get(3));
        preferencesService.writeChosenDirector(userId, -1L);
        preferencesService.writeChosenActor(userId, -1L);

        preferencesService.fullSearch(userId, result);
        Assertions.assertEquals(resultStr, result.getText());
        Assertions.assertTrue(result.getReplyMarkup().toString().contains(resultMovie));
        Assertions.assertTrue(result.getReplyMarkup().toString().contains(resultMovie2));
    }

    @Test
    public void searchByRatingTest(){
        SendMessage result = new SendMessage();
        String resultStr = "Выбери минимальный рейтинг фильма:";
        preferencesService.searchByRating(result);
        List<Rating> lst = Arrays.stream(Rating.values()).filter((rating)->!result.getReplyMarkup().toString().contains(rating.getKpValue())).toList();
        Assertions.assertEquals(resultStr, result.getText());
        Assertions.assertTrue(lst.isEmpty());
    }

    @Test
    public void searchByDirectorTest(){
        SendMessage result = new SendMessage();
        String resultStr = "Введи данные режиссера с клавиатуры:";
        String resultStrButton = "Не важно";
        preferencesService.searchByDirector(result);
        Assertions.assertEquals(resultStr, result.getText());
        Assertions.assertTrue(result.getReplyMarkup().toString().contains(resultStrButton));
    }
    @Test
    public void searchByActorTest(){
        SendMessage result = new SendMessage();
        String resultStr = "Введи данные актера с клавиатуры:";
        String resultStrButton = "Не важно";
        preferencesService.searchByActor(result);
        Assertions.assertEquals(resultStr, result.getText());
        Assertions.assertTrue(result.getReplyMarkup().toString().contains(resultStrButton));
    }
    @Test
    public void findPersonTest(){
        SendMessage result = new SendMessage();
        String resultStr = "Выбери персону: ";
        String search = "Ургант";
        String tst = "Иван Ургант";
        preferencesService.findPerson(result, search, PreferencesFunction.ACTOR);
        Assertions.assertEquals(resultStr, result.getText());
        Assertions.assertTrue(result.getReplyMarkup().toString().contains(tst));
    }
}
