package com.film.bot.integration.services;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.service.UserService;
import com.film.bot.unit.user.manager.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceTest extends IntegrationTestBase {

    @Autowired
    private UserService userService;

    @Test
    public void testUserInsert() {
        String username = "test_username";
        userService.insert(new User(120L, username));

        Assertions.assertTrue(userService.existsByUsername(username));
    }

    @Test
    public void testInsertWithException() {
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> userService.insert(new User(1L, ""))
        );

        Assertions.assertEquals("Username is empty", thrown.getMessage());
    }
}
