package com.film.bot.integration.unit.user.dal;

import java.util.Optional;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.unit.user.dal.entity.UserEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserDaoTest extends IntegrationTestBase {

    private static final String USERNAME = "test_username";
    private static final long USER_ID = 463875L;

    @Test
    public void testExistsByUsername() {
        UserEntity userEntity = new UserEntity(USER_ID, USERNAME);

        userDao.save(userEntity);

        Assertions.assertTrue(userDao.existsByUsername(USERNAME));
    }

    @Test
    public void testFindByUsername() {
        UserEntity userEntity = new UserEntity(USER_ID, USERNAME);
        userDao.save(userEntity);

        Optional<UserEntity> foundUser = userDao.findByUsername(USERNAME);

        Assertions.assertTrue(foundUser.isPresent());
        Assertions.assertEquals(userEntity, foundUser.get());
    }

}
