package com.film.bot.integration.unit.user.dal;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.unit.user.dal.entity.UserHistoryEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.telegram.telegrambots.meta.api.objects.Update;

public class UserHistoryDaoTest extends IntegrationTestBase {

    private static final long USER_ID = 123345L;

    @Test
    public void testFindUserHistoriesByUserIdOrderByCreatedAtDesc() {
        UserHistoryEntity history = createEntity();
        UserHistoryEntity history2 = createEntity();

        userHistoryDao.save(history);
        userHistoryDao.save(history2);

        Long userId = history.getUserId();

        List<UserHistoryEntity> userHistories = userHistoryDao
                .findUserHistoriesByUserIdOrderByCreatedAtDesc(userId)
                .toList();

        Assertions.assertEquals(2, userHistories.size());
        Assertions.assertEquals(history, userHistories.get(0));
        Assertions.assertEquals(history2, userHistories.get(1));
    }

    @Test
    public void testNotFindUserHistoriesByUserId() {
        UserHistoryEntity history = createEntity();

        userHistoryDao.save(history);

        Assertions.assertEquals(
                1,
                userHistoryDao.findUserHistoriesByUserIdOrderByCreatedAtDesc(USER_ID).toList().size()
        );
        Assertions.assertTrue(userHistoryDao.findUserHistoriesByUserIdOrderByCreatedAtDesc(978714L).toList().isEmpty());

    }

    private UserHistoryEntity createEntity() {
        return new UserHistoryEntity(
                UUID.randomUUID().toString(),
                USER_ID,
                LocalDateTime.now().toEpochSecond(ZoneOffset.UTC),
                new Update()
        );
    }
}
