package com.film.bot.integration.unit.preferences.dal;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.unit.preferences.dal.entity.PreferencesEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PreferencesDaoTest extends IntegrationTestBase {

    private static final long USER_ID = 100L;

    @Test
    public void testFindByUserIdOrderByCreatedAtDesc() {
        PreferencesEntity preferences = createPreferences();
        preferencesDao.save(preferences);

        PreferencesEntity preferences2 = createPreferences();
        preferencesDao.save(preferences2);

        List<PreferencesEntity> preferencesList = preferencesDao.findByUserIdOrderByCreatedAtDesc(USER_ID).toList();

        Assertions.assertEquals(2, preferencesList.size());
        Assertions.assertTrue(preferencesList.get(0).getCreatedAt() > preferencesList.get(1).getCreatedAt());
    }

    @Test
    public void testFindByNonExistentUserIdOrderByCreatedAtDesc() {
        List<PreferencesEntity> preferencesList = preferencesDao.findByUserIdOrderByCreatedAtDesc(66L).toList();

        Assertions.assertTrue(preferencesList.isEmpty());
    }

    private PreferencesEntity createPreferences() {
        return new PreferencesEntity(
                UUID.randomUUID().toString(),
                "comedy",
                "3.3",
                "period",
                123L,
                123L,
                USER_ID,
                Instant.now().toEpochMilli()
        );

    }

}
