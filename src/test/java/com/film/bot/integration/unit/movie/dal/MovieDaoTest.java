package com.film.bot.integration.unit.movie.dal;

import java.util.List;

import com.film.bot.integration.IntegrationTestBase;
import com.film.bot.unit.movie.dal.entity.MovieEntity;
import com.film.bot.unit.movie.enums.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MovieDaoTest extends IntegrationTestBase {

    private static final long MOVIE_ID = 564634L;
    private static final long MOVIE_ID_2 = 1463764L;
    private static final MovieEntity TITANIC_MOVIE = new MovieEntity(
            MOVIE_ID,
            "Титаник",
            "Titanic",
            List.of(Genre.DOCUMENTARY.getAlias(), Genre.DRAMA.getAlias()),
            6.3
    );
    private static final MovieEntity SPIDER_MAN_MOVIE = new MovieEntity(
            MOVIE_ID_2,
            "Человек паук",
            "Spider-Man",
            List.of(Genre.ACTION.getAlias(), Genre.ADVENTURE.getAlias()),
            5.2
    );

    @BeforeEach
    public void setup() {
        movieDao.save(TITANIC_MOVIE);
        movieDao.save(SPIDER_MAN_MOVIE);
    }

    @Test
    public void testFindAllMovies() {
        List<MovieEntity> movies = movieDao.findAll();

        Assertions.assertNotNull(movies);
        Assertions.assertEquals(2, movies.size());
    }

    @Test
    public void testFindMovieByIds() {
        List<MovieEntity> foundMovies = movieDao.findByIdIn(List.of(MOVIE_ID, MOVIE_ID_2));
        Assertions.assertFalse(foundMovies.isEmpty());
        Assertions.assertEquals(2, foundMovies.size());
    }

    @Test
    public void testNotFindMovieByIds() {
        Assertions.assertTrue(movieDao.findByIdIn(List.of(1L)).isEmpty());
    }

}
