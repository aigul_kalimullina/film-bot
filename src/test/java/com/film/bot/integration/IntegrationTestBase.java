package com.film.bot.integration;

import com.film.bot.MongoExtension;
import com.film.bot.unit.movie.dal.MovieDao;
import com.film.bot.unit.preferences.dal.PreferencesDao;
import com.film.bot.unit.user.dal.UserDao;
import com.film.bot.unit.user.dal.UserHistoryDao;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.utility.DockerImageName;

@DataMongoTest
@ExtendWith({SpringExtension.class, MongoExtension.class})
@ActiveProfiles("tests")
public class IntegrationTestBase {


    @Autowired
    protected UserHistoryDao userHistoryDao;

    @Autowired
    protected UserDao userDao;

    @Autowired
    protected PreferencesDao preferencesDao;

    @Autowired
    protected MovieDao movieDao;

    @AfterEach
    void clean() {
        userHistoryDao.deleteAll();
        userDao.deleteAll();
        preferencesDao.deleteAll();
        movieDao.deleteAll();
    }

//    @AfterAll
//    public static void shutdownMongoDbTestContainer() {
//        mongoDBContainer.stop();
//        mongoDBContainer.close();
//    }
}
