package com.film.bot.unit.movie.manager.model;

import java.util.List;

public record Movie(
        Long id,
        String ruName,
        String enName,
        List<String> genres,
        double rating
) {
}
