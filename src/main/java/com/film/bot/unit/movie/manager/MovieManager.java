package com.film.bot.unit.movie.manager;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.film.bot.unit.movie.dal.MovieDao;
import com.film.bot.unit.movie.dal.entity.MovieEntity;
import com.film.bot.unit.movie.manager.exception.MovieNotFoundException;
import com.film.bot.unit.movie.manager.mapper.MovieEntityMapper;
import com.film.bot.unit.movie.manager.model.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MovieManager {

    private final MovieDao movieDao;
    private final MovieEntityMapper movieEntityMapper;

    public void addMovie(Movie movie) {
        movieDao.save(movieEntityMapper.toDal(movie));
    }

    public List<Movie> findMoviesByIds(List<Long> movieIds) {
        Map<Long, Movie> idToMovie = movieDao.findByIdIn(movieIds)
                .stream()
                .map(movieEntityMapper::fromDal)
                .collect(Collectors.toMap(Movie::id, Function.identity()));
        return movieIds.stream().map(idToMovie::get).filter(Objects::nonNull).toList();
    }

    public Movie findByMovieId(long movieId) {
        MovieEntity movieEntity = movieDao.findById(movieId)
                .orElseThrow(() -> new MovieNotFoundException("Movie with id " + movieId + " not found"));
        return movieEntityMapper.fromDal(movieEntity);
    }
}
