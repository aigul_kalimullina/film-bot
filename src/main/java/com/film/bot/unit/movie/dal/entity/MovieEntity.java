package com.film.bot.unit.movie.dal.entity;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(collection = "movie")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MovieEntity {

    @Id
    Long id;
    String ruName;
    String enName;
    List<String> genres;
    double rating;
}
