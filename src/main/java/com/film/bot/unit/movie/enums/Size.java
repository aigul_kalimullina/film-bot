package com.film.bot.unit.movie.enums;

public enum Size {
    FIRST(0.5, 1.0),
    SECOND(1.0, 4.0),
    THIRD(4.0, 16.0),
    FOURTH(16.0, null),
    /**/;

    private final Double start;
    private final Double end;

    Size(Double start, Double end) {
        this.start = start;
        this.end = end;
    }

    public Double getStart() {
        return start;
    }

    public Double getEnd() {
        return end;
    }
}
