package com.film.bot.unit.movie.enums;

import java.util.EnumSet;
import java.util.List;

public enum Genre {
    ANIME("аниме"),
    BIOGRAPHY("биография"),
    ACTION("боевик"),
    WESTERN("вестерн"),
    MILITARY("военный"),
    DETECTIVE("детектив"),
    KIDS("детский"),
    FOR_ADULTS("для взрослых"),
    DOCUMENTARY("документальный"),
    DRAMA("драма"),
    GAME("игра"),
    HISTORY("история"),
    COMEDY("комедия"),
    CONCERT("концерт"),
    SHORT("короткометражка"),
    CRIMINAL("криминал"),
    SOAP_DRAMA("мелодрама"),
    CARTOON("мультфильм"),
    MUSICAL("мюзикл"),
    NEWS("новости"),
    ADVENTURE("приключения"),
    REAL_TV("реальное ТВ"),
    FAMILY("семейный"),
    SPORT("спорт"),
    TALK_SHOW("ток-шоу"),
    THRILLER("триллер"),
    HORROR("ужасы"),
    FANTASTIC("фантастика"),
    FILM_NOIR("фильм-нуар"),
    FANTASY("фэнтези"),
    /**/;

    public static final List<String> ALL_GENRES = EnumSet.allOf(Genre.class)
            .stream()
            .map(Genre::getAlias)
            .toList();

    private final String alias;

    Genre(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}
