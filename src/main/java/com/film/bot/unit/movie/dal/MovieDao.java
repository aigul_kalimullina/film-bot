package com.film.bot.unit.movie.dal;

import java.util.Collection;
import java.util.List;

import com.film.bot.unit.movie.dal.entity.MovieEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieDao extends MongoRepository<MovieEntity, Long> {

    List<MovieEntity> findByIdIn(Collection<Long> ids);
}
