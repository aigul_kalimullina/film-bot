package com.film.bot.unit.movie.enums;

public enum Rating {
    LESS_FIVE("< 5", "1-3"),
    MORE_FIVE("> 5", "5-10"),
    MORE_SIX("> 6", "6-10"),
    MORE_SEVEN("> 7", "7-10"),
    MORE_EIGHT("> 8", "8-10"),
    MORE_NINE("> 9", "9-10"),
    /**/;

    private final String value;
    private final String kpValue;

    Rating(String value, String kpValue) {
        this.value = value;
        this.kpValue = kpValue;
    }

    public String getValue() {
        return value;
    }

    public String getKpValue() {
        return kpValue;
    }
}
