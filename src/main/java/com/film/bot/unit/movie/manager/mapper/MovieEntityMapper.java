package com.film.bot.unit.movie.manager.mapper;

import com.film.bot.unit.movie.dal.entity.MovieEntity;
import com.film.bot.unit.movie.manager.model.Movie;
import org.springframework.stereotype.Component;

@Component
public class MovieEntityMapper {

    public Movie fromDal(MovieEntity movieEntity) {
        return new Movie(
                movieEntity.getId(),
                movieEntity.getRuName(),
                movieEntity.getEnName(),
                movieEntity.getGenres(),
                movieEntity.getRating()
        );
    }

    public MovieEntity toDal(Movie movie) {
        return new MovieEntity(
                movie.id(),
                movie.ruName(),
                movie.enName(),
                movie.genres(),
                movie.rating()
        );
    }
}
