package com.film.bot.unit.preferences.dal.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(collection = "user_preferences")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PreferencesEntity {

    @Id
    String id;
    String genre;
    String rating;
    String period;
    Long director;
    Long actor;
    Long userId;
    Long createdAt;
}
