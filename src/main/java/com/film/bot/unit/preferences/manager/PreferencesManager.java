package com.film.bot.unit.preferences.manager;

import com.film.bot.unit.preferences.dal.PreferencesDao;
import com.film.bot.unit.preferences.dal.entity.PreferencesEntity;
import com.film.bot.unit.preferences.manager.mapper.PreferencesEntityMapper;
import com.film.bot.unit.preferences.manager.model.Preferences;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class PreferencesManager {

    private final PreferencesDao preferencesDao;
    private final PreferencesEntityMapper preferencesEntityMapper;

    public void saveGenre(long userId, String genre) {
        preferencesDao.save(
                new PreferencesEntity(
                        UUID.randomUUID().toString(),
                        genre,
                        null,
                        null,
                        null,
                        null,
                        userId,
                        Instant.now().toEpochMilli()
                )
        );
    }

    public void saveRating(long userId, String rating) {
        preferencesDao.save(
                preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)
                        .peek(item -> item.setRating(rating))
                        .findFirst()
                        .orElseThrow()
        );
    }

    public void savePeriod(long userId, String period) {
        preferencesDao.save(
                preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)
                        .peek(item -> item.setPeriod(period))
                        .findFirst()
                        .orElseThrow()
        );
    }

    public void savePreferencesWithDirector(long userId, long directorId) {
        preferencesDao.save(
                preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)
                        .peek(item -> item.setDirector(directorId))
                        .findFirst()
                        .orElseThrow()
        );
    }

    public void savePreferencesWithActor(long userId, long actorId) {
        preferencesDao.save(
                preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)
                        .peek(item -> item.setActor(actorId))
                        .findFirst()
                        .orElseThrow()
        );
    }

    public Preferences getAllSavedPreferences(long userId) {
        return preferencesDao.findByUserIdOrderByCreatedAtDesc(userId)
                .map(preferencesEntityMapper::fromDal)
                .findFirst()
                .orElseThrow();
    }
}
