package com.film.bot.unit.preferences.dal;

import java.util.stream.Stream;

import com.film.bot.unit.preferences.dal.entity.PreferencesEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PreferencesDao extends MongoRepository<PreferencesEntity, String> {

    Stream<PreferencesEntity> findByUserIdOrderByCreatedAtDesc(Long userId);
}
