package com.film.bot.unit.preferences.manager.mapper;

import com.film.bot.unit.preferences.dal.entity.PreferencesEntity;
import com.film.bot.unit.preferences.manager.model.Preferences;
import org.springframework.stereotype.Component;

@Component
public class PreferencesEntityMapper {

    public Preferences fromDal(PreferencesEntity preferencesEntity) {
        return new Preferences(
                preferencesEntity.getId(),
                preferencesEntity.getGenre(),
                preferencesEntity.getRating(),
                preferencesEntity.getPeriod(),
                preferencesEntity.getDirector(),
                preferencesEntity.getActor(),
                preferencesEntity.getUserId(),
                preferencesEntity.getCreatedAt()
        );
    }
}
