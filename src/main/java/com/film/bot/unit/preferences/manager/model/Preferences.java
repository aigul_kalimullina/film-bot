package com.film.bot.unit.preferences.manager.model;

import lombok.AllArgsConstructor;
import lombok.Value;

public record Preferences(
        String id,
        String genre,
        String rating,
        String period,
        Long director,
        Long actor,
        Long userId,
        Long createdAt
) {
}
