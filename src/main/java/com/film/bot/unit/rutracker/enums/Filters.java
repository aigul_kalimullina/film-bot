package com.film.bot.unit.rutracker.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum Filters {

    SEED("Seed"),
    RATING("Rating"),
    BEST("Best"),
    FIRST("FIRST"),
    SECOND("SECOND"),
    THIRD("THIRD"),
    FOURTH("FOURTH"),
    /**/;

    private static final Map<String, Filters> FUNCTIONS_MAP = Arrays.stream(Filters.values())
            .collect(Collectors.toMap(Filters::getAlias, Function.identity()));
    private final String alias;

    Filters(String alias) {
        this.alias = alias;
    }
    public static Filters fromValue(String txt) {
        return FUNCTIONS_MAP.get(txt);
    }

    public String getAlias() {
        return alias;
    }
}
