package com.film.bot.unit.rutracker;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.film.bot.unit.movie.enums.Size;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
public class RuTrackerSearch {

    public RuTrackerSearch() {
        if (Settings.USE_PROXY) {
            HttpProxy.setProxy();
        }
    }

    public String createSimpleSearchLink(String searchPhrase) {
        String encodeStr = URLEncoder.encode(searchPhrase, StandardCharsets.UTF_8);
        return Settings.SEARCH_URL + encodeStr;
    }

    public List<Torrent> search(String searchPhrase) throws IOException {
        if (!Authenticator.isAuthenticated()) {
            Authenticator.login();
        }

        if (Authenticator.isAuthenticated()) {
            Document doc;
            if (Settings.USE_PROXY) {
                doc = Jsoup.connect(Settings.SEARCH_URL + searchPhrase)
                        .cookies(Authenticator.getCookies())
                        .proxy(HttpProxy.getProxyObject())
                        .get();
            } else {
                doc = Jsoup.connect(Settings.SEARCH_URL + searchPhrase)
                        .cookies(Authenticator.getCookies())
                        .get();
            }

            Elements results = doc.getElementsByClass("tCenter hl-tr");
            List<Torrent> searchResults = new ArrayList<>();
            for (Element result : results) {
                searchResults.add(new Torrent(result));
            }
            return searchResults;
        }
        return Collections.emptyList();
    }

    public Torrent seedersFilter(List<Torrent> results) {
        if (results.size() > 0) {
            List<Torrent> filteredResults = results.stream().
                    filter(row -> row.getSeeders() > 0).
                    filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                    sorted(Comparator.comparingInt(Torrent::getSeeders).reversed()).toList();
            return filteredResults.get(0);
        }
        return null;
    }

    public Torrent ratingFilter(List<Torrent> results) {
        if (results.size() > 0) {
            List<Torrent> filteredResults = results.stream().
                    filter(row -> row.getSeeders() > 0).
                    filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                    sorted(Comparator.comparingInt(Torrent::getTimesDownloaded).reversed()).toList();
            return filteredResults.get(0);
        }
        return null;
    }

    public Torrent sizeFilter(List<Torrent> results, Size size) {
        if (results.size() > 0) {
            List<Torrent> filteredResults = results.stream().
                    filter(row -> row.getSeeders() > 0).
                    filter(row -> row.getSize() >= size.getStart()).
                    filter(row -> size.getEnd() == null || row.getSize() <= size.getEnd()).
                    filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                    sorted(Comparator.comparingInt(Torrent::getSeeders).reversed()).toList();
            if (filteredResults.size() > 0) {
                Torrent currentResult = filteredResults.get(0);
                int topSeedsAmount = currentResult.getSeeders();
                if (filteredResults.size() > 1) {
                    for (int i = 1; i < filteredResults.size(); i++) {
                        if (filteredResults.get(i).getDate().after(currentResult.getDate()) &&
                                (filteredResults.get(0).getSeeders() - filteredResults.get(i).getSeeders()) < percentSeeders(topSeedsAmount) &&
                                filteredResults.get(i).getSeeders() > 0) {
                            currentResult = filteredResults.get(i);
                        }
                    }
                }
                return currentResult;
            }
            return results.get(0);
        } else {
            return null;
        }
    }

    // trying to choose best result based on date, amount of seeds and status of the torrent
    public Torrent offerBestOne(List<Torrent> results) {
        if (results.size() > 0) {
            List<Torrent> filteredResults = results.stream().
                    filter(row -> row.getSeeders() > 0).
                    filter(row -> !row.getStatus().equals(TorrentStatus.NOT_APPROVED) || !row.getStatus().equals(TorrentStatus.APPROVED)).
                    sorted(Comparator.comparingInt(Torrent::getSeeders).reversed()).toList();
            if (filteredResults.size() > 0) {
                Torrent currentResult = filteredResults.get(0);
                int topSeedsAmount = currentResult.getSeeders();
                if (filteredResults.size() > 1) {
                    for (int i = 1; i < filteredResults.size(); i++) {
                        if (filteredResults.get(i).getDate().after(currentResult.getDate()) &&
                                (filteredResults.get(0).getSeeders() - filteredResults.get(i).getSeeders()) < percentSeeders(topSeedsAmount) &&
                                filteredResults.get(i).getSeeders() > 0) {
                            currentResult = filteredResults.get(i);
                        }
                    }
                }
                return currentResult;
            }
            return results.get(0);
        } else {
            return null;
        }
    }

    // 15 percent difference in seeders counts result as qualifier to best offering
    private int percentSeeders(int seedersAmount) {
        double result = ((double) seedersAmount / 100) * 15;
        return (int) result;
    }
}
