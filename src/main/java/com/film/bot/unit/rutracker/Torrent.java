package com.film.bot.unit.rutracker;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
@Slf4j
public class Torrent {

    private final TorrentStatus status;
    private final String forum;
    private final String name;
    private final String url;
    private final String author;
    private final double size; // size in GB
    private final int seeders;
    private final int leechers;
    private final int timesDownloaded;
    private final Date date;
    private String torrentMagnetURL;

    public Torrent(Element element) {
        this.status = TorrentStatus.valueOfLabel(
                element.getElementsByClass("row1 t-ico").get(1).attr("title")
        );
        this.forum = element.getElementsByClass("gen f ts-text").get(0).text();
        this.name = element.getElementsByClass("t-title").get(0).text();
        this.url = Settings.TRACKER_URL + element.select("a").get(1).attr("href");
        this.author = element.getElementsByClass("wbr u-name").get(0).text();

        this.size = round(
                Double.parseDouble(element.getElementsByClass("row4 small nowrap tor-size")
                        .get(0)
                        .attr("data-ts_text")) / 1024 / 1024 / 1024
        );
        int tempSeeds;
        try {
            tempSeeds = Integer.parseInt(element.getElementsByClass("seedmed").get(0).text());
        } catch (Exception ex) {
            log.error("Unable to parse seeds", ex);
            tempSeeds = 0;
        }
        this.seeders = tempSeeds;
        this.leechers = Integer.parseInt(element.getElementsByClass("row4 leechmed bold").get(0).text());
        this.timesDownloaded = Integer.parseInt(
                element.getElementsByClass("row4 small number-format").get(0).text()
        );
        this.date = new Date(
                Long.parseLong(element.getElementsByClass("row4 small nowrap")
                        .get(0)
                        .attr("data-ts_text")) * 1000
        );
        this.torrentMagnetURL = null;
    }

    public double round(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}