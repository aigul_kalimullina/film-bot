package com.film.bot.unit.kinopoisk.model.response;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.film.bot.unit.kinopoisk.model.Country;
import com.film.bot.unit.kinopoisk.model.Genre;
import com.film.bot.unit.kinopoisk.model.ProductionCompany;
import com.film.bot.unit.kinopoisk.model.Rating;
import com.film.bot.unit.kinopoisk.model.Video;
import com.film.bot.unit.kinopoisk.model.utils.MovieTextGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieResponse {

    @JsonProperty("id")
    private long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("alternativeName")
    private String alternativeName;
    @JsonProperty("enName")
    private String enName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("rating")
    private Rating rating;
    @JsonProperty("year")
    private int year;
    @JsonProperty("ageRating")
    private int ageRating;
    @JsonProperty("movieLength")
    private int movieLength;
    @JsonProperty("genres")
    private List<Genre> genres;
    @JsonProperty("countries")
    private List<Country> countries;
    @JsonProperty("videos")
    private Video videos;
    @JsonProperty("productionCompanies")
    private List<ProductionCompany> productionCompanies;

    @Override
    public String toString() {
        final StringBuilder sb = MovieTextGenerator.generateMovieBase(
                name,
                alternativeName,
                enName,
                year,
                description,
                movieLength,
                rating,
                genres,
                countries,
                ageRating
        );
        if (Objects.nonNull(videos)) {
            if (!CollectionUtils.isEmpty(videos.getTrailers())) {
                String trailer = videos.getTrailers().get(0).getUrl();
                if (StringUtils.isNotEmpty(trailer)) {
                    sb.append("Трейлер: ").append(trailer).append('\n');
                }
            }

            if (!CollectionUtils.isEmpty(videos.getTeasers())) {
                String teaser = videos.getTeasers().get(0).getUrl();
                if (StringUtils.isNotEmpty(teaser)) {
                    sb.append("Тизер: ").append(teaser).append('\n');
                }
            }
        }

        if (!CollectionUtils.isEmpty(productionCompanies)) {
            Set<String> names = productionCompanies
                    .stream()
                    .filter(Objects::nonNull)
                    .map(ProductionCompany::getName)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            if (!CollectionUtils.isEmpty(names)) {
                sb.append("Компании производства: ").append(String.join(", ", names)).append('\n');
            }
        }
        return sb.toString();
    }
}
