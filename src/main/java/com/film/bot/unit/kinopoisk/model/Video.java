package com.film.bot.unit.kinopoisk.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Video {
    private List<Trailer> trailers;
    private List<Teaser> teasers;
}
