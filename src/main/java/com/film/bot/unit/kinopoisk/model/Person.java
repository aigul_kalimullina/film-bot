package com.film.bot.unit.kinopoisk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    @JsonProperty("id")
    Long id;
    @JsonProperty("name")
    String name;
    @JsonProperty("enName")
    String enName;
    @JsonProperty("age")
    Integer age;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(name)) {
            sb.append(name);
        } else {
            sb.append(enName);
        }
        if (age != null && age != 0) {
            sb.append(" ").append(age).append(" ");
            if (age % 10 == 1 && age % 100 != 11) {
                sb.append("год");
            } else if (age % 10 >= 2 && age % 10 <= 4 && (age % 100 < 10 || age % 100 >= 20)) {
                sb.append("года");
            } else {
                sb.append("лет");
            }
        }
        return sb.toString();
    }
}
