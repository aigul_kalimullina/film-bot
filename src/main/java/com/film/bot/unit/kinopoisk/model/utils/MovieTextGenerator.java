package com.film.bot.unit.kinopoisk.model.utils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.film.bot.unit.kinopoisk.model.Country;
import com.film.bot.unit.kinopoisk.model.Genre;
import com.film.bot.unit.kinopoisk.model.Rating;
import com.film.bot.unit.movie.manager.model.Movie;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

@UtilityClass
public final class MovieTextGenerator {

    public static StringBuilder generateMovieBase(
            String name,
            String alternativeName,
            String enName,
            int year,
            String description,
            int movieLength,
            Rating rating,
            List<Genre> genres,
            List<Country> countries,
            int ageRating
    ) {
        final StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(name)) {
            sb.append("Русское название: ").append(name).append('\n');
        }
        if (StringUtils.isNotEmpty(enName)) {
            sb.append("Английское название: ").append(enName).append('\n');
        }
        if (StringUtils.isNotEmpty(alternativeName)) {
            sb.append("Альтернативное название: ").append(alternativeName).append('\n');
        }
        if (StringUtils.isNotEmpty(description)) {
            sb.append("Описание: ").append(description.replace("\n", "")).append('\n');
        }
        if (Objects.nonNull(rating)) {
            if (rating.getKp() != 0) {
                sb.append("Рейтинг: ").append(rating.getKp()).append('\n');
            }
        }
        if (year != 0) {
            sb.append("Год производства: ").append(year).append('\n');
        }
        if (ageRating != 0) {
            sb.append("Возрастное ограничение: ").append(ageRating).append("+ \n");
        }
        if (movieLength != 0) {
            sb.append("Продолжительность: ").append(movieLength).append(" мин.").append('\n');
        }
        if (!CollectionUtils.isEmpty(genres)) {
            sb.append("Жанры: ")
                    .append(String.join(", ", genres
                            .stream()
                            .filter(Objects::nonNull)
                            .map(Genre::getName)
                            .filter(Objects::nonNull)
                            .toList())
                    )
                    .append('\n');
        }
        if (!CollectionUtils.isEmpty(countries)) {
            List<String> names = countries
                    .stream()
                    .filter(Objects::nonNull)
                    .map(Country::getName)
                    .filter(Objects::nonNull)
                    .limit(4)
                    .distinct()
                    .collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(names)) {
                sb.append("Страны: ").append(String.join(", ", names)).append('\n');
            }
        }
        return sb;
    }

    public static StringBuilder generateFavouriteBase(Movie movie) {
        StringBuilder text = new StringBuilder();
        if (StringUtils.isNotEmpty(movie.ruName())) {
            text.append("\uD83C\uDDF7\uD83C\uDDFA Название: ").append(movie.ruName()).append('\n');
        }
        if (StringUtils.isNotEmpty(movie.enName())) {
            text.append("\uD83C\uDDEC\uD83C\uDDE7 Название: ").append(movie.enName()).append('\n');
        }
        if (!CollectionUtils.isEmpty(movie.genres())) {
            text.append("\uD83E\uDDE9 Жанры: ").append(String.join(", ", movie.genres())).append('\n');
        }
        if (movie.rating() != 0) {
            text.append("⬆️ Рейтинг: ").append(movie.rating()).append('\n');
        }
        return text;
    }
}
