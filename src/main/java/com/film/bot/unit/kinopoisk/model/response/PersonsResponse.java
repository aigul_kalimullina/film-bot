package com.film.bot.unit.kinopoisk.model.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.film.bot.unit.kinopoisk.model.Person;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonsResponse {
    @JsonProperty("docs")
    List<Person> persons;
    @JsonProperty("limit")
    Integer limit;
}