package com.film.bot.unit.kinopoisk.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.film.bot.unit.kinopoisk.model.Film;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MoviesResponse {
    @JsonProperty("docs")
    private List<Film> films;
    @JsonProperty("limit")
    private int limit;
    @JsonProperty("pages")
    private int pages;
}
