package com.film.bot.unit.kinopoisk.model;

import java.util.List;

import com.film.bot.unit.kinopoisk.model.utils.MovieTextGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Film {
    private int id;
    private String name;
    private String alternativeName;
    private String enName;
    private int year;
    private String description;
    private int movieLength;
    private List<PersonProfession> persons;
    private Rating rating;
    private List<Genre> genres;
    private List<Country> countries;
    private int ageRating;

    @Override
    public String toString() {
        return MovieTextGenerator.generateMovieBase(
                name,
                alternativeName,
                enName,
                year,
                description,
                movieLength,
                rating,
                genres,
                countries,
                ageRating
        ).toString();
    }
}
