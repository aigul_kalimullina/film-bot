package com.film.bot.unit.kinopoisk.enums;

public enum KinopoiskLinks {
    MOVIE("/v1.4/movie"),
    BY_MOVIE_ID("/v1.4/movie/"),
    RANDOM("/v1.4/movie/random"),
    SEARCH_BY_NAME("/v1.4/movie/search"),
    PERSON("/v1.4/person/search"),
    /**/;

    private final String alias;

    KinopoiskLinks(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}
