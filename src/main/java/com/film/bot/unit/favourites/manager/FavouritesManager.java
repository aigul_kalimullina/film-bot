package com.film.bot.unit.favourites.manager;

import java.util.List;
import java.util.Set;

import com.film.bot.unit.favourites.dal.FavouritesDao;
import com.film.bot.unit.favourites.dal.entity.FavouritesEntity;
import com.film.bot.unit.favourites.manager.model.Favourites;
import com.film.bot.unit.movie.manager.MovieManager;
import com.film.bot.unit.movie.manager.model.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FavouritesManager {

    private final FavouritesDao favouritesDao;
    private final MovieManager movieManager;

    public List<Movie> findFavouriteMoviesByUserId(long userId) {
        List<Long> movieIds = favouritesDao.findById(userId)
                .map(FavouritesEntity::getMovieIds)
                .orElseGet(List::of);

        if (movieIds.isEmpty()) {
            return List.of();
        }

        return movieManager.findMoviesByIds(movieIds);
    }

    public void addToFavourites(Favourites favourites) {
        favouritesDao.findById(favourites.userId()).ifPresentOrElse(
                favouritesEntity -> {
                    favouritesEntity.getMovieIds().add(favourites.movieId());
                    favouritesDao.save(favouritesEntity);
                },
                () -> favouritesDao.insert(new FavouritesEntity(
                        favourites.userId(),
                        List.of(favourites.movieId())
                ))
        );
    }

    public Movie findFavouriteMovieInfo(long movieId) {
        return movieManager.findByMovieId(movieId);
    }
}
