package com.film.bot.unit.favourites.manager.model;

public record Favourites(long userId, long movieId) {
}
