package com.film.bot.unit.favourites.dal.entity;

import java.util.List;
import java.util.Set;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(collection = "favourites")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FavouritesEntity {

    @Id
    Long userId;
    List<Long> movieIds;
}
