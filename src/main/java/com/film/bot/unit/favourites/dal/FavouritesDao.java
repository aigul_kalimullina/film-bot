package com.film.bot.unit.favourites.dal;

import com.film.bot.unit.favourites.dal.entity.FavouritesEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FavouritesDao extends MongoRepository<FavouritesEntity, Long> {

}
