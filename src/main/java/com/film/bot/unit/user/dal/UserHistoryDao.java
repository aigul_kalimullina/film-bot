package com.film.bot.unit.user.dal;

import java.util.stream.Stream;

import com.film.bot.unit.user.dal.entity.UserHistoryEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserHistoryDao extends MongoRepository<UserHistoryEntity, String> {

    Stream<UserHistoryEntity> findUserHistoriesByUserIdOrderByCreatedAtDesc(Long userId);
}
