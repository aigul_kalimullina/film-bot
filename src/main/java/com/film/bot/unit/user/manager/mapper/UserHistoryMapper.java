package com.film.bot.unit.user.manager.mapper;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import com.film.bot.unit.user.dal.entity.UserHistoryEntity;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class UserHistoryMapper {

    public Update fromDal(UserHistoryEntity userEntity) {
        return userEntity.getUpdate();
    }

    public UserHistoryEntity toDal(Update update) {
        return new UserHistoryEntity(
                UUID.randomUUID().toString(),
                update.hasMessage()
                        ? update.getMessage().getFrom().getId()
                        : update.getCallbackQuery().getFrom().getId(),
                LocalDateTime.now().toEpochSecond(ZoneOffset.UTC),
                update
        );
    }
}
