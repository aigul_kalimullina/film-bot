package com.film.bot.unit.user.manager.model;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class User {

    Long id;
    String username;
}
