package com.film.bot.unit.user.dal;

import com.film.bot.unit.user.dal.entity.UserEntity;
import com.film.bot.unit.user.manager.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends MongoRepository<UserEntity, Long> {
    boolean existsByUsername(String username);

    Optional<UserEntity> findByUsername(String username);
}
