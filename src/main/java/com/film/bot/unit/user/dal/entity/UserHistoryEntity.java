package com.film.bot.unit.user.dal.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.telegram.telegrambots.meta.api.objects.Update;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(collection = "telegram_user_messages")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserHistoryEntity {

    @Id
    String id;
    Long userId;
    Long createdAt;
    Update update;
}
