package com.film.bot.unit.user.manager.mapper;

import com.film.bot.unit.user.dal.entity.UserEntity;
import com.film.bot.unit.user.manager.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserEntityMapper {

    public User fromDal(UserEntity userEntity) {
        return new User(
                userEntity.getId(),
                userEntity.getUsername()
        );
    }

    public UserEntity toDal(User user) {
        return new UserEntity(
                user.getId(),
                user.getUsername()
        );
    }
}
