package com.film.bot.unit.user.manager;

import java.util.stream.Stream;

import com.film.bot.unit.user.dal.UserDao;
import com.film.bot.unit.user.dal.UserHistoryDao;
import com.film.bot.unit.user.dal.entity.UserEntity;
import com.film.bot.unit.user.manager.mapper.UserHistoryMapper;
import com.film.bot.unit.user.manager.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
@RequiredArgsConstructor
public class UserManager {

    private final UserHistoryMapper userHistoryMapper;
    private final UserDao userDao;
    private final UserHistoryDao userHistoryDao;

    public boolean exists(String username) {
        return userDao.existsByUsername(username);
    }

    public void insert(User user) {
        if (user.getUsername().isEmpty()) {
            throw new IllegalArgumentException("Username is empty");
        }
        userDao.save(new UserEntity(user.getId(), user.getUsername()));
    }

    public void saveInteraction(Update update) {
        userHistoryDao.save(userHistoryMapper.toDal(update));
    }

    public Stream<Update> findUserInteractions(Long userId) {
        return userHistoryDao.findUserHistoriesByUserIdOrderByCreatedAtDesc(userId).map(userHistoryMapper::fromDal);
    }

    public Long findLastMovie(Long userId) {
        return findUserInteractions(userId)
                .filter(Update::hasCallbackQuery)
                .findFirst()
                .map(update -> update.getCallbackQuery().getData())
                .map(movie -> Long.parseLong(movie.replace("Info_", "")))
                .orElseThrow();
    }
}
