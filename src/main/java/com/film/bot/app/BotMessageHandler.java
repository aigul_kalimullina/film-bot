package com.film.bot.app;

import java.util.ArrayList;
import java.util.List;

import com.film.bot.service.FavouritesService;
import com.film.bot.service.MovieService;
import com.film.bot.service.PreferencesService;
import com.film.bot.unit.rutracker.RuTrackerSearch;
import com.film.bot.unit.rutracker.enums.Filters;
import com.film.bot.service.enums.PreferencesFunction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

@Slf4j
@RequiredArgsConstructor
@Component
public class BotMessageHandler {

    private final FavouritesService favouritesService;
    private final MovieService movieService;
    private final PreferencesService preferencesService;

    public void start(SendMessage message) {
        ReplyKeyboardMarkup keyboardMarkup = initStartKeyboard();
        message.setText(
                """
                        Приветствуем тебя в кино-боте!
                        Бесплатный справочник о кино в твоем устройстве с удобным поиском \uD83E\uDD33
                        """
        );
        message.setReplyMarkup(keyboardMarkup);
    }

    public void handleFunctions(BotFunctions botFunctions, long id, SendMessage message) {
        switch (botFunctions) {
            case GENRES -> movieService.getMovieGenres(message);
            case FAVORITES -> favouritesService.getUserFavouriteMovies(id, message);
            case RANDOM -> movieService.getRandomMovie(message);
            case SEARCH -> message.setText("Введи название фильма: ");
            case POPULAR -> movieService.getPopularMovies(message);
            case LIKE_MOVIE -> favouritesService.addMovieToFavourites(id, message);
            case PREFERENCES -> preferencesService.startSearchByGenres(message);
            case LINKS -> movieService.getLinks(id, message);
            case MAIN_MENU -> {
                message.setReplyMarkup(initStartKeyboard());
                message.setText("Возврат на главное меню");
            }
        }
    }

    public void handleMessage(long userId, String txt, SendMessage message) {
        if (txt.startsWith("Favourite_")) {
            long movieId = Long.parseLong(txt.replace("Favourite_", ""));
            favouritesService.getFavouriteMovieInfo(movieId, message);
        } else if (txt.startsWith("Info_")) {
            long movieId = Long.parseLong(txt.replace("Info_", ""));
            movieService.getRichMovieInfo(movieId, message);
        } else if (txt.startsWith("Genre_")) {
            String genre = txt.replace("Genre_", "");
            movieService.getMovieByGenre(genre, message);
        } else if (txt.startsWith("Filter_")) {
            String str = txt.replace("Filter_", "");
            String[] lst = str.split("_");
            long movieId = Long.parseLong(lst[1]);
            Filters filter = Filters.fromValue(lst[0]);
            movieService.getDownloadLinkWithFilters(movieId, message, filter);
        } else if (txt.startsWith("Preferences_")) {
            String[] queryParts = txt.split("_");
            PreferencesFunction function = PreferencesFunction.valueOf(queryParts[1]);
            String messageStart = null;
            switch (function) {
                case GENRE -> {
                    if (queryParts.length == 3) {
                        preferencesService.writeChosenGenre(userId, queryParts[2]);
                        messageStart = "Выбран жанр " + queryParts[2];
                    } else {
                        preferencesService.writeChosenGenre(userId, null);
                        messageStart = "Выбор пропущен";
                    }
                    preferencesService.searchByRating(message);
                }
                case RATING -> {
                    if (queryParts.length == 3) {
                        preferencesService.writeChosenRating(userId, queryParts[2]);
                        messageStart = "Выбран рейтинг " + queryParts[2];
                    } else {
                        preferencesService.writeChosenRating(userId, null);
                        messageStart = "Выбор пропущен";
                    }
                    preferencesService.searchByPeriods(message);
                }
                case PERIOD -> {
                    if (queryParts.length == 3) {
                        preferencesService.writeChosenPeriod(userId, queryParts[2]);
                        messageStart = "Выбран период " + queryParts[2];
                    } else {
                        preferencesService.writeChosenPeriod(userId, null);
                        messageStart = "Выбор пропущен";
                    }
                    preferencesService.searchByDirector(message);
                }
                case DIRECTOR -> {
                    if (queryParts.length == 3) {
                        preferencesService.writeChosenDirector(userId, Long.parseLong(queryParts[2]));
                        messageStart = "Выбран режисер";
                    } else {
                        preferencesService.writeChosenDirector(userId, -1L);
                        messageStart = "Выбор пропущен";
                    }
                    preferencesService.searchByActor(message);
                }
                case ACTOR -> {
                    if (queryParts.length == 3) {
                        preferencesService.writeChosenActor(userId, Long.parseLong(queryParts[2]));
                        messageStart = "Выбран актер";
                    } else {
                        preferencesService.writeChosenActor(userId, -1L);
                        messageStart = "Выбор пропущен";
                    }
                    preferencesService.fullSearch(userId, message);
                }
            }
            message.setText(messageStart + "\n\n" + message.getText());
        }
    }

    public void handlePreferencesMessage(SendMessage message, String text, String currentText) {
        if (text.startsWith("Preferences_PERIOD_")) {
            preferencesService.findPerson(message, currentText, PreferencesFunction.DIRECTOR);
        } else {
            preferencesService.findPerson(message, currentText, PreferencesFunction.ACTOR);
        }
    }

    public void findBySearch(SendMessage message, String txt) {
        movieService.getMovieInfoBySearch(message, txt);
    }

    private ReplyKeyboardMarkup initStartKeyboard() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboardRows = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        BotFunctions.FIRST_ROW_FUNCTIONS.forEach(functions -> row.add(new KeyboardButton(functions.getAlias())));
        keyboardRows.add(row);

        KeyboardRow row2 = new KeyboardRow();
        BotFunctions.SECOND_ROW_FUNCTIONS.forEach(functions -> row2.add(new KeyboardButton(functions.getAlias())));
        keyboardRows.add(row2);
        replyKeyboardMarkup.setKeyboard(keyboardRows);
        return replyKeyboardMarkup;
    }
}
