package com.film.bot.app;

import com.film.bot.service.UserService;
import com.film.bot.unit.user.manager.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class FilmTelegramBot extends TelegramLongPollingBot {

    private final UserService userService;
    private final BotMessageHandler botMessageHandler;

    @Value("${bot.token}")
    private String BOT_TOKEN;
    @Value("${bot.name}")
    private String BOT_NAME;

    @Override
    public void onUpdateReceived(Update update) {
        try {
            log.info("Update is [{}]", update);
            SendMessage messageToSent = new SendMessage();
            if (update.hasMessage() && update.getMessage().hasText()) {
                String username = update
                        .getMessage()
                        .getFrom()
                        .getUserName();

                Long id = update.getMessage().getFrom().getId();
                String txt = update.getMessage().getText();

                if (!userService.existsByUsername(username)) {
                    userService.insert(new User(id, username));
                }

                BotFunctions botFunctions = BotFunctions.fromValue(txt);
                if (txt.equals("/start")) {
                    botMessageHandler.start(messageToSent);
                } else if (botFunctions != null) {
                    botMessageHandler.handleFunctions(botFunctions, id, messageToSent);
                } else {
                    Optional<Update> previousMessage = userService.findUserHistory(id).filter(Update::hasMessage).findFirst();
                    Optional<Update> previousCallback = userService.findUserHistory(id).filter(Update::hasCallbackQuery).findFirst();

                    previousCallback.ifPresent(value -> botMessageHandler.handlePreferencesMessage(
                            messageToSent,
                            value.getCallbackQuery().getData(),
                            txt
                    ));
                    if (previousMessage.isPresent() && previousMessage.get().getMessage().hasText()) {
                        String msg = previousMessage.get().getMessage().getText();
                        if (msg.equals(BotFunctions.SEARCH.getAlias())) {
                            botMessageHandler.findBySearch(messageToSent, txt);
                        }
                    }
                }
                messageToSent.setChatId(update.getMessage().getChatId().toString());
            } else if (update.hasCallbackQuery()) {
                botMessageHandler.handleMessage(
                        update.getCallbackQuery().getFrom().getId(),
                        update.getCallbackQuery().getData(),
                        messageToSent
                );
                messageToSent.setChatId(update.getCallbackQuery().getMessage().getChatId().toString());
            }
            if (StringUtils.isBlank(messageToSent.getText())) {
                messageToSent.setText("Произошла ошибка");
            }
            execute(messageToSent);
        } catch (TelegramApiException exception) {
            log.error("Unexpected bot function", exception);
        }
        userService.saveUserInteraction(update);
    }

    @Override
    public String getBotUsername() {
        return BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }
}
