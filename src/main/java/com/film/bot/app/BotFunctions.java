package com.film.bot.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum BotFunctions {

    FAVORITES("⭐️ Избранное"),
    POPULAR("\uD83D\uDD25 Популярное"),
    RANDOM("\uD83C\uDFB2 Случайный фильм"),
    PREFERENCES("\uD83C\uDFAC Выбрать по предпочтениям"),
    GENRES("\uD83E\uDDE9 Жанры"),
    SEARCH("\uD83D\uDD0D Поиск"),
    LIKE_MOVIE("\uD83C\uDF1F Добавить в избранное"),
    LINKS("\uD83D\uDD17 Получить ссылки"),
    MAIN_MENU("⬅️ Главное меню")
    /**/;

    public static final List<BotFunctions> FIRST_ROW_FUNCTIONS = Arrays.asList(
            FAVORITES,
            POPULAR,
            RANDOM
    );

    public static final List<BotFunctions> SECOND_ROW_FUNCTIONS = Arrays.asList(
            PREFERENCES,
            GENRES,
            SEARCH
    );

    private static final Map<String, BotFunctions> FUNCTIONS_MAP = Arrays.stream(BotFunctions.values())
            .collect(Collectors.toMap(BotFunctions::getAlias, Function.identity()));
    private final String alias;

    BotFunctions(String alias) {
        this.alias = alias;
    }
    public static BotFunctions fromValue(String txt) {
        return FUNCTIONS_MAP.get(txt);
    }

    public String getAlias() {
        return alias;
    }
}
