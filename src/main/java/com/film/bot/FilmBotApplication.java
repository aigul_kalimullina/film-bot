package com.film.bot;


import com.film.bot.app.BotMessageHandler;
import com.film.bot.app.FilmTelegramBot;
import com.film.bot.service.UserService;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@ComponentScan(basePackages = {
        "com.film.bot",
        "org.telegram.telegrambots"
})
public class FilmBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilmBotApplication.class, args);
    }

    @Bean
    public CloseableHttpClient httpClient() {
        return HttpClients.createDefault();
    }

    @Bean
    @Profile("!tests")
    public FilmTelegramBot filmTelegramBot(UserService userService, BotMessageHandler botMessageHandler) {
        return new FilmTelegramBot(userService, botMessageHandler);
    }
}