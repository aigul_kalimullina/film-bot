package com.film.bot.service.enums;

public enum PreferencesFunction {
    GENRE,
    RATING,
    PERIOD,
    DIRECTOR,
    ACTOR
    /**/;
}
