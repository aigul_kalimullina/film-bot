package com.film.bot.service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.movie.enums.Size;
import com.film.bot.unit.rutracker.RuTrackerSearch;
import com.film.bot.unit.rutracker.Torrent;
import com.film.bot.unit.rutracker.enums.Filters;
import com.film.bot.unit.user.manager.UserManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.HttpStatusException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

@Slf4j
@Service
@RequiredArgsConstructor
public class MovieService {

    private final KinopoiskService kinopoiskService;
    private final BotMessageGenerator botMessageGenerator;
    private final RuTrackerSearch ruTrackerSearch;
    private final UserManager userManager;

    public void getMovieGenres(SendMessage message) {
        message.setText("Выбери жанр:");
        botMessageGenerator.generateGenres(message, "Genre_");
    }

    public void getRandomMovie(SendMessage message) {
        MovieResponse movie = kinopoiskService.findRandomMovie();
        if (Objects.nonNull(movie)) {
            message.setText("Полученный фильм:");
            InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            InlineKeyboardButton button = new InlineKeyboardButton();
            if (StringUtils.isNotBlank(movie.getName())) {
                button.setText(movie.getName());
            } else if (StringUtils.isNotBlank(movie.getAlternativeName())) {
                button.setText(movie.getAlternativeName());
            } else if (StringUtils.isNotBlank(movie.getEnName())) {
                button.setText(movie.getEnName());
            }
            button.setCallbackData("Info_" + movie.getId());
            row1.add(button);
            buttons.add(row1);
            keyboardMarkup.setKeyboard(buttons);
            message.setReplyMarkup(keyboardMarkup);
        } else {
            message.setText("Фильм не был найден");
        }
    }

    public void getPopularMovies(SendMessage message) {
        List<Film> films = kinopoiskService.findTop10Movies().getFilms();
        botMessageGenerator.generateMovieInfoMessage(
                films,
                "Топ фильмов не найден!",
                "Топ 10 популярных фильмов: ",
                message
        );
    }

    public void getRichMovieInfo(Long movieId, SendMessage message) {
        MovieResponse movie = kinopoiskService.findMovieById(movieId);
        if (Objects.nonNull(movie)) {
            botMessageGenerator.initMovieMessage(movie.toString(), message);
        } else {
            message.setText("Фильм не был найден");
        }
    }

    public void getMovieByGenre(String genre, SendMessage message) {
        List<Film> films = kinopoiskService.findMovieByFilters(genre).getFilms();
        botMessageGenerator.generateMovieInfoMessage(
                films,
                "Фильмы в этом жанре не были найдены!",
                StringUtils.isNotBlank(genre)
                        ? "Фильмы, найденные в жанре " + genre + " : "
                        : "Фильмы со случайным жанром",
                message
        );
    }

    public void getMovieInfoBySearch(SendMessage message, String txt) {
        List<Film> response = kinopoiskService.searchMovieByName(txt).getFilms().stream().limit(1).toList();
        StringBuilder sb = new StringBuilder("Найденные фильмы по запросу: \n \n");
        if (!CollectionUtils.isEmpty(response)) {
            List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
            response.forEach(film -> {
                sb.append(film.toString()).append("\n");
                List<InlineKeyboardButton> row1 = new ArrayList<>();
                InlineKeyboardButton button = new InlineKeyboardButton();
                if (StringUtils.isNotBlank(film.getName())) {
                    button.setText(film.getName());
                } else if (StringUtils.isNotBlank(film.getAlternativeName())) {
                    button.setText(film.getAlternativeName());
                } else if (StringUtils.isNotBlank(film.getEnName())) {
                    button.setText(film.getEnName());
                }
                button.setCallbackData("Info_" + film.getId());
                row1.add(button);
                buttons.add(row1);
            });

            botMessageGenerator.initMovieMessage(sb.toString(), message);
            InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
            keyboardMarkup.setKeyboard(buttons);
            message.setReplyMarkup(keyboardMarkup);
        } else {
            message.setText("Фильм по запросу не был найден!");
        }
    }

    public void getLinks(Long userId, SendMessage message) {
        long movieId = userManager.findLastMovie(userId);
        getLinksByMovieId(message, movieId);
    }

    public void getLinksByMovieId(SendMessage message, long movieId) {
        String kpUrl = "https://www.kinopoisk.ru/film/" + movieId + "/";
        String sskpUrl = "http://sspoisk.ru/film/" + movieId + "/";
        message.setText("Ссылка на кинопоиск: " + kpUrl + "\n"
                + "Ссылка на просмотр: " + sskpUrl + "\n"
                + "Для получения ссылки на скачивание выбери необходимые фильтры через клавиатуру: ");
        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText("по сидам");
        button.setCallbackData("Filter_Seed_" + movieId);
        row1.add(button);

        InlineKeyboardButton button1 = new InlineKeyboardButton();
        button1.setText("по рейтингу");
        button1.setCallbackData("Filter_Rating_" + movieId);
        row1.add(button1);

        InlineKeyboardButton button2 = new InlineKeyboardButton();
        button2.setText("лучшая раздача");
        button2.setCallbackData("Filter_Best_" + movieId);
        row1.add(button2);
        buttons.add(row1);

        Arrays.stream(Size.values()).forEach(size -> {
            InlineKeyboardButton btn = new InlineKeyboardButton();
            StringBuilder messageText = new StringBuilder("Размер ГБ:");
            if (size.getStart() != null) {
                messageText.append(" от ").append(size.getStart());
            }
            if (size.getEnd() != null) {
                messageText.append(" до ").append(size.getEnd());
            }
            btn.setText(messageText.toString());
            btn.setCallbackData("Filter_" + size.name() + "_" + movieId);
            buttons.add(List.of(btn));
        });
        keyboardMarkup.setKeyboard(buttons);
        message.setReplyMarkup(keyboardMarkup);
    }

    public void getDownloadLinkWithFilters(Long movieId, SendMessage message, Filters filter) {
        MovieResponse movie = kinopoiskService.findMovieById(movieId);
        String name = movie.getName() == null ? movie.getAlternativeName() : movie.getName();
        String searchString = movie.getYear() != 0
                ? name + " " + movie.getYear()
                : name;

        try {
            List<Torrent> torrens;
            try {
                torrens = ruTrackerSearch.search(URLEncoder.encode(searchString, StandardCharsets.UTF_8));
            } catch (Exception e) {
                torrens = ruTrackerSearch.search(URLEncoder.encode(name, StandardCharsets.UTF_8));
                searchString = name;
            }

            if (CollectionUtils.isEmpty(torrens)) {
                message.setText("По данным параметрам не найден ни один торрент");
                return;
            }
            Torrent torrent = torrens.get(0);
            switch (filter) {
                case SEED -> torrent = ruTrackerSearch.seedersFilter(torrens);
                case RATING -> torrent = ruTrackerSearch.ratingFilter(torrens);
                case BEST -> torrent = ruTrackerSearch.offerBestOne(torrens);
                case FIRST -> torrent = ruTrackerSearch.sizeFilter(torrens, Size.FIRST);
                case SECOND -> torrent = ruTrackerSearch.sizeFilter(torrens, Size.SECOND);
                case THIRD -> torrent = ruTrackerSearch.sizeFilter(torrens, Size.THIRD);
                case FOURTH -> torrent = ruTrackerSearch.sizeFilter(torrens, Size.FOURTH);
            }

            message.setText("Ссылка на скачивание: " + torrent.getUrl() + "\n"
                    + "Ссылка на простой поиск торрента: "
                    + ruTrackerSearch.createSimpleSearchLink(searchString));
        } catch (HttpStatusException e) {
            log.error(e.getMessage(), e);
            message.setText("Проблемы с сервисом rutracher\n" +
                    "Ссылка на простой поиск торрента: "
                    + ruTrackerSearch.createSimpleSearchLink(searchString)
            );
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            message.setText("По данным параметрам не найдена ни одна ссылка на скачивание\n" +
                    "Ссылка на простой поиск торрента: "
                    + ruTrackerSearch.createSimpleSearchLink(searchString)
            );
        }

    }
}
