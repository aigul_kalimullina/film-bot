package com.film.bot.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import com.film.bot.unit.favourites.manager.FavouritesManager;
import com.film.bot.unit.favourites.manager.model.Favourites;
import com.film.bot.unit.kinopoisk.model.Genre;
import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.kinopoisk.model.utils.MovieTextGenerator;
import com.film.bot.unit.movie.manager.MovieManager;
import com.film.bot.unit.movie.manager.model.Movie;
import com.film.bot.unit.user.manager.UserManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

@Slf4j
@Service
@RequiredArgsConstructor
public class FavouritesService {

    private final KinopoiskService kinopoiskService;
    private final MovieManager movieManager;
    private final MovieService movieService;
    private final FavouritesManager favouritesManager;
    private final UserManager userManager;

    public void addMovieToFavourites(long userId, SendMessage message) {
        long movieId = userManager.findUserInteractions(userId)
                .filter(Update::hasCallbackQuery)
                .findFirst()
                .map(update -> update.getCallbackQuery().getData())
                .map(movie -> movie.replace("Info_", ""))
                .map(movie -> movie.replace("Filter_Best_", ""))
                .map(movie -> movie.replace("Favourite_", ""))
                .map(Long::parseLong)
                .orElseThrow();

        List<Long> movies = favouritesManager.findFavouriteMoviesByUserId(userId)
                .stream()
                .map(Movie::id)
                .toList();

        if (movies.contains(movieId)) {
            message.setText("Фильм уже находится в избранном");
            return;
        }

        MovieResponse movie = kinopoiskService.findMovieById(movieId);
        movieManager.addMovie(new Movie(
                movie.getId(),
                movie.getName(),
                StringUtils.isNotBlank(movie.getEnName()) ? movie.getEnName() : movie.getAlternativeName(),
                !CollectionUtils.isEmpty(movie.getGenres())
                        ? movie.getGenres().stream().map(Genre::getName).toList()
                        : List.of(),
                Objects.nonNull(movie.getRating()) ? movie.getRating().getKp() : 0
        ));
        favouritesManager.addToFavourites(new Favourites(userId, movieId));
        message.setText("Фильм добавлен в избранное");
    }

    public void getUserFavouriteMovies(long userId, SendMessage message) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> movies = new ArrayList<>();

        favouritesManager.findFavouriteMoviesByUserId(userId)
                .forEach(movie -> {
                    InlineKeyboardButton button = new InlineKeyboardButton();
                    if (StringUtils.isNotEmpty(movie.ruName())) {
                        button.setText(movie.ruName());
                    } else if (StringUtils.isNotEmpty(movie.enName())) {
                        button.setText(movie.enName());
                    }
                    button.setCallbackData("Favourite_" + movie.id());
                    movies.add(List.of(button));
                });

        inlineKeyboardMarkup.setKeyboard(movies);
        if (movies.isEmpty()) {
            message.setText("Ни один фильм еще не был добавлен в Избранное");
            return;
        }
        message.setText("Твои фильмы в избранном: ");
        message.setReplyMarkup(inlineKeyboardMarkup);
    }

    public void getFavouriteMovieInfo(long movieId, SendMessage message) {
        Movie movie = favouritesManager.findFavouriteMovieInfo(movieId);
        String movieText = MovieTextGenerator.generateFavouriteBase(movie).toString();
        movieService.getLinksByMovieId(message, movieId);
        message.setText(movieText + "\n" + message.getText());
    }
}
