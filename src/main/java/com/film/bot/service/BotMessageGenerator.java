package com.film.bot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.film.bot.app.BotFunctions;
import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.movie.enums.Genre;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

@Component
public class BotMessageGenerator {

    public void generateGenres(
            SendMessage message,
            String callbackText
    ) {
        List<String> genres = Genre.ALL_GENRES;
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        for (int i = 0; i < 30; i += 2) {
            List<InlineKeyboardButton> keyboardButtonsRow = new ArrayList<>();

            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(genres.get(i));
            button.setCallbackData(callbackText + genres.get(i));
            keyboardButtonsRow.add(button);

            button = new InlineKeyboardButton();
            button.setText(genres.get(i + 1));
            button.setCallbackData(callbackText + genres.get(i + 1));
            keyboardButtonsRow.add(button);

            rowList.add(keyboardButtonsRow);
        }

        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText("любой жанр");
        button.setCallbackData(callbackText);
        rowList.add(List.of(button));

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(rowList);
        message.setReplyMarkup(inlineKeyboardMarkup);
    }

    public void generatePreferencesButtons(
            SendMessage message,
            String callbackText,
            List<List<InlineKeyboardButton>> buttonsKeyboard
    ) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText("Не важно");
        button.setCallbackData(callbackText);
        buttonsKeyboard.add(List.of(button));

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(buttonsKeyboard);
        message.setReplyMarkup(inlineKeyboardMarkup);
    }

    public void generateMovieInfoMessage(
            List<Film> films,
            String emptyMessage,
            String baseMessage,
            SendMessage message
    ) {
        if (films != null) {
            films = films
                    .stream()
                    .filter(Objects::nonNull)
                    .toList();
        } else {
            films = List.of();
        }


        if (CollectionUtils.isEmpty(films)) {
            message.setText(emptyMessage);
            return;
        }

        message.setText(baseMessage);

        List<List<InlineKeyboardButton>> movies = new ArrayList<>();
        films.stream()
                .filter(film -> Objects.nonNull(film.getName()))
                .forEach(film -> {
                    InlineKeyboardButton button = new InlineKeyboardButton();
                    button.setText(film.getName());
                    button.setCallbackData("Info_" + film.getId());
                    movies.add(List.of(button));
                });

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(movies);
        message.setReplyMarkup(inlineKeyboardMarkup);
    }

    public void initMovieMessage(String text, SendMessage message) {
        message.setText(text);
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboardRows = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        row.add(new KeyboardButton(BotFunctions.LIKE_MOVIE.getAlias()));
        row.add(new KeyboardButton(BotFunctions.LINKS.getAlias()));
        row.add(new KeyboardButton(BotFunctions.MAIN_MENU.getAlias()));
        keyboardRows.add(row);

        keyboardMarkup.setKeyboard(keyboardRows);
        message.setReplyMarkup(keyboardMarkup);
    }
}
