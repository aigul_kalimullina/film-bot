package com.film.bot.service;

import java.util.stream.Stream;

import com.film.bot.unit.user.manager.UserManager;
import com.film.bot.unit.user.manager.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserManager userManager;

    public void saveUserInteraction(Update update) {
        userManager.saveInteraction(update);
    }

    public Stream<Update> findUserHistory(long userId) {
        return userManager.findUserInteractions(userId);
    }

    public void insert(User user) {
        userManager.insert(user);
    }

    public boolean existsByUsername(String username) {
        return userManager.exists(username);
    }
}

