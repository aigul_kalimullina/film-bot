package com.film.bot.service;

import com.film.bot.unit.kinopoisk.model.Film;
import com.film.bot.unit.kinopoisk.model.response.MovieResponse;
import com.film.bot.unit.kinopoisk.model.response.MoviesResponse;
import com.film.bot.unit.kinopoisk.enums.KinopoiskLinks;
import com.film.bot.unit.kinopoisk.model.response.PersonsResponse;
import com.film.bot.unit.preferences.manager.model.Preferences;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
@Service
public class KinopoiskService {

    private final ApiRequestPerformer apiRequestPerformer;

    public KinopoiskService(ApiRequestPerformer apiRequestPerformer) {
        this.apiRequestPerformer = apiRequestPerformer;
    }

    public PersonsResponse findPersonByName(String name) {
        String url = KinopoiskLinks.PERSON.getAlias()
                + "?page=1&limit=200&query="
                + name.replace(" ", "%20");
        PersonsResponse response = apiRequestPerformer.generateRequest(url, PersonsResponse.class);
        return response != null ? response : new PersonsResponse();
    }

    public MoviesResponse searchMovieByName(String name) {
        String url = KinopoiskLinks.SEARCH_BY_NAME.getAlias()
                + "?page=1&limit=5&query="
                + name.replace(" ", "%20");
        MoviesResponse response = apiRequestPerformer.generateRequest(url, MoviesResponse.class);
        return response != null ? response : new MoviesResponse();
    }

    public MoviesResponse findMovieByFilters(String genre) {
        String url = KinopoiskLinks.MOVIE.getAlias() + "?page=1&limit=10";
        if (StringUtils.isNoneEmpty(genre)) {
            url += "&genres.name=" + URLEncoder.encode(genre, StandardCharsets.UTF_8);
        }
        MoviesResponse response = apiRequestPerformer.generateRequest(url, MoviesResponse.class);
        return response != null ? response : new MoviesResponse();
    }

    public MovieResponse findMovieById(long movieId) {
        String url = KinopoiskLinks.BY_MOVIE_ID.getAlias() + movieId;
        MovieResponse response = apiRequestPerformer.generateRequest(url, MovieResponse.class);
        return response != null ? response : new MovieResponse();
    }

    public MoviesResponse findTop10Movies() {
        String url = KinopoiskLinks.MOVIE.getAlias()
                + "?page=1&limit=10&sortField=rating.kp&sortType=-1&lists=popular-films";

        MoviesResponse response = apiRequestPerformer.generateRequest(url, MoviesResponse.class);
        return response != null ? response : new MoviesResponse();
    }

    public MovieResponse findRandomMovie() {
        MovieResponse response = apiRequestPerformer.generateRequest(KinopoiskLinks.RANDOM.getAlias(), MovieResponse.class);
        return response != null ? response : new MovieResponse();
    }

    public List<Film> findByPreferences(Preferences preferences) {
        StringBuilder requestBuilder = new StringBuilder(KinopoiskLinks.MOVIE.getAlias());

        requestBuilder.append("?page=1&limit=20");
        if (StringUtils.isNotBlank(preferences.genre())) {
            requestBuilder.append("&genres.name=").append(preferences.genre());
        }

        Long actor = preferences.actor();
        Long director = preferences.director();

        if (actor != -1L) {
            requestBuilder.append("&persons.id=").append(actor);
            requestBuilder.append("&persons.profession=").append("актер");
        }
        if (director != -1L) {
            requestBuilder.append("&persons.id=").append(director);
            requestBuilder.append("&persons.profession=").append("режиссер");
        }


        String period = preferences.period();
        if (StringUtils.isNotBlank(period)) {
            requestBuilder.append("&year=").append(period);
        }
        String rating = preferences.rating();
        if (StringUtils.isNotBlank(rating)) {
            requestBuilder.append("&rating.kp=").append(rating);
        }

        String selectedFields = "&selectFields=persons&selectFields=id" +
                "&selectFields=name&selectFields=alternativeName&selectFields=enName&selectFields=type" +
                "&selectFields=year&selectFields=description&selectFields=shortDescription&selectFields=movieLength" +
                "&selectFields=isSeries&selectFields=ticketsOnSale&selectFields=totalSeriesLength" +
                "&selectFields=seriesLength&selectFields=ratingMpaa&selectFields=ageRating&selectFields=top10" +
                "&selectFields=top250&selectFields=typeNumber&selectFields=status&selectFields=names&selectFields=logo" +
                "&selectFields=poster&selectFields=backdrop&selectFields=rating&selectFields=votes&selectFields=genres" +
                "&selectFields=countries&selectFields=releaseYears";

        requestBuilder.append(selectedFields);
        MoviesResponse response = apiRequestPerformer.generateRequest(requestBuilder.toString(), MoviesResponse.class);

        return response.getFilms().stream().filter(item ->
                        item.getPersons()
                                .stream()
                                .anyMatch(person ->
                                        actor == -1 ||
                                                person.getId().equals(actor)
                                                        && "актеры".equals(person.getProfession())))
                .filter(item ->
                        item.getPersons()
                                .stream()
                                .anyMatch(person ->
                                        director == -1 ||
                                                person.getId().equals(director)
                                                        && "режиссеры".equals(person.getProfession())))
                .limit(10)
                .toList();

    }
}
