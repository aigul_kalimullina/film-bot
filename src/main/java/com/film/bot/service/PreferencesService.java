package com.film.bot.service;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.film.bot.service.enums.PreferencesFunction;
import com.film.bot.unit.kinopoisk.model.response.PersonsResponse;
import com.film.bot.unit.movie.enums.Rating;
import com.film.bot.unit.preferences.manager.PreferencesManager;
import com.film.bot.unit.preferences.manager.model.Preferences;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

@Service
@RequiredArgsConstructor
public class PreferencesService {

    @Value("#{'${movie.periods.for.preferences}'.split(',')}")
    private final List<String> periods;
    private final PreferencesManager preferencesManager;
    private final KinopoiskService kinopoiskService;
    private final BotMessageGenerator botMessageGenerator;

    public void startSearchByGenres(SendMessage message) {
        message.setText("Выбери любимый жанр:");
        botMessageGenerator.generateGenres(
                message,
                "Preferences_" + PreferencesFunction.GENRE + "_"
        );
    }

    public void writeChosenGenre(long userId, String genre) {
        preferencesManager.saveGenre(userId, genre);
    }

    public void searchByRating(SendMessage message) {
        message.setText("Выбери минимальный рейтинг фильма:");
        List<List<InlineKeyboardButton>> ratings = new ArrayList<>();

        String callbackData = "Preferences_" + PreferencesFunction.RATING + "_";
        EnumSet.allOf(Rating.class)
                .forEach(rating -> {
                    InlineKeyboardButton button = new InlineKeyboardButton();
                    button.setText(rating.getValue());
                    button.setCallbackData(callbackData + rating.getKpValue());
                    ratings.add(List.of(button));
                });

        botMessageGenerator.generatePreferencesButtons(
                message,
                callbackData,
                ratings
        );
    }

    public void writeChosenRating(long userId, String rating) {
        preferencesManager.saveRating(userId, rating);
    }

    public void searchByPeriods(SendMessage message) {
        message.setText("Выбери период выпуска:");
        List<List<InlineKeyboardButton>> periodList = new ArrayList<>();

        periods.forEach(period -> {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(period);
            button.setCallbackData("Preferences_" + PreferencesFunction.PERIOD + "_" + period);
            periodList.add(List.of(button));
        });

        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText("Не важно");
        button.setCallbackData("Preferences_" + PreferencesFunction.PERIOD + "_");
        periodList.add(List.of(button));

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(periodList);
        message.setReplyMarkup(inlineKeyboardMarkup);
    }

    public void writeChosenPeriod(long userId, String period) {
        preferencesManager.savePeriod(userId, period);
    }

    public void searchByDirector(SendMessage message) {
        message.setText("Введи данные режиссера с клавиатуры:");

        List<List<InlineKeyboardButton>> periodList = new ArrayList<>();

        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText("Не важно");
        button.setCallbackData("Preferences_" + PreferencesFunction.DIRECTOR + "_");
        periodList.add(List.of(button));

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(periodList);
        message.setReplyMarkup(inlineKeyboardMarkup);
    }

    public void writeChosenDirector(long userId, Long directorId) {
        preferencesManager.savePreferencesWithDirector(userId, directorId);
    }

    public void searchByActor(SendMessage message) {
        message.setText("Введи данные актера с клавиатуры:");
        String callbackData = "Preferences_" + PreferencesFunction.ACTOR + "_";
        botMessageGenerator.generatePreferencesButtons(message, callbackData, new ArrayList<>());
    }

    public void writeChosenActor(long userId, Long actorId) {
        preferencesManager.savePreferencesWithActor(userId, actorId);
    }

    public void fullSearch(long userId, SendMessage message) {
        Preferences preferences = preferencesManager.getAllSavedPreferences(userId);
        botMessageGenerator.generateMovieInfoMessage(
                kinopoiskService.findByPreferences(preferences),
                "По твоим предпочтениям ничего не нашлось",
                "Фильмы по твоим предпочтениям:",
                message
        );
    }

    public void findPerson(SendMessage message, String currentText, PreferencesFunction function) {
        PersonsResponse persons = kinopoiskService.findPersonByName(currentText);
        if (CollectionUtils.isEmpty(persons.getPersons())) {
            switch (function) {
                case DIRECTOR -> searchByDirector(message);
                case ACTOR -> searchByActor(message);
            }
            message.setText("По твоему запросу не найдена ни одна персона, попробуй снова");
            return;
        }
        message.setText("Выбери персону: ");
        List<List<InlineKeyboardButton>> personsList = new ArrayList<>();

        persons.getPersons().stream()
                .filter(person -> person.getAge() != null && person.getAge() != 0)
                .limit(5)
                .forEach(person -> {
                    InlineKeyboardButton button = new InlineKeyboardButton();
                    button.setText(person.toString());
                    button.setCallbackData("Preferences_" + function + "_" + person.getId());
                    personsList.add(List.of(button));
                });

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(personsList);
        message.setReplyMarkup(inlineKeyboardMarkup);
    }
}
