package com.film.bot.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ApiRequestPerformer {
    private final String token;
    private final String api;
    private final CloseableHttpClient httpclient;
    private final ObjectMapper objectMapper;

    public ApiRequestPerformer(
            @Value("${kinopoisk.token}") String token,
            @Value("${kinopoisk.api}") String api,
            ObjectMapper objectMapper
    ) {
        this.token = token;
        this.api = api;
        this.httpclient = HttpClients.createDefault();
        this.objectMapper = objectMapper;
    }

    public <T> T generateRequest(String url, Class<T> valueType) {
        HttpUriRequest httpGet = new HttpGet(api + url);
        httpGet.setHeader("X-API-KEY", token);
        httpGet.setHeader("Accept", "application/json");
        try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
            String result = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
            return objectMapper.readValue(result, valueType);
        } catch (IOException e) {
            log.error("Failed request generation", e);
        }
        return null;
    }
}
