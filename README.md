### Film bot

Как запустить проект?

1) Создаем Сертификат yandexCA
```
cd ~/.mongodb && \
sudo keytool -importcert \
-alias YandexCA -file root.crt \
-keystore ssl -storepass <пароль> \
--noprompt
   ```

2) Далее добавляем YATrustStore в список доверенных источников (путем добавления данных параметров запуска)
```   
--add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED
--add-opens=java.base/java.util.concurrent=ALL-UNNAMED
--add-opens=java.base/java.lang=ALL-UNNAMED
-Djavax.net.ssl.trustStore=/Users/kalimullina/.mongodb/YATrustStore
-Djavax.net.ssl.trustStorePassword=<пароль, тот же что и выше>
-Dmongodb.password=<пароль передам отдельно>
-Dmongodb.username=<username передам отдельно>
```
3) Далее проект просто запускается
